#!/usr/bin/env bash

php_version="7.0"
php_conf="/etc/php/7.0/fpm/php.ini"
fpm_conf="/etc/php/7.0/fpm/pool.d/www.conf"
cassandra_version="3.0.9"
cassandra_so="/usr/lib/php/20151012/cassandra.so"

sudo add-apt-repository -y ppa:ondrej/php
sudo add-apt-repository -y ppa:openjdk-r/ppa
sudo echo "deb http://debian.datastax.com/community stable main" > /etc/apt/sources.list.d/cassandra.sources.list
sudo curl -sS -L http://debian.datastax.com/debian/repo_key | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y \
  nginx \
  wget \
  openssh-client \
  curl \
  git \
  php7.0 \
  php7.0-dev \
  php7.0-bcmath \
  php7.0-common \
  php7.0-ctype \
  php7.0-fpm \
  php7.0-mbstring \
  php7.0-mcrypt \
  php7.0-pdo \
  cassandra=3.0.9 cassandra-tools=3.0.9 \
  openjdk-8-jre \
  rabbitmq-server \
  openssl \
  php7.0-gd \
  php7.0-xml \
  php7.0-curl \
  php7.0-json \
  php7.0-zip --force-yes

# Setup nginx configs
sudo rm -rf /etc/nginx/sites-enabled/default
sudo cp -f /var/www/Herd/conf/nginx-site.conf /etc/nginx/sites-enabled/minds.conf
sudo cp -f /var/www/Herd/conf/nginx.conf /etc/nginx/nginx.conf

# Tweak PHP configs
sudo sed -i -e "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php/7.0/fpm/php.ini && \
sudo sed -i -e "s/upload_max_filesize\s*=\s*2M/upload_max_filesize = 100M/g" /etc/php/7.0/fpm/php.ini && \
sudo sed -i -e "s/post_max_size\s*=\s*8M/post_max_size = 100M/g" /etc/php/7.0/fpm/php.ini && \
sudo sed -i -e "s/variables_order = \"GPCS\"/variables_order = \"EGPCS\"/g" /etc/php/7.0/fpm/php.ini && \
sudo sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.0/fpm/pool.d/www.conf && \
sudo sed -i -e "s/;catch_workers_output\s*=\s*yes/catch_workers_output = yes/g" /etc/php/7.0/fpm/pool.d/www.conf && \
sudo sed -i -e "s/pm.max_children = 4/pm.max_children = 4/g" /etc/php/7.0/fpm/pool.d/www.conf && \
sudo sed -i -e "s/pm.start_servers = 2/pm.start_servers = 3/g" /etc/php/7.0/fpm/pool.d/www.conf && \
sudo sed -i -e "s/pm.min_spare_servers = 1/pm.min_spare_servers = 2/g" /etc/php/7.0/fpm/pool.d/www.conf && \
sudo sed -i -e "s/pm.max_spare_servers = 3/pm.max_spare_servers = 4/g" /etc/php/7.0/fpm/pool.d/www.conf && \
sudo sed -i -e "s/pm.max_requests = 500/pm.max_requests = 200/g" /etc/php/7.0/fpm/pool.d/www.conf && \
sudo sed -i -e "s/user = nobody/user = nginx/g" /etc/php/7.0/fpm/pool.d/www.conf && \
sudo sed -i -e "s/group = nobody/group = nginx/g" /etc/php/7.0/fpm/pool.d/www.conf && \
sudo sed -i -e "s/;listen.mode = 0660/listen.mode = 0666/g" /etc/php/7.0/fpm/pool.d/www.conf && \
sudo sed -i -e "s/;listen.owner = nobody/listen.owner = nginx/g" /etc/php/7.0/fpm/pool.d/www.conf && \
sudo sed -i -e "s/;listen.group = nobody/listen.group = nginx/g" /etc/php/7.0/fpm/pool.d/www.conf && \
sudo sed -i -e "s/listen = 127.0.0.1:9000/listen = \/var\/run\/php-fpm7.sock/g" /etc/php/7.0/fpm/pool.d/www.conf

# Cassandra options

sudo sed -i "s/^listen_address:.*/listen_address: 127.0.0.1/" /etc/cassandra/cassandra.yaml
sudo sed -i "s/^\(\s*\)- seeds:.*/\1- seeds: 127.0.0.1/" /etc/cassandra/cassandra.yaml
sudo sed -i "s/^rpc_address:.*/rpc_address: 0.0.0.0/" /etc/cassandra/cassandra.yaml
sudo sed -i "s/^# broadcast_address:.*/broadcast_address: 127.0.0.1/" /etc/cassandra/cassandra.yaml
sudo sed -i "s/^# broadcast_rpc_address:.*/broadcast_rpc_address: 127.0.0.1/" /etc/cassandra/cassandra.yaml
sudo sed -i 's/^start_rpc.*$/start_rpc: true/' /etc/cassandra/cassandra.yaml

# Setup cassandra driver
sudo apt-get install -y php7.0-dev libgmp-dev libpcre3-dev g++ make cmake libssl-dev openssl
if [ ! -f "/usr/lib/php/20151012/cassandra.so" ]; then
  sudo wget -nv http://downloads.datastax.com/cpp-driver/ubuntu/14.04/dependencies/libuv/v1.18.0/libuv_1.18.0-1_amd64.deb
  sudo wget -nv http://downloads.datastax.com/cpp-driver/ubuntu/14.04/dependencies/libuv/v1.18.0/libuv-dev_1.18.0-1_amd64.deb
  sudo wget -nv http://downloads.datastax.com/cpp-driver/ubuntu/14.04/cassandra/v2.8.1/cassandra-cpp-driver_2.8.1-1_amd64.deb
  sudo wget -nv http://downloads.datastax.com/cpp-driver/ubuntu/14.04/cassandra/v2.8.1/cassandra-cpp-driver-dev_2.8.1-1_amd64.deb
  sudo dpkg -i libuv_1.18.0-1_amd64.deb
  sudo dpkg -i libuv-dev_1.18.0-1_amd64.deb
  sudo dpkg -i cassandra-cpp-driver_2.8.1-1_amd64.deb
  sudo dpkg -i cassandra-cpp-driver-dev_2.8.1-1_amd64.deb
fi
sudo pecl install cassandra-1.3.0
sudo echo "extension=cassandra.so" > "/etc/php/7.0/mods-available/cassandra.ini"
sudo phpenmod cassandra

# Setup Mongo driver
sudo apt-get install -y mongodb php7.0-mongodb --force-yes
sudo phpenmod mongodb

# Setup Redis driver
sudo apt-get install -y redis-server php7.0-redis --force-yes
sudo phpenmod redis
# start services
sudo service nginx restart
sudo service php7.0-fpm restart
sudo service cassandra restart
sudo service mongodb restart
sudo service redis-server restart

# Install NodeJS
if ! dpkg --compare-versions `node --version | sed 's/^.//'` ge '6.0'; then
  sudo curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
  sudo apt-get install -y nodejs build-essential
fi
sudo npm install -g npm typescript ts-node

# Install Composer
if [ -f /usr/local/bin/composer ]; then
  composer self-update
else
  wget -nv -O composer-setup.php https://getcomposer.org/installer
  php composer-setup.php --install-dir=/usr/local/bin --filename=composer
  rm composer-setup.php
fi

cd /var/www/Herd/engine
composer install
cd -

# Additional folders
sudo mkdir --parents --mode=0777 /tmp/minds-cache/
sudo mkdir --parents --mode=0777 /data/

cd /var/www/Herd

if [ -f "/var/www/Herd/engine/settings.php" ]; then
  echo "Provisioning Minds…"

  sudo php ./bin/cli.php install \
    --use-existing-settings \
    --graceful-storage-provision \
    --username=minds \
    --password=password \
    --email=minds@dev.minds.io
else
  echo "Installing Minds…"

  sudo php ./bin/regenerateDevKeys.php

  sudo php ./bin/cli.php install \
    --graceful-storage-provision \
    --domain=dev.minds.io \
    --username=minds \
    --password=password \
    --email=minds@dev.minds.io \
    --private-key=/var/www/Herd/.dev/minds.pem \
    --public-key=/var/www/Herd/.dev/minds.pub
fi
