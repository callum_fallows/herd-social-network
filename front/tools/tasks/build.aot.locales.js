"use strict";
var yargs_1 = require("yargs");
var run_1 = require("../utils/run");
var config_1 = require("../config");
var path_1 = require("path");
var glob = require("glob");
var fs_1 = require("fs");
module.exports = function (gulp, plugins) { return function (cb) {
    var locales = yargs_1.argv.locales;
    if (locales === true || !locales) {
        return cb(Error('No locales specified. Please use --locales=XX,YY,ZZ or locales=all'));
    }
    var files = [];
    if (locales === 'all') {
        files = glob.sync(path_1.join(config_1.APP_SRC, 'locale', 'Minds.*.xliff'));
    }
    else {
        files = locales.split(',').map(function (locale) { return path_1.join(config_1.APP_SRC, 'locale', "Minds." + locale + ".xliff"); });
    }
    files.forEach(function (file) { return fs_1.statSync(file); });
    files.forEach(function (file) {
        var lang = (/Minds\.([a-z\-_]+)\.xliff/g).exec(file)[1];
        run_1.run("gulp build --aot --locale=" + lang + " --v=" + config_1.VERSION, {}, false);
    });
    cb();
}; };
//# sourceMappingURL=build.aot.locales.js.map