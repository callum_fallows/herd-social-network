"use strict";
var path_1 = require("path");
var utils_1 = require("../utils");
var config_1 = require("../config");
module.exports = function buildIndex(gulp, plugins) {
    return function () {
        var filename = config_1.LOCALE && config_1.ENV === 'prod' ? config_1.LOCALE_AOT_INDEX_FILE_NAME + "." + config_1.LOCALE + ".php" : 'index.php';
        return gulp.src(path_1.join(config_1.APP_SRC, 'index.php'))
            .pipe(plugins.rename(filename))
            .pipe(injectJs())
            .pipe(injectCss())
            .pipe(plugins.template(utils_1.templateLocals()))
            .pipe(gulp.dest(config_1.APP_DEST));
    };
    function inject() {
        var files = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            files[_i] = arguments[_i];
        }
        return plugins.inject(gulp.src(files, {
            read: false
        }), {
            transform: utils_1.transformPath(plugins, 'dev')
        });
    }
    function injectJs() {
        return inject(path_1.join(config_1.JS_DEST, config_1.JS_PROD_SHIMS_BUNDLE), path_1.join(config_1.JS_DEST, config_1.JS_PROD_APP_BUNDLE));
    }
    function injectCss() {
        return inject(path_1.join(config_1.CSS_DEST, config_1.CSS_PROD_BUNDLE));
    }
};
//# sourceMappingURL=build.index.js.map