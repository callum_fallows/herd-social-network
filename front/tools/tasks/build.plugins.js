"use strict";
var path_1 = require("path");
var util = require("gulp-util");
var chalk = require("chalk");
var fs_1 = require("fs");
var config_1 = require("../config");
module.exports = function buildPlugins(gulp, _plugins, option) {
    return function (cb) {
        if (!fs_1.existsSync(config_1.PLUGINS_DIR)) {
            cb();
            return;
        }
        var plugins = fs_1.readdirSync(config_1.PLUGINS_DIR);
        plugins.map(function (plugin, i) {
            if (plugin.indexOf('_') === 0) {
                if (i == plugins.length - 1) {
                    cb();
                }
                return;
            }
            var path = path_1.join(config_1.PLUGINS_DIR, plugin);
            try {
                var info = require(path_1.join('../../', path, 'plugin.json'));
                if (info.name == '{{plugin.name}}') {
                    throw "Plugin not setup";
                }
                gulp.src(path_1.join(path, 'app', '**', '*'))
                    .pipe(gulp.dest(path_1.join(config_1.APP_SRC, 'src', 'plugins', plugin)));
                util.log(chalk.green('[Y]'), chalk.yellow(plugin));
            }
            catch (error) {
                if (error.code != 'MODULE_NOT_FOUND') {
                    util.log(chalk.red('[E] ' + plugin), error);
                }
            }
            if (i == plugins.length - 1) {
                cb();
            }
        });
    };
};
//# sourceMappingURL=build.plugins.js.map