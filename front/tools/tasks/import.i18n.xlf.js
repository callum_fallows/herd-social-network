"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var config_1 = require("../config");
var yargs_1 = require("yargs");
var child_process_1 = require("child_process");
var path_1 = require("path");
var fs_1 = require("fs");
var run = function (cmd, env, outputAsResult) {
    if (env === void 0) { env = {}; }
    if (outputAsResult === void 0) { outputAsResult = true; }
    var shell = process.env.ComSpec || process.env.SHELL || false, opts = {
        env: __assign({}, process.env, env),
        maxBuffer: 1024 * 1024,
        stdio: outputAsResult ? 'pipe' : 'inherit'
    };
    if (shell) {
        opts['shell'] = shell;
    }
    console.info("[run] " + cmd + " (" + JSON.stringify(env) + ")");
    var result = child_process_1.execSync(cmd, opts);
    if (result instanceof Buffer) {
        result = result.toString();
    }
    return result;
};
function transform(source) {
    fs_1.statSync(source);
    var fileContent = fs_1.readFileSync(source).toString();
    fileContent = fileContent
        .replace(/%([0-9]+)\$s/g, function (substring, match_1) {
        var idx = parseInt(match_1) - 1;
        if (idx < 1) {
            return "{{id=\"INTERPOLATION\"}}";
        }
        return "{{id=\"INTERPOLATION_" + idx + "\"}}";
    })
        .replace(/{{([^}]+)}}/g, "<x $1 />");
    fs_1.writeFileSync(source, fileContent);
}
module.exports = function (gulp, plugins) { return function (cb) {
    if (!yargs_1.argv.locale) {
        cb('Please specify a locale');
    }
    transform(path_1.join(config_1.APP_SRC, 'locale', "Minds." + yargs_1.argv.locale + ".xliff"));
}; };
//# sourceMappingURL=import.i18n.xlf.js.map