"use strict";
var run_1 = require("../utils/run");
var config_1 = require("../config");
module.exports = function (gulp, plugins) { return function (cb) {
    run_1.run("gulp build --aot --v=" + config_1.VERSION, {}, false);
    run_1.run("gulp build --aot --embed --v=" + config_1.VERSION, {}, false);
    run_1.run("gulp build --aot --shims --v=" + config_1.VERSION, {}, false);
    run_1.run("gulp build --aot --locales=all --v=" + config_1.VERSION, {}, false);
    cb();
}; };
//# sourceMappingURL=build.aot.all.js.map