"use strict";
var run_1 = require("../utils/run");
var config_1 = require("../config");
module.exports = function (gulp, plugins) { return function (cb) {
    run_1.run("node_modules/.bin/ngc -p tsconfig-aot.json", {}, false);
    run_1.run("node_modules/.bin/rollup -c rollup-config.ts -o public/js/build-aot." + config_1.VERSION + ".js", {}, false);
    cb();
}; };
//# sourceMappingURL=build.aot.js.map