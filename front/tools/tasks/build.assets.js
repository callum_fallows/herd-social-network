"use strict";
var path_1 = require("path");
var config_1 = require("../config");
module.exports = function buildAssets(gulp, plugins) {
    return function () {
        return gulp.src([
            path_1.join(config_1.APP_SRC, '**'),
            '!' + path_1.join(config_1.APP_SRC, '**', '*.ts'),
            '!' + path_1.join(config_1.APP_SRC, '**', '*.css'),
            '!' + path_1.join(config_1.APP_SRC, '**', '*.html'),
            '!' + path_1.join(config_1.APP_SRC, '**', '*.php'),
        ])
            .pipe(gulp.dest(config_1.APP_DEST));
    };
};
//# sourceMappingURL=build.assets.js.map