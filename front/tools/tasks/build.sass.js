"use strict";
var path_1 = require("path");
var config_1 = require("../config");
var utils_1 = require("../utils");
module.exports = function buildSass(gulp, plugins, option) {
    return function () {
        return gulp.src(path_1.join(config_1.APP_SRC, '**', '*.scss'))
            .pipe(plugins.cssGlobbing({ extensions: ['.scss'] }))
            .pipe(plugins.sass({
            includePaths: [path_1.join(config_1.APP_SRC, 'stylesheets')],
            style: 'compressed'
        }).on('error', plugins.sass.logError))
            .pipe(plugins.autoprefixer(config_1.AUTOPREFIXER_BROWSERS))
            .pipe(plugins.template(utils_1.templateLocals()))
            .pipe(gulp.dest(config_1.APP_DEST));
    };
};
//# sourceMappingURL=build.sass.js.map