"use strict";
var rimraf = require("rimraf");
module.exports = function (gulp, plugins) { return function (cb) {
    rimraf.sync('.ngcli-build/');
    rimraf.sync('.tmp.aot/');
    rimraf.sync('.tmp/');
    cb();
}; };
//# sourceMappingURL=clean.tmp.js.map