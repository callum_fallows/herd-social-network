"use strict";
var yargs_1 = require("yargs");
var run_1 = require("../utils/run");
var config_1 = require("../config");
var path_1 = require("path");
var fs_1 = require("fs");
module.exports = function (gulp, plugins) { return function (cb) {
    var locale = yargs_1.argv.locale;
    if (locale === true || !locale) {
        return cb(Error('No locale specified. Please use --locale=XX'));
    }
    fs_1.statSync(path_1.join(config_1.APP_SRC, 'locale', "Minds." + locale + ".xliff"));
    run_1.run("node_modules/.bin/ngc -p tsconfig-aot.json --i18nFile=./app/locale/Minds." + locale + ".xliff --locale=" + locale + " --i18nFormat=xlf", {}, false);
    run_1.run("node_modules/.bin/rollup -c rollup-config.ts -o public/js/build-aot." + locale + "." + config_1.VERSION + ".js", {}, false);
    cb();
}; };
//# sourceMappingURL=build.aot.locale.js.map