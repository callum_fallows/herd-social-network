"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var gutil = require("gulp-util");
var XML = require("pixl-xml");
var path_1 = require("path");
var yargs_1 = require("yargs");
var fs_1 = require("fs");
var config_1 = require("../config");
var ContextError = (function (_super) {
    __extends(ContextError, _super);
    function ContextError(context, message) {
        var _this = _super.call(this, message) || this;
        _this.message = message;
        _this.type = 'error';
        _this.context = context instanceof Array ?
            context : [context];
        return _this;
    }
    return ContextError;
}(Error));
var ContextWarning = (function (_super) {
    __extends(ContextWarning, _super);
    function ContextWarning() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.type = 'warning';
        return _this;
    }
    return ContextWarning;
}(ContextError));
function parseContext(context) {
    var result = {};
    if (!(context instanceof Array)) {
        throw new Error("Invalid context format: " + JSON.stringify(context));
    }
    context.forEach(function (contextItem) {
        if (!contextItem._Attribs || !contextItem._Attribs['context-type']) {
            throw new Error("Invalid context format: " + JSON.stringify(context));
        }
        result[contextItem._Attribs['context-type']] = contextItem._Data;
    });
    if (!result.sourcefile || !result.linenumber) {
        throw new Error("Invalid context format: " + JSON.stringify(context));
    }
    return result;
}
function parseNote(note) {
    var result = {};
    note.forEach(function (noteItem) {
        if (!noteItem._Attribs || !noteItem._Attribs.from) {
            throw new Error("Invalid note format: " + JSON.stringify(note));
        }
        result[noteItem._Attribs.from] = noteItem._Data;
    });
    return result;
}
var SourceHash = (function () {
    function SourceHash(source, meaning) {
        this._excerpt = '';
        this._hash = require('crypto').createHash('sha256');
        this._hash.update(this.normalizeString("{{meaning=" + meaning + "}" + source));
        this._excerpt = source.replace(/{{[^}]+}}/g, '%');
    }
    SourceHash.prototype.normalizeString = function (text) {
        return text.replace(/[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+/g, ' ').trim();
    };
    SourceHash.prototype.getDigest = function () {
        return this._hash.digest('hex');
    };
    SourceHash.prototype.getExcerpt = function () {
        return this._excerpt;
    };
    return SourceHash;
}());
var SourceCache = (function () {
    function SourceCache() {
        this._cache = {};
    }
    SourceCache.prototype.add = function (source, meaning, data) {
        var sourceHash = new SourceHash(source, meaning), digest = sourceHash.getDigest();
        if (typeof this._cache[digest] === 'undefined') {
            this._cache[digest] = [];
        }
        this._cache[digest].push(__assign({}, data, { excerpt: sourceHash.getExcerpt() }));
        return this;
    };
    SourceCache.prototype.lint = function () {
        var warnings = 0;
        for (var hash in this._cache) {
            var entries = this._cache[hash];
            if (entries.length === 1) {
                continue;
            }
            var ids = entries.map(function (entry) { return entry.id; }).join(', '), context = entries
                .reduce(function (result, entry) {
                result.push.apply(result, entry.context);
                return result;
            }, []);
            gutil.log(gutil.colors.yellow('!'), 'Units', gutil.colors.cyan(ids), 'share the same meaning and content.');
            gutil.log(' ', gutil.colors.gray("\"" + entries[0].excerpt + "\""));
            context.forEach(function (context) {
                gutil.log('    ', gutil.colors.yellow('->'), gutil.colors.gray(context.sourcefile + ":" + context.linenumber));
            });
            console.log('');
            warnings++;
        }
        return warnings;
    };
    return SourceCache;
}());
var Linter = (function () {
    function Linter(units) {
        this.units = units;
        this.sourceCache = new SourceCache();
    }
    Linter.prototype.lint = function (_a) {
        var permissive = _a.permissive;
        var count = 0;
        var successes = 0;
        var warnings = 0;
        var errors = 0;
        console.log('');
        for (var _i = 0, _b = this.units; _i < _b.length; _i++) {
            var unit = _b[_i];
            try {
                this.lintUnit(unit, count);
                successes++;
            }
            catch (e) {
                var prefix = '-';
                if (!e.type || e.type == 'error') {
                    prefix = gutil.colors.red('\u2717');
                    errors++;
                }
                else if (e.type == 'warning') {
                    prefix = gutil.colors.yellow('!');
                    warnings++;
                }
                gutil.log(prefix, e.message);
                if (e.context) {
                    e.context.forEach(function (context, index) {
                        gutil.log(' ', gutil.colors.yellow("#" + index), gutil.colors.gray(context.sourcefile + ":" + context.linenumber));
                    });
                }
                console.log('');
            }
            finally {
                count++;
            }
        }
        if (!permissive) {
            warnings += this.sourceCache.lint();
        }
        gutil.log('Lint finished:');
        gutil.log('=', gutil.colors.magenta("" + count));
        if (successes)
            gutil.log(gutil.colors.green('\u2713'), gutil.colors.magenta("" + successes));
        if (warnings)
            gutil.log(gutil.colors.yellow('!'), gutil.colors.magenta("" + warnings));
        if (errors)
            gutil.log(gutil.colors.red('\u2717'), gutil.colors.magenta("" + errors));
        console.log('');
        return { count: count, successes: successes, warnings: warnings, errors: errors };
    };
    Linter.prototype.lintUnit = function (unit, index) {
        var id = unit._Attribs.id, context = null, note = { meaning: 'GENERIC' };
        if (unit['context-group']) {
            if (unit['context-group'] instanceof Array) {
                context = unit['context-group'].map(function (contextGroupItem) { return parseContext(contextGroupItem.context); });
            }
            else {
                context = [parseContext(unit['context-group'].context)];
            }
        }
        if (!id) {
            throw new ContextError(context, "Missing ID for unit #" + index);
        }
        if (unit.note) {
            note = __assign({}, note, parseNote(unit.note instanceof Array ? unit.note : [unit.note]));
        }
        if (typeof unit.source !== 'string') {
            throw new ContextError(context, "Invalid interpolation value on 'source' for unit " + id + ". Did you use `gulp extract`?");
        }
        this.sourceCache
            .add(unit.source, note.meaning, { id: id, context: context });
        if (/^[a-f0-9]+$/.test(id)) {
            throw new ContextWarning(context, "Unit " + id + " likely is an Angular i18n generated ID");
        }
    };
    return Linter;
}());
module.exports = function (gulp, plugins) { return function (cb) {
    if (!yargs_1.argv.file) {
        throw new Error('Missing file parameter. e.g. Default.xlf');
    }
    var file = path_1.join(config_1.APP_SRC, 'locale', yargs_1.argv.file);
    fs_1.statSync(file);
    var fileContent = fs_1.readFileSync(file).toString();
    var xmlContent = XML.parse(fileContent, {
        preserveAttributes: true,
        preserveDocumentNode: true
    });
    var linter = new Linter(xmlContent.xliff.file.body['trans-unit']);
    var result = linter.lint(yargs_1.argv);
    var valid = !result.errors;
    if (yargs_1.argv.strict) {
        valid = !result.errors && !result.warnings;
    }
    else if (yargs_1.argv.soft) {
        valid = true;
    }
    cb(valid ? null : yargs_1.argv.source + " failed passing the checks.");
}; };
//# sourceMappingURL=lint.i18n.xlf.js.map