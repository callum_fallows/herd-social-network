"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var child_process_1 = require("child_process");
var gutil = require("gulp-util");
exports.run = function (cmd, env, outputAsResult) {
    if (env === void 0) { env = {}; }
    if (outputAsResult === void 0) { outputAsResult = true; }
    var shell = process.env.ComSpec || process.env.SHELL || false, opts = {
        env: __assign({}, process.env, env),
        maxBuffer: 1024 * 1024,
        stdio: outputAsResult ? 'pipe' : 'inherit'
    };
    if (shell) {
        opts['shell'] = shell;
    }
    if (!outputAsResult) {
        gutil.log(gutil.colors.grey('$'), gutil.colors.cyan(cmd), gutil.colors.grey("(" + JSON.stringify(env) + ")"));
    }
    var result = child_process_1.execSync(cmd, opts);
    if (result instanceof Buffer) {
        result = result.toString();
    }
    return result;
};
//# sourceMappingURL=run.js.map