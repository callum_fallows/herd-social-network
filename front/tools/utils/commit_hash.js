"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var run_1 = require("./run");
exports.getCommitHash = function () {
    try {
        var hash = run_1.run('git rev-parse --short HEAD').trim();
        return '_' + hash;
    }
    catch (e) {
        throw new Error('Cannot determine current release. Run it within the Git repository or use the --v flag');
    }
};
//# sourceMappingURL=commit_hash.js.map