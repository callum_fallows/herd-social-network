"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var slash = require("slash");
var path_1 = require("path");
var config_1 = require("../config");
var injectables = [];
function injectableAssetsRef() {
    return injectables;
}
exports.injectableAssetsRef = injectableAssetsRef;
function registerInjectableAssetsRef(paths, target) {
    if (target === void 0) { target = ''; }
    injectables = injectables.concat(paths
        .filter(function (path) { return !/(\.map)$/.test(path); })
        .map(function (path) { return path_1.join(target, slash(path).split('/').pop()); }));
}
exports.registerInjectableAssetsRef = registerInjectableAssetsRef;
function transformPath(plugins, env) {
    return function (filepath) {
        console.log('[filepath]', filepath);
        filepath = filepath.replace("/" + config_1.APP_DEST, '');
        arguments[0] = config_1.APP_CDN + filepath + '?v=' + config_1.VERSION;
        return slash(plugins.inject.transform.apply(plugins.inject.transform, arguments));
    };
}
exports.transformPath = transformPath;
//# sourceMappingURL=template_injectables.js.map