"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./utils/tasks_tools"));
__export(require("./utils/template_injectables"));
__export(require("./utils/template_locals"));
function tsProjectFn(plugins) {
    return plugins.typescript.createProject('tsconfig.json', {
        typescript: require('typescript')
    });
}
exports.tsProjectFn = tsProjectFn;
//# sourceMappingURL=utils.js.map