"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs_1 = require("fs");
var yargs_1 = require("yargs");
var path_1 = require("path");
var commit_hash_1 = require("./utils/commit_hash");
var ENVIRONMENTS = {
    DEVELOPMENT: 'dev',
    PRODUCTION: 'prod'
};
var DEFAULT_ENV = ENVIRONMENTS.DEVELOPMENT;
if (yargs_1.argv.aot) {
    DEFAULT_ENV = ENVIRONMENTS.PRODUCTION;
}
exports.PROJECT_ROOT = path_1.normalize(path_1.join(__dirname, '..'));
exports.ENV = yargs_1.argv['env'] || DEFAULT_ENV;
exports.DEBUG = yargs_1.argv['debug'] || false;
exports.PORT = yargs_1.argv['port'] || 5555;
exports.LIVE_RELOAD_PORT = yargs_1.argv['reload-port'] || 4002;
exports.DOCS_PORT = yargs_1.argv['docs-port'] || 4003;
exports.APP_BASE = yargs_1.argv['base'] || '/';
exports.VERSION = yargs_1.argv['v'] || (exports.ENV !== ENVIRONMENTS.PRODUCTION ? Date.now() : commit_hash_1.getCommitHash());
exports.BOOTSTRAP_MODULE = 'bootstrap';
exports.APP_TITLE = 'Minds App';
exports.APP_SRC = 'app';
exports.APP_CDN = yargs_1.argv['useCdn'] ? '//d15u56mvtglc6v.cloudfront.net/front/public' : '';
exports.ASSETS_SRC = exports.APP_SRC + "/assets_";
exports.TOOLS_DIR = 'tools';
exports.PLUGINS_DIR = '../plugins';
exports.TMP_DIR = '.tmp';
exports.TEST_DEST = 'test';
exports.APP_DEST = "public";
exports.CSS_DEST = exports.APP_DEST + "/stylesheets";
exports.JS_DEST = exports.APP_DEST + "/js";
exports.APP_ROOT = "" + exports.APP_BASE;
exports.CSS_PROD_BUNDLE = 'main.css';
exports.JS_PROD_SHIMS_BUNDLE = 'shims.js';
exports.JS_PROD_APP_BUNDLE = 'app.js';
exports.VERSION_NPM = '2.14.7';
exports.VERSION_NODE = '4.0.0';
exports.LOCALE = yargs_1.argv['locale'] || '';
exports.LOCALE_AOT_INDEX_FILE_NAME = yargs_1.argv['locale-aot-index-file-name'] || 'index-aot';
exports.DEV_NPM_DEPENDENCIES = normalizeDependencies([
    { src: 'systemjs/dist/system-polyfills.js', inject: 'shims', dest: exports.JS_DEST },
    { src: 'core-js/client/shim.min.js', inject: 'shims', dest: exports.JS_DEST },
    { src: 'reflect-metadata/Reflect.js', inject: 'shims', dest: exports.JS_DEST },
    { src: 'zone.js/dist/zone.min.js', inject: 'shims', dest: exports.JS_DEST },
    { src: 'systemjs/dist/system.src.js', inject: 'shims', dest: exports.JS_DEST },
    { src: 'rxjs/bundles/Rx.min.js', inject: 'libs', dest: exports.JS_DEST },
    { src: 'socket.io-client/dist/socket.io.js', inject: 'libs', dest: exports.JS_DEST },
    { src: 'rome/dist/rome.js', inject: 'libs', dest: exports.JS_DEST },
    { src: 'moment/min/moment.min.js', inject: 'libs', dest: exports.JS_DEST },
    { src: 'material-datetime-picker/dist/material-datetime-picker.js', inject: 'libs', dest: exports.JS_DEST },
    { src: 'tinymce/tinymce.min.js', inject: 'async', dest: exports.JS_DEST },
    { src: 'braintree-web/dist/braintree.js', inject: 'async', dest: exports.JS_DEST }
]);
exports.PROD_NPM_DEPENDENCIES = normalizeDependencies([
    { src: 'rome/dist/rome.js', inject: 'libs', dest: exports.JS_DEST },
    { src: 'moment/min/moment.min.js', inject: 'libs', dest: exports.JS_DEST },
    { src: 'material-datetime-picker/dist/material-datetime-picker.js', inject: 'libs', dest: exports.JS_DEST },
    { src: 'medium-editor/dist/js/medium-editor.min.js', inject: 'libs', dest: exports.JS_DEST },
    { src: 'systemjs/dist/system-polyfills.src.js', inject: 'shims' },
    { src: 'core-js/client/shim.min.js', inject: 'shims' },
    { src: 'reflect-metadata/Reflect.js', inject: 'shims' },
    { src: 'zone.js/dist/zone.min.js', inject: 'shims' },
    { src: 'systemjs/dist/system.js', inject: 'shims' },
    { src: 'socket.io-client/dist/socket.io.js', inject: 'libs' },
    { src: 'intl/dist/Intl.min.js', inject: 'shims' },
    { src: 'intl/locale-data/jsonp/en.js', inject: 'shims' },
]);
exports.APP_ASSETS = [
    { src: exports.PROJECT_ROOT + "/app/shims/fontawesome.js", inject: 'shims', dest: 'JS_DEST' },
    { src: exports.CSS_DEST + "/main.css", inject: true, dest: exports.CSS_DEST },
];
exports.DEV_DEPENDENCIES = exports.DEV_NPM_DEPENDENCIES.concat(exports.APP_ASSETS);
exports.PROD_DEPENDENCIES = exports.PROD_NPM_DEPENDENCIES.concat(exports.APP_ASSETS);
exports.AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];
var SYSTEM_PACKAGES = {
    '@angular/animations/browser': { main: '../bundles/animations-browser.umd.js', defaultExtension: 'js' },
    '@angular/animations': { main: 'bundles/animations.umd.js', defaultExtension: 'js' },
    '@angular/common': { main: 'bundles/common.umd.js', defaultExtension: 'js' },
    '@angular/compiler': { main: 'bundles/compiler.umd.js', defaultExtension: 'js' },
    '@angular/core': { main: 'bundles/core.umd.js', defaultExtension: 'js' },
    '@angular/forms': { main: 'bundles/forms.umd.js', defaultExtension: 'js' },
    '@angular/http': { main: 'bundles/http.umd.js', defaultExtension: 'js' },
    '@angular/platform-browser/animations': { main: '../bundles/platform-browser-animations.umd.js', defaultExtension: 'js' },
    '@angular/platform-browser': { main: 'bundles/platform-browser.umd.js', defaultExtension: 'js' },
    '@angular/platform-browser-dynamic': { main: 'bundles/platform-browser-dynamic.umd.js', defaultExtension: 'js' },
    '@angular/platform-server': { main: 'bundles/platform-server.umd.js', defaultExtension: 'js' },
    '@angular/router': { main: 'bundles/router.umd.js', defaultExtension: 'js' },
    'symbol-observable': { main: 'index.js', defaultExtension: 'js' }
};
var SYSTEM_CONFIG_DEV = {
    defaultJSExtensions: true,
    paths: (_a = {},
        _a[exports.BOOTSTRAP_MODULE] = "" + exports.APP_BASE + exports.BOOTSTRAP_MODULE,
        _a['*'] = exports.APP_BASE + "node_modules/*",
        _a),
    packages: SYSTEM_PACKAGES
};
exports.SYSTEM_CONFIG = SYSTEM_CONFIG_DEV;
exports.SYSTEM_BUILDER_CONFIG = {
    defaultJSExtensions: true,
    paths: (_b = {},
        _b[exports.TMP_DIR + "/*"] = exports.TMP_DIR + "/*",
        _b['*'] = 'node_modules/*',
        _b),
    packages: SYSTEM_PACKAGES
};
function normalizeDependencies(deps) {
    deps
        .filter(function (d) { return !/\*/.test(d.src); })
        .forEach(function (d) { return d.src = require.resolve(d.src); });
    return deps;
}
function appVersion() {
    var pkg = JSON.parse(fs_1.readFileSync('package.json').toString());
    return pkg.version;
}
var _a, _b;
//# sourceMappingURL=config.js.map