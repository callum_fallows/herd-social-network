"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var embed_component_1 = require("./embed.component");
var embed_1 = require("./src/router/embed");
var app_1 = require("./src/router/app");
var declarations_1 = require("./src/declarations");
var plugin_declarations_1 = require("./src/plugin-declarations");
var providers_1 = require("./src/services/providers");
var plugin_providers_1 = require("./src/plugin-providers");
var captcha_module_1 = require("./src/modules/captcha/captcha.module");
var common_module_1 = require("./src/common/common.module");
var legacy_module_1 = require("./src/modules/legacy/legacy.module");
var video_module_1 = require("./src/modules/video/video.module");
var report_module_1 = require("./src/modules/report/report.module");
var post_menu_module_1 = require("./src/common/components/post-menu/post-menu.module");
var ban_module_1 = require("./src/modules/ban/ban.module");
var groups_module_1 = require("./src/modules/groups/groups.module");
var blog_module_1 = require("./src/modules/blogs/blog.module");
var notification_module_1 = require("./src/modules/notifications/notification.module");
var EmbedModule = (function () {
    function EmbedModule() {
    }
    return EmbedModule;
}());
EmbedModule = __decorate([
    core_1.NgModule({
        bootstrap: [
            embed_component_1.Embed
        ],
        declarations: [
            embed_component_1.Embed,
            app_1.MINDS_APP_ROUTING_DECLARATIONS,
            embed_1.MINDS_EMBED_ROUTING_DECLARATIONS,
            declarations_1.MINDS_DECLARATIONS,
            plugin_declarations_1.MINDS_PLUGIN_DECLARATIONS,
        ],
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.ReactiveFormsModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            router_1.RouterModule.forRoot(embed_1.MindsEmbedRoutes, { initialNavigation: false, useHash: true }),
            captcha_module_1.CaptchaModule,
            common_module_1.CommonModule,
            legacy_module_1.LegacyModule,
            video_module_1.VideoModule,
            report_module_1.ReportModule,
            ban_module_1.BanModule,
            post_menu_module_1.PostMenuModule,
            groups_module_1.GroupsModule,
            blog_module_1.BlogModule,
            notification_module_1.NotificationModule
        ],
        providers: [
            embed_1.MindsEmbedRoutingProviders,
            providers_1.MINDS_PROVIDERS,
            plugin_providers_1.MINDS_PLUGIN_PROVIDERS,
        ],
        schemas: [
            core_1.CUSTOM_ELEMENTS_SCHEMA
        ]
    })
], EmbedModule);
exports.EmbedModule = EmbedModule;
//# sourceMappingURL=embed.module.js.map