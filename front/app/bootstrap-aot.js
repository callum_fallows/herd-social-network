"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var app_module_ngfactory_1 = require("../.tmp.aot/app/app.module.ngfactory");
core_1.enableProdMode();
platform_browser_1.platformBrowser().bootstrapModuleFactory(app_module_ngfactory_1.MindsModuleNgFactory);
//# sourceMappingURL=bootstrap-aot.js.map