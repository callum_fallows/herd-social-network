"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var notification_service_1 = require("./src/modules/notifications/notification.service");
var analytics_1 = require("./src/services/analytics");
var sockets_1 = require("./src/services/sockets");
var session_1 = require("./src/services/session");
var login_referrer_service_1 = require("./src/services/login-referrer.service");
var scroll_to_top_service_1 = require("./src/services/scroll-to-top.service");
var context_service_1 = require("./src/services/context.service");
var Minds = (function () {
    function Minds(notificationService, scrollToTop, analytics, sockets, loginReferrer, context) {
        this.notificationService = notificationService;
        this.scrollToTop = scrollToTop;
        this.analytics = analytics;
        this.sockets = sockets;
        this.loginReferrer = loginReferrer;
        this.context = context;
        this.minds = window.Minds;
        this.session = session_1.SessionFactory.build();
        this.name = 'Minds';
    }
    Minds.prototype.ngOnInit = function () {
        var _this = this;
        this.notificationService.getNotifications();
        this.session.isLoggedIn(function (is) {
            if (is) {
                if (_this.minds.user.language !== _this.minds.language) {
                    console.log('[app]:: language change', _this.minds.user.language, _this.minds.language);
                    window.location.reload(true);
                }
            }
        });
        this.loginReferrer
            .avoid([
            '/login',
            '/logout',
            '/register',
            '/forgot-password',
        ])
            .listen();
        this.scrollToTop.listen();
        this.context.listen();
    };
    Minds.prototype.ngOnDestroy = function () {
        this.loginReferrer.unlisten();
        this.scrollToTop.unlisten();
    };
    return Minds;
}());
Minds = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-app',
        templateUrl: 'src/controllers/index.html'
    }),
    __metadata("design:paramtypes", [notification_service_1.NotificationService,
        scroll_to_top_service_1.ScrollToTopService,
        analytics_1.AnalyticsService,
        sockets_1.SocketsService,
        login_referrer_service_1.LoginReferrerService,
        context_service_1.ContextService])
], Minds);
exports.Minds = Minds;
//# sourceMappingURL=app.component.js.map