"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var embed_module_ngfactory_1 = require("../.tmp.aot/app/embed.module.ngfactory");
core_1.enableProdMode();
platform_browser_1.platformBrowser().bootstrapModuleFactory(embed_module_ngfactory_1.EmbedModuleNgFactory);
//# sourceMappingURL=bootstrap-embed-aot.js.map