"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var i18n_providers_1 = require("./src/i18n-providers");
var app_module_1 = require("./app.module");
var embed_module_1 = require("./embed.module");
if (String('<%= ENV %>') === 'prod') {
    core_1.enableProdMode();
}
i18n_providers_1.getTranslationProviders().then(function (providers) {
    platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(window.Minds.MindsContext === 'embed' ? embed_module_1.EmbedModule : app_module_1.MindsModule, { providers: providers });
});
//# sourceMappingURL=bootstrap.js.map