"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OptimizedResize = (function () {
    function OptimizedResize() {
        this.callbacks = [];
        this.running = false;
    }
    OptimizedResize.prototype.isRunning = function () {
        return this.running;
    };
    OptimizedResize.prototype.resize = function () {
        if (!this.running) {
            this.running = true;
            if (window.requestAnimationFrame) {
                window.requestAnimationFrame(this.runCallbacks.bind(this));
            }
            else {
                setTimeout(this.runCallbacks.bind(this), 66);
            }
        }
    };
    OptimizedResize.prototype.runCallbacks = function () {
        this.callbacks.forEach(function (callback) {
            callback();
        });
        this.running = false;
    };
    OptimizedResize.prototype.addCallback = function (callback) {
        if (callback) {
            this.callbacks.push(callback);
        }
    };
    OptimizedResize.prototype.add = function (callback) {
        if (!this.callbacks.length) {
            window.addEventListener('resize', this.resize.bind(this));
        }
        this.addCallback(callback);
    };
    return OptimizedResize;
}());
exports.optimizedResize = new OptimizedResize();
//# sourceMappingURL=optimized-resize.js.map