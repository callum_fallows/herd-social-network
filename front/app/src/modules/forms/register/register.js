"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var api_1 = require("../../../services/api");
var session_1 = require("../../../services/session");
var recaptcha_component_1 = require("../../../modules/captcha/recaptcha/recaptcha.component");
var RegisterForm = (function () {
    function RegisterForm(client, fb) {
        this.client = client;
        this.session = session_1.SessionFactory.build();
        this.errorMessage = '';
        this.twofactorToken = '';
        this.hideLogin = false;
        this.inProgress = false;
        this.minds = window.Minds;
        this.done = new core_1.EventEmitter();
        this.form = fb.group({
            username: ['', forms_1.Validators.required],
            email: ['', forms_1.Validators.required],
            password: ['', forms_1.Validators.required],
            password2: ['', forms_1.Validators.required],
            captcha: ['']
        });
    }
    RegisterForm.prototype.register = function (e) {
        var _this = this;
        e.preventDefault();
        this.errorMessage = '';
        if (this.form.value.password !== this.form.value.password2) {
            if (this.reCaptcha) {
                this.reCaptcha.reset();
            }
            this.errorMessage = 'Passwords must match.';
            return;
        }
        this.form.value.referrer = this.referrer;
        this.inProgress = true;
        var self = this;
        this.client.post('api/v1/register', this.form.value)
            .then(function (data) {
            _this.inProgress = false;
            self.session.login(data.user);
            _this.done.next(data.user);
        })
            .catch(function (e) {
            console.log(e);
            _this.inProgress = false;
            if (_this.reCaptcha) {
                _this.reCaptcha.reset();
            }
            if (e.status === 'failed') {
                self.errorMessage = 'Incorrect username/password. Please try again.';
                self.session.logout();
            }
            if (e.status === 'error') {
                self.errorMessage = e.message;
                self.session.logout();
            }
            return;
        });
    };
    return RegisterForm;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], RegisterForm.prototype, "referrer", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], RegisterForm.prototype, "done", void 0);
__decorate([
    core_1.ViewChild('reCaptcha'),
    __metadata("design:type", recaptcha_component_1.ReCaptchaComponent)
], RegisterForm.prototype, "reCaptcha", void 0);
RegisterForm = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-form-register',
        templateUrl: 'register.html'
    }),
    __metadata("design:paramtypes", [api_1.Client, forms_1.FormBuilder])
], RegisterForm);
exports.RegisterForm = RegisterForm;
//# sourceMappingURL=register.js.map