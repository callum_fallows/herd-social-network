"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var api_1 = require("../../../services/api");
var session_1 = require("../../../services/session");
var LoginForm = (function () {
    function LoginForm(client, fb, zone) {
        this.client = client;
        this.zone = zone;
        this.session = session_1.SessionFactory.build();
        this.errorMessage = '';
        this.twofactorToken = '';
        this.hideLogin = false;
        this.inProgress = false;
        this.minds = window.Minds;
        this.done = new core_1.EventEmitter();
        this.doneRegistered = new core_1.EventEmitter();
        this.form = fb.group({
            username: ['', forms_1.Validators.required],
            password: ['', forms_1.Validators.required]
        });
    }
    LoginForm.prototype.login = function () {
        var _this = this;
        if (this.inProgress)
            return;
        this.errorMessage = '';
        this.inProgress = true;
        var self = this;
        this.client.post('api/v1/authenticate', { username: this.form.value.username, password: this.form.value.password })
            .then(function (data) {
            _this.inProgress = false;
            _this.session.login(data.user);
            _this.done.next(data.user);
        })
            .catch(function (e) {
            _this.inProgress = false;
            if (e.status === 'failed') {
                self.errorMessage = 'Incorrect username/password. Please try again.';
                self.session.logout();
            }
            if (e.status === 'error') {
                if (e.message === 'LoginException:BannedUser') {
                    self.errorMessage = 'You are not allowed to login.';
                    self.session.logout();
                    return;
                }
                self.twofactorToken = e.message;
                self.hideLogin = true;
            }
        });
    };
    LoginForm.prototype.twofactorAuth = function (code) {
        var _this = this;
        this.client.post('api/v1/twofactor/authenticate', { token: this.twofactorToken, code: code.value })
            .then(function (data) {
            _this.session.login(data.user);
            _this.done.next(data.user);
        })
            .catch(function (e) {
            _this.errorMessage = 'Sorry, we couldn\'t verify your two factor code. Please try logging again.';
            _this.twofactorToken = '';
            _this.hideLogin = false;
        });
    };
    LoginForm.prototype.loginWithFb = function () {
        var _this = this;
        window.onSuccessCallback = function (user) {
            _this.zone.run(function () {
                _this.session.login(user);
                if (user['new']) {
                    _this.doneRegistered.next(user);
                }
                if (!user['new']) {
                    _this.done.next(user);
                }
            });
        };
        window.onErrorCallback = function (reason) {
            if (reason) {
                alert(reason);
            }
        };
        window.open(this.minds.site_url + 'api/v1/thirdpartynetworks/facebook/login', 'Login with Facebook', 'toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=no, width=600, height=400, top=100, left=100');
    };
    return LoginForm;
}());
LoginForm = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-form-login',
        outputs: ['done', 'doneRegistered'],
        templateUrl: 'login.html'
    }),
    __metadata("design:paramtypes", [api_1.Client, forms_1.FormBuilder, core_1.NgZone])
], LoginForm);
exports.LoginForm = LoginForm;
//# sourceMappingURL=login.js.map