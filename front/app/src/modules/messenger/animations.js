"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
exports.animations = [
    core_1.trigger('attentionNeeded', [
        core_1.transition('* => *', [
            core_1.style({ transform: 'perspective(1px) translateZ(0)', transformOrigin: '0 100%' }),
            core_1.animate(1000, core_1.keyframes([
                core_1.style({ transform: 'skew(0deg)', offset: 0.0000 }),
                core_1.style({ transform: 'skew(-12deg)', offset: 0.1665 }),
                core_1.style({ transform: 'skew(10deg)', offset: 0.3333 }),
                core_1.style({ transform: 'skew(-6deg)', offset: 0.4995 }),
                core_1.style({ transform: 'skew(4deg)', offset: 0.6666 }),
                core_1.style({ transform: 'skew(-2deg)', offset: 0.8325 }),
                core_1.style({ transform: 'skew(0deg)', offset: 1.0000 }),
            ]))
        ])
    ])
];
//# sourceMappingURL=animations.js.map