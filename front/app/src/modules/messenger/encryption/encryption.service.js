"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var storage_1 = require("../../../services/storage");
var MessengerEncryptionService = (function () {
    function MessengerEncryptionService(client, storage) {
        this.client = client;
        this.storage = storage;
        this.reKeying = false;
        this.on = false;
        this.setup = false;
    }
    MessengerEncryptionService._ = function (client) {
        return new MessengerEncryptionService(client, new storage_1.Storage());
    };
    MessengerEncryptionService.prototype.isOn = function () {
        this.on = !!this.storage.get('encryption-password');
        return this.on;
    };
    MessengerEncryptionService.prototype.unlock = function (password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.client.post('api/v2/keys/unlock', { password: password })
                .then(function (response) {
                _this.storage.set('encryption-password', response.password);
                _this.on = true;
                resolve();
            })
                .catch(function () {
                reject();
            });
        });
    };
    MessengerEncryptionService.prototype.isSetup = function () {
        this.setup = window.Minds.user.chat;
        return this.setup;
    };
    MessengerEncryptionService.prototype.doSetup = function (password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.client.post('api/v2/keys/setup', { password: password, download: false })
                .then(function (response) {
                _this.storage.set('encryption-password', response.password);
                _this.setup = true;
                _this.on = true;
                resolve();
            })
                .catch(function () {
                reject();
            });
        });
    };
    MessengerEncryptionService.prototype.rekey = function (password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.client.post('api/v2/keys/setup', { password: password, download: false })
                .then(function (response) {
                _this.storage.set('encryption-password', response.password);
                _this.setup = true;
                _this.on = true;
                _this.reKeying = false;
                resolve();
            })
                .catch(function () {
                reject();
            });
        });
    };
    MessengerEncryptionService.prototype.getEncryptionPassword = function () {
        return this.storage.get('encryption-password');
    };
    MessengerEncryptionService.prototype.logout = function () {
        this.storage.destroy('encryption-password');
        this.on = false;
    };
    return MessengerEncryptionService;
}());
exports.MessengerEncryptionService = MessengerEncryptionService;
//# sourceMappingURL=encryption.service.js.map