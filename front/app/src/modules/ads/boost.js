"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../services/api");
var storage_1 = require("../../services/storage");
var BoostAds = (function () {
    function BoostAds(client, storage) {
        this.client = client;
        this.storage = storage;
        this.handler = 'content';
        this.limit = 2;
        this.offset = '';
        this.boosts = [];
    }
    BoostAds.prototype.ngOnInit = function () {
        this.fetch();
    };
    BoostAds.prototype.fetch = function () {
        var _this = this;
        if (this.storage.get('boost:offset:sidebar'))
            this.offset = this.storage.get('boost:offset:sidebar');
        this.client.get('api/v1/boost/fetch/' + this.handler, { limit: this.limit, offset: this.offset })
            .then(function (response) {
            if (!response.boosts) {
                return;
            }
            _this.boosts = response.boosts;
            if (response['load-next'])
                _this.storage.set('boost:offset:sidebar', response['load-next']);
        });
    };
    return BoostAds;
}());
BoostAds = __decorate([
    core_1.Component({
        selector: 'm-ads-boost',
        inputs: ['handler', 'limit'],
        template: "\n    <h3 class=\"m-ad-boost-heading mdl-color-text--black\">\n      <i class=\"material-icons\">trending_up</i> <!-- i18n: @@ADS__PROMOTEED_CONTENT -->Boosted content<!-- /i18n -->\n    </h3>\n    <div class=\"m-ad-boost-entity\" *ngFor=\"let entity of boosts\">\n      <minds-card [object]=\"entity\" hostClass=\"mdl-card mdl-shadow--8dp\"></minds-card>\n    </div>\n  ",
        host: {
            'class': 'm-ad-block m-ad-block-boosts'
        }
    }),
    __metadata("design:paramtypes", [api_1.Client, storage_1.Storage])
], BoostAds);
exports.BoostAds = BoostAds;
//# sourceMappingURL=boost.js.map