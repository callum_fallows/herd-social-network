"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var title_1 = require("../../services/ux/title");
var api_1 = require("../../services/api");
var session_1 = require("../../services/session");
var context_service_1 = require("../../services/context.service");
var Blog = (function () {
    function Blog(client, route, title, context) {
        this.client = client;
        this.route = route;
        this.title = title;
        this.context = context;
        this.offset = '';
        this.moreData = true;
        this.inProgress = false;
        this.blogs = [];
        this.session = session_1.SessionFactory.build();
        this._filter = 'featured';
        this._filter2 = '';
    }
    Blog.prototype.ngOnInit = function () {
        var _this = this;
        this.title.setTitle('Blogs');
        this.minds = window.Minds;
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            _this._filter = params['filter'];
            switch (_this._filter) {
                case 'trending':
                    _this.title.setTitle('Trending Blogs');
                    break;
                case 'featured':
                    _this.title.setTitle('Featured Blogs');
                    break;
                case 'all':
                    break;
                case 'owner':
                    break;
                default:
                    _this._filter2 = _this._filter;
                    _this._filter = 'owner';
            }
            _this.inProgress = false;
            _this.offset = '';
            _this.moreData = true;
            _this.blogs = [];
            _this.load();
        });
        this.context.set('object:blog');
    };
    Blog.prototype.ngOnDestroy = function () {
        this.paramsSubscription.unsubscribe();
    };
    Blog.prototype.load = function (refresh) {
        if (refresh === void 0) { refresh = false; }
        if (this.inProgress)
            return false;
        var self = this;
        this.inProgress = true;
        this.client.get('api/v1/blog/' + this._filter + '/' + this._filter2, { limit: 12, offset: this.offset })
            .then(function (response) {
            if (!response.blogs) {
                self.moreData = false;
                self.inProgress = false;
                return false;
            }
            if (refresh) {
                self.blogs = response.blogs;
            }
            else {
                if (self.offset)
                    response.blogs.shift();
                for (var _i = 0, _a = response.blogs; _i < _a.length; _i++) {
                    var blog = _a[_i];
                    self.blogs.push(blog);
                }
            }
            self.offset = response['load-next'];
            self.inProgress = false;
        })
            .catch(function (e) {
            self.inProgress = false;
        });
    };
    return Blog;
}());
Blog = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-blog',
        templateUrl: 'list.html'
    }),
    __metadata("design:paramtypes", [api_1.Client, router_1.ActivatedRoute, title_1.MindsTitle, context_service_1.ContextService])
], Blog);
exports.Blog = Blog;
var view_1 = require("./view/view");
exports.BlogView = view_1.BlogView;
var infinite_1 = require("./view/infinite");
exports.BlogViewInfinite = infinite_1.BlogViewInfinite;
var edit_1 = require("./edit/edit");
exports.BlogEdit = edit_1.BlogEdit;
//# sourceMappingURL=blog.js.map