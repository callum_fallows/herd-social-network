"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var common_module_1 = require("../../common/common.module");
var modals_module_1 = require("../modals/modals.module");
var ads_module_1 = require("../ads/ads.module");
var legacy_module_1 = require("../legacy/legacy.module");
var post_menu_module_1 = require("../../common/components/post-menu/post-menu.module");
var blog_1 = require("./blog");
var card_1 = require("./card/card");
var view_1 = require("./view/view");
var wire_module_1 = require("../wire/wire.module");
var routes = [
    { path: 'blog/view/:guid/:title', component: blog_1.BlogViewInfinite },
    { path: 'blog/view/:guid', component: blog_1.BlogViewInfinite },
    { path: 'blog/edit/:guid', component: blog_1.BlogEdit },
    { path: 'blog/:filter', component: blog_1.Blog },
    { path: ':username/blog/:slugid', component: blog_1.BlogViewInfinite },
];
var BlogModule = (function () {
    function BlogModule() {
    }
    return BlogModule;
}());
BlogModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            common_module_1.CommonModule,
            router_1.RouterModule.forChild(routes),
            forms_1.FormsModule,
            forms_1.ReactiveFormsModule,
            modals_module_1.ModalsModule,
            ads_module_1.AdsModule,
            legacy_module_1.LegacyModule,
            post_menu_module_1.PostMenuModule,
            wire_module_1.WireModule
        ],
        declarations: [
            view_1.BlogView,
            card_1.BlogCard,
            blog_1.BlogViewInfinite,
            blog_1.BlogEdit,
            blog_1.Blog,
        ],
        exports: [
            view_1.BlogView,
            card_1.BlogCard,
            blog_1.BlogViewInfinite,
            blog_1.BlogEdit,
            blog_1.Blog,
        ],
        entryComponents: [
            card_1.BlogCard
        ]
    })
], BlogModule);
exports.BlogModule = BlogModule;
//# sourceMappingURL=blog.module.js.map