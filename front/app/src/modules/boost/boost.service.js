"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../services/api");
var session_1 = require("../../services/session");
var BoostService = (function () {
    function BoostService(client) {
        this.client = client;
        this.session = session_1.SessionFactory.build();
    }
    BoostService.prototype.load = function (type, filter, _a) {
        var _b = _a === void 0 ? {} : _a, limit = _b.limit, offset = _b.offset;
        return this.client.get("api/v1/boost/" + type + "/" + filter, {
            limit: limit || 12,
            offset: offset || ''
        })
            .then(function (_a) {
            var boosts = _a.boosts, loadNext = _a["load-next"];
            return {
                boosts: boosts && boosts.length ? boosts : [],
                loadNext: loadNext || ''
            };
        });
    };
    BoostService.prototype.accept = function (boost) {
        if (this.getBoostType(boost) !== 'p2p') {
            return Promise.resolve(false);
        }
        boost.state = 'accepted';
        return this.client.put("api/v1/boost/peer/" + boost.guid)
            .then(function () {
            return true;
        })
            .catch(function (e) {
            boost.state = 'created';
            return false;
        });
    };
    BoostService.prototype.canAccept = function (boost) {
        return boost.state === 'created' && this.getBoostType(boost) === 'p2p' && this.isIncoming(boost);
    };
    BoostService.prototype.reject = function (boost) {
        if (this.getBoostType(boost) !== 'p2p') {
            return Promise.resolve(false);
        }
        boost.state = 'rejected';
        return this.client.delete("api/v1/boost/peer/" + boost.guid)
            .then(function () {
            return true;
        })
            .catch(function (e) {
            boost.state = 'created';
            return false;
        });
    };
    BoostService.prototype.canReject = function (boost) {
        return boost.state === 'created' && this.getBoostType(boost) === 'p2p' && this.isIncoming(boost);
    };
    BoostService.prototype.revoke = function (boost) {
        var revokeEndpoint;
        if (this.getBoostType(boost) === 'p2p') {
            revokeEndpoint = "api/v1/boost/peer/" + boost.guid + "/revoke";
        }
        else {
            revokeEndpoint = "api/v1/boost/" + boost.handler + "/" + boost.guid + "/revoke";
        }
        boost.state = 'revoked';
        return this.client.delete(revokeEndpoint)
            .then(function () {
            return true;
        })
            .catch(function (e) {
            boost.state = 'created';
            return false;
        });
    };
    BoostService.prototype.canRevoke = function (boost) {
        return boost.state === 'created' && ((this.getBoostType(boost) === 'p2p' && !this.isIncoming(boost)) ||
            (this.getBoostType(boost) !== 'p2p'));
    };
    BoostService.prototype.getBoostType = function (boost) {
        if (boost.handler) {
            return boost.handler;
        }
        else if (boost.destination) {
            return 'p2p';
        }
        return false;
    };
    BoostService.prototype.isIncoming = function (boost) {
        return boost.destination.guid === this.session.getLoggedInUser().guid;
    };
    return BoostService;
}());
BoostService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [api_1.Client])
], BoostService);
exports.BoostService = BoostService;
//# sourceMappingURL=boost.service.js.map