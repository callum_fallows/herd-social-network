"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../../../services/api");
var session_1 = require("../../../../services/session");
var BoostConsoleBooster = (function () {
    function BoostConsoleBooster(client) {
        this.client = client;
        this.inProgress = false;
        this.loaded = false;
        this.open = false;
        this.posts = [];
        this.media = [];
        this.session = session_1.SessionFactory.build();
    }
    BoostConsoleBooster.prototype.ngOnInit = function () {
        this.load();
    };
    BoostConsoleBooster.prototype.load = function (refresh) {
        var _this = this;
        if (this.inProgress) {
            return Promise.resolve(false);
        }
        if (!refresh && this.loaded) {
            return Promise.resolve(true);
        }
        this.inProgress = true;
        var promises = [
            this.client.get('api/v1/newsfeed/personal'),
            this.client.get('api/v1/entities/owner')
        ];
        return Promise.all(promises)
            .then(function (responses) {
            _this.loaded = true;
            _this.inProgress = false;
            _this.posts = responses[0].activity || [];
            _this.media = responses[1].entities || [];
        })
            .catch(function (e) {
            _this.inProgress = false;
            return false;
        });
    };
    BoostConsoleBooster.prototype.toggle = function () {
        this.open = !this.open;
        if (this.open) {
            this.load();
        }
    };
    return BoostConsoleBooster;
}());
__decorate([
    core_1.Input('toggled'),
    __metadata("design:type", Boolean)
], BoostConsoleBooster.prototype, "open", void 0);
__decorate([
    core_1.Input('type'),
    __metadata("design:type", String)
], BoostConsoleBooster.prototype, "type", void 0);
BoostConsoleBooster = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'm-boost-console-booster',
        templateUrl: 'booster.component.html'
    }),
    __metadata("design:paramtypes", [api_1.Client])
], BoostConsoleBooster);
exports.BoostConsoleBooster = BoostConsoleBooster;
//# sourceMappingURL=booster.component.js.map