"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var session_1 = require("../../../services/session");
var BoostConsoleComponent = (function () {
    function BoostConsoleComponent(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.session = session_1.SessionFactory.build();
    }
    BoostConsoleComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.activatedRoute.snapshot.params['type']) {
            this.router.navigateByUrl('/wallet/boost/newsfeed', { replaceUrl: true });
            return;
        }
        this.activatedRoute.params.subscribe(function (params) {
            if (params['type']) {
                _this.type = params['type'];
            }
            if (params['toggled']) {
                _this.toggled = params['toggled'];
            }
            _this.filter = params['filter'] || 'inbox';
        });
        if (!this.type) {
            this.type = 'newsfeed';
        }
    };
    return BoostConsoleComponent;
}());
BoostConsoleComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'm-boost-console',
        templateUrl: 'console.component.html'
    }),
    __metadata("design:paramtypes", [router_1.Router,
        router_1.ActivatedRoute])
], BoostConsoleComponent);
exports.BoostConsoleComponent = BoostConsoleComponent;
//# sourceMappingURL=console.component.js.map