"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var client_1 = require("../../../services/api/client");
var client_mock_spec_1 = require("../../../../tests/client-mock.spec");
var abbr_1 = require("../../../common/pipes/abbr");
var material_mock_spec_1 = require("../../../../tests/material-mock.spec");
var material_switch_mock_spec_1 = require("../../../../tests/material-switch-mock.spec");
var overlay_modal_service_mock_spec_1 = require("../../../../tests/overlay-modal-service-mock.spec");
var overlay_modal_1 = require("../../../services/ux/overlay-modal");
var scheduler_1 = require("../../../common/components/scheduler/scheduler");
var creator_component_1 = require("./creator.component");
var boost_service_1 = require("../boost.service");
var StripeCheckoutMock = (function () {
    function StripeCheckoutMock() {
        this.inputed = new core_1.EventEmitter;
        this.done = new core_1.EventEmitter;
        this.amount = 0;
        this.gateway = 'merchants';
        this.useMDLStyling = true;
        this.useCreditCard = true;
        this.useBitcoin = false;
    }
    return StripeCheckoutMock;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], StripeCheckoutMock.prototype, "amount", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], StripeCheckoutMock.prototype, "merchant_guid", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], StripeCheckoutMock.prototype, "gateway", void 0);
__decorate([
    core_1.Input('useMDLStyling'),
    __metadata("design:type", Boolean)
], StripeCheckoutMock.prototype, "useMDLStyling", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], StripeCheckoutMock.prototype, "useCreditCard", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], StripeCheckoutMock.prototype, "useBitcoin", void 0);
StripeCheckoutMock = __decorate([
    core_1.Component({
        selector: 'minds-payments-stripe-checkout',
        outputs: ['inputed', 'done'],
        template: ''
    })
], StripeCheckoutMock);
exports.StripeCheckoutMock = StripeCheckoutMock;
describe('BoostCreatorComponent', function () {
    var boostComponent;
    var fixture;
    var submitSection;
    var boostSubmitButton;
    var loadCategoriesSpy;
    window.Minds.categories = {
        "art": "Art",
        "animals": "Animals",
        "music": "Music",
        "science": "Science",
        "technology": "Technology",
        "gaming": "Gaming",
        "history": "History",
        "nature": "Nature",
        "news": "News",
        "politics": "Politics",
        "comedy": "Comedy",
        "film": "Film ",
        "education": "Education",
        "sports": "Sports",
        "food": "Food",
        "modeling": "Modeling",
        "spirituality": "Spirituality ",
        "travel": "Travel",
        "health": "Health"
    };
    var boostUser = {
        'guid': '123',
        'type': 'user',
        'subtype': false,
        'time_created': '1500037446',
        'time_updated': false,
        'container_guid': '0',
        'owner_guid': '0',
        'site_guid': false,
        'access_id': '2',
        'name': 'minds',
        'username': 'minds',
        'language': 'en',
        'icontime': false,
        'legacy_guid': false,
        'featured_id': false,
        'banned': 'no',
        'website': '',
        'briefdescription': 'test',
        'dob': '',
        'gender': '',
        'city': '',
        'merchant': {
            'service': 'stripe',
            'id': 'acct_1ApIzEA26BgQpK9C',
            'exclusive': { 'background': 1502453050, 'intro': '' }
        },
        'boostProPlus': false,
        'fb': false,
        'mature': 0,
        'monetized': '',
        'signup_method': false,
        'social_profiles': [],
        'feature_flags': false,
        'programs': ['affiliate'],
        'plus': false,
        'verified': false,
        'disabled_boost': false,
        'categories': ['news', 'film', 'spirituality'],
        'wire_rewards': null,
        'subscribed': false,
        'subscriber': false,
        'subscribers_count': 1,
        'subscriptions_count': 1,
        'impressions': 337,
        'boost_rating': '2'
    };
    var boostActivity = {
        type: 'activity',
        guid: '12345',
        owner_guid: '54321'
    };
    var boostTargetUser = {
        'guid': '100000000000000063',
        'type': 'user',
        'username': 'mark',
        'merchant': {
            'service': 'stripe',
            'id': 'acct_19QgKYEQcGuFgRfS',
            'exclusive': {
                'enabled': true,
                'amount': 2,
                'intro': ''
            }
        },
        'subscribers_count': 51467,
        'impressions': 758644,
        'boostProPlus': '1',
        'fb': {
            'uuid': '578128092345756',
            'name': 'Mark\'s test page'
        }
    };
    var boostBlog = {
        guid: '111111111',
        type: 'object',
        title: 'MOCK BLOG ENTRY',
        description: 'THIS IS A MOCK BLOCK ENTRY :)',
        ownerObj: boostUser,
        owner_guid: boostUser.guid
    };
    function getPaymentMethodItem(i) {
        return fixture.debugElement.query(platform_browser_1.By.css("section.m-boost--creator-section-payment > ul.m-boost--creator-selector > li:nth-child(" + i + ")"));
    }
    function getAmountInput() {
        return fixture.debugElement.query(platform_browser_1.By.css('input.m-boost--creator-wide-input--edit'));
    }
    function getAmountLabel() {
        return fixture.debugElement.query(platform_browser_1.By.css('span.m-boost--creator-wide-input--label'));
    }
    function getErrorLabel() {
        return fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--submit-error'));
    }
    function getBoostTypesList() {
        return fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-type > ul.m-boost--creator-selector'));
    }
    function getBoostTypeItem(i) {
        return fixture.debugElement.query(platform_browser_1.By.css("section.m-boost--creator-section-type > ul.m-boost--creator-selector > li:nth-child(" + i + ")"));
    }
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                material_mock_spec_1.MaterialMock,
                material_switch_mock_spec_1.MaterialSwitchMock,
                abbr_1.AbbrPipe,
                scheduler_1.Scheduler,
                StripeCheckoutMock,
                creator_component_1.BoostCreatorComponent
            ],
            imports: [forms_1.FormsModule],
            providers: [
                { provide: client_1.Client, useValue: client_mock_spec_1.clientMock },
                boost_service_1.BoostService,
                { provide: overlay_modal_1.OverlayModalService, useValue: overlay_modal_service_mock_spec_1.overlayModalServiceMock }
            ]
        }).compileComponents();
    }));
    beforeEach(function (done) {
        jasmine.MAX_PRETTY_PRINT_DEPTH = 10;
        jasmine.clock().uninstall();
        jasmine.clock().install();
        fixture = testing_1.TestBed.createComponent(creator_component_1.BoostCreatorComponent);
        boostComponent = fixture.componentInstance;
        client_mock_spec_1.clientMock.response = {};
        client_mock_spec_1.clientMock.response["api/v1/boost/rates"] = {
            'status': 'success',
            'balance': 28540,
            'hasPaymentMethod': false,
            'rate': 1,
            'cap': 5000,
            'min': 500,
            'priority': 1,
            'usd': 1000,
            'minUsd': 1
        };
        submitSection = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-submit'));
        boostSubmitButton = fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--submit > button.m-boost--creator-button'));
        fixture.detectChanges();
        if (fixture.isStable()) {
            done();
        }
        else {
            fixture.whenStable().then(function () {
                done();
            });
        }
    });
    afterEach(function () {
        jasmine.clock().uninstall();
    });
    it('should have a title', function () {
        var title = fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--header h2'));
        expect(title).not.toBeNull();
        expect(title.nativeElement.textContent).toContain('Boost');
    });
    it('should have a boost type selection section', function () {
        var boostTypeSection = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-type'));
        expect(boostTypeSection).not.toBeNull();
    });
    it('boost type selection section should have a title that says \'Boost Type\'', function () {
        var boostTypeTitle = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-type > .m-boost--creator-section-title--small'));
        expect(boostTypeTitle).not.toBeNull();
        expect(boostTypeTitle.nativeElement.textContent).toContain('Boost Type');
    });
    it('should only offer "content" boost type when the boosted item is NOT an "activity"', function () {
        boostComponent.object = boostUser;
        boostComponent.syncAllowedTypes();
        fixture.detectChanges();
        var availableBoostTypes = getBoostTypesList();
        expect(availableBoostTypes).not.toBeNull();
        expect(availableBoostTypes.nativeElement.children.length).toBe(1);
        expect(getBoostTypeItem(1).query(platform_browser_1.By.css('h4')).nativeElement.textContent).toContain('Sidebars');
    });
    it('should offer both "p2p" and "newsfeed" boost types when the boosted item is an "activity"', function () {
        boostComponent.object = { type: 'activity', guid: '123' };
        boostComponent.syncAllowedTypes();
        fixture.detectChanges();
        var availableBoostTypes = getBoostTypesList();
        expect(availableBoostTypes).not.toBeNull();
        expect(availableBoostTypes.nativeElement.children.length).toBe(2);
        expect(getBoostTypeItem(1).query(platform_browser_1.By.css('h4')).nativeElement.textContent).toContain('Newsfeeds');
        expect(getBoostTypeItem(2).query(platform_browser_1.By.css('h4')).nativeElement.textContent).toContain('Channels');
    });
    it('should have an amount of views section, with an input and label', function () {
        expect(fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--amount'))).not.toBeNull();
        expect(getAmountInput()).not.toBeNull();
        expect(getAmountLabel()).not.toBeNull();
    });
    it('amount of views section should have a title that says \'How many views do you want?\'', function () {
        var amountSectionTitle = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-amount > .m-boost--creator-section-title--small'));
        expect(amountSectionTitle).not.toBeNull();
        expect(amountSectionTitle.nativeElement.textContent).toContain('How many views do you want?');
    });
    it('should have a payment section', function () {
        var paymentSection = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-payment'));
        expect(paymentSection).not.toBeNull();
    });
    it('payment section should have a title that says \'Payment Method\'', function () {
        var paymentTitle = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-payment > .m-boost--creator-section-title--small'));
        expect(paymentTitle).not.toBeNull();
        expect(paymentTitle.nativeElement.textContent).toContain('Payment Method');
    });
    it('should have payment method list (usd, points)', function () {
        var methods = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-payment > ul.m-boost--creator-selector'));
        expect(methods).not.toBeNull();
        expect(methods.nativeElement.children.length).toBe(2);
        expect(fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator-selector > li:first-child > h4')).nativeElement.textContent).toContain('USD');
        expect(fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator-selector > li:nth-child(2) > h4')).nativeElement.textContent).toContain('points');
    });
    it('clicking on a payment option should highlight it', testing_1.fakeAsync(function () {
        testing_1.tick();
        var moneyOption = getPaymentMethodItem(1);
        expect(moneyOption.nativeElement.classList.contains('m-boost--creator-selector--highlight')).toBeFalsy();
        moneyOption.nativeElement.click();
        fixture.detectChanges();
        testing_1.tick();
        expect(moneyOption.nativeElement.classList.contains('m-boost--creator-selector--highlight')).toBeTruthy();
    }));
    describe('when selected payment method is usd', function () {
        it('then a card selector should appear', testing_1.fakeAsync(function () {
            testing_1.tick();
            boostComponent.setBoostCurrency('usd');
            fixture.detectChanges();
            expect(fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator-payment'))).not.toBeNull();
        }));
        describe('and the boost type is NOT "p2p"', function () {
            beforeEach(testing_1.fakeAsync(function () {
                boostComponent.object = boostActivity;
                boostComponent.syncAllowedTypes();
                fixture.detectChanges();
                var boostTypeOption = getBoostTypeItem(1);
                boostTypeOption.nativeElement.click();
                fixture.detectChanges();
                expect(boostComponent.boost.type).toEqual('newsfeed');
                boostComponent.setBoostCurrency('usd');
                fixture.detectChanges();
                testing_1.tick();
            }));
            it('the boost priority section should appear', function () {
                var prioritySection = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-priority'));
                expect(prioritySection).not.toBeNull();
            });
            it('boost priority section should have a title that says \'Priority\'', function () {
                var priorityTitle = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-priority > h3'));
                expect(priorityTitle).not.toBeNull();
                expect(priorityTitle.nativeElement.textContent).toContain('Priority');
            });
            it('boost priority option should be highlighted and usd payment amount should be updated when priority is selected', testing_1.fakeAsync(function () {
                var priorityToggle = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-priority > .m-boost--creator-toggle'));
                expect(priorityToggle).not.toBeNull();
                expect(priorityToggle.nativeElement.classList.contains('m-boost--creator-toggle--highlight')).toBeFalsy();
                priorityToggle.nativeElement.click();
                fixture.detectChanges();
                testing_1.tick();
                expect(priorityToggle.nativeElement.classList.contains('m-boost--creator-toggle--highlight')).toBeTruthy();
                var chargeAmount = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-payment > .m-boost--creator-selector > li:first-child > h4 > b'));
                var totalCharge = boostComponent.rates.minUsd + boostComponent.rates.priority;
                expect(chargeAmount).not.toBeNull();
                expect(chargeAmount.nativeElement.textContent).toEqual("$" + totalCharge.toFixed(2));
            }));
        });
    });
    it('changing amount input should change the boost\'s amount', function () {
        var amountInput = getAmountInput();
        amountInput.nativeElement.value = '10';
        amountInput.nativeElement.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        expect(boostComponent.boost.amount).toBe(10);
    });
    it('should have a submit section', function () {
        expect(submitSection).not.toBeNull();
    });
    it('if there are any errors, hovering over the submit section should show them', testing_1.fakeAsync(function () {
        spyOn(boostComponent, 'showErrors').and.callThrough();
        spyOn(boostComponent, 'validateBoost').and.callFake(function () {
            throw new creator_component_1.VisibleBoostError('I\'m an error');
        });
        submitSection.nativeElement.dispatchEvent(new Event('mouseenter'));
        fixture.detectChanges();
        testing_1.tick();
        expect(boostComponent.showErrors).toHaveBeenCalled();
        expect(getErrorLabel().nativeElement.textContent).toContain('I\'m an error');
    }));
    it('should have a submit button with the label \'Boost\'', function () {
        expect(boostSubmitButton).not.toBeNull();
        expect(boostSubmitButton.nativeElement.textContent).toContain('Boost');
    });
    it('boost button should be disabled either if the user hasn\'t entered data, there\'s an error, the component\'s loading something or just saved the boost', function () {
        spyOn(boostComponent, 'canSubmit').and.returnValue(true);
        fixture.detectChanges();
        expect(boostSubmitButton.nativeElement.disabled).toBeFalsy();
        boostComponent.criticalError = true;
        fixture.detectChanges();
        expect(boostSubmitButton.nativeElement.disabled).toBeTruthy();
        boostComponent.criticalError = false;
        boostComponent.inProgress = true;
        fixture.detectChanges();
        expect(boostSubmitButton.nativeElement.disabled).toBeTruthy();
        boostComponent.inProgress = false;
        boostComponent.success = true;
        fixture.detectChanges();
        expect(boostSubmitButton.nativeElement.disabled).toBeTruthy();
        boostComponent.success = false;
        boostComponent.canSubmit.and.returnValue(false);
        fixture.detectChanges();
        expect(boostSubmitButton.nativeElement.disabled).toBeTruthy();
    });
    it('boost button should call submit()', function () {
        spyOn(boostComponent, 'submit').and.callThrough();
        spyOn(boostComponent, 'canSubmit').and.returnValue(true);
        fixture.detectChanges();
        boostSubmitButton.nativeElement.click();
        fixture.detectChanges();
        expect(boostComponent.submit).toHaveBeenCalled();
    });
    describe('when the boost type is "p2p"', function () {
        var boostTargetSearchInput;
        beforeEach(function () {
            boostComponent.object = boostActivity;
            boostComponent.syncAllowedTypes();
            boostComponent.setBoostType('p2p');
            fixture.detectChanges();
            boostTargetSearchInput = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-target > .m-boost--creator--target > .m-boost--creator-wide-input--edit'));
        });
        it('should NOT offer boost target categories', function () {
            var categories = fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--categories'));
            expect(categories).toBeNull();
        });
        it('should have a target section with label \'Target\', description, and search box', function () {
            var boostTargetSection = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-target'));
            expect(boostTargetSection).not.toBeNull();
            var boostTargetTitle = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-target > h3'));
            expect(boostTargetTitle).not.toBeNull();
            expect(boostTargetTitle.nativeElement.textContent).toContain('Target');
            var boostTargetDescription = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-target > h3 > .m-boost--creator-section--title-context'));
            expect(boostTargetDescription).not.toBeNull();
            expect(boostTargetSearchInput).not.toBeNull();
        });
        it('should offer target suggestions when target input is changed', testing_1.fakeAsync(function () {
            spyOn(boostComponent, 'searchTarget').and.callThrough();
            spyOn(boostComponent, 'setTarget').and.callThrough();
            var searchQuery = 'mark';
            client_mock_spec_1.clientMock.get.calls.reset();
            client_mock_spec_1.clientMock.response["api/v1/search"] = {
                'status': 'success',
                'entities': [
                    boostTargetUser
                ],
                'load-next': 9
            };
            boostTargetSearchInput.nativeElement.value = searchQuery;
            fixture.detectChanges();
            boostTargetSearchInput.nativeElement.dispatchEvent(new Event('input'));
            boostTargetSearchInput.nativeElement.dispatchEvent(new Event('keyup'));
            jasmine.clock().tick(10);
            expect(boostComponent.searchTarget).toHaveBeenCalled();
            expect(client_mock_spec_1.clientMock.get).toHaveBeenCalled();
            testing_1.tick();
        }));
        describe('if the boost target is a boost pro plus subscriber', function () {
            beforeEach(function () {
                boostComponent.setTarget(boostTargetUser);
                fixture.detectChanges();
            });
            it('allows boost to be posted to target\'s facebook page if available, and allows it to be scheduled', function () {
                spyOn(boostComponent, 'togglePostToFacebook').and.callThrough();
                var fbToggle = fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator-toggle--target-facebook'));
                expect(fbToggle.nativeElement.classList.contains('m-boost--creator-toggle--highlight')).toBeFalsy();
                fbToggle.nativeElement.click();
                fixture.detectChanges();
                expect(boostComponent.togglePostToFacebook).toHaveBeenCalled();
                expect(fbToggle.nativeElement.classList.contains('m-boost--creator-toggle--highlight')).toBeTruthy();
                var fbScheduler = fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--target-facebook-scheduler'));
                expect(fbScheduler).not.toBeNull();
            });
        });
        it('successfully submits "p2p" boost', testing_1.fakeAsync(function () {
            spyOn(boostComponent, 'submit').and.callThrough();
            spyOn(boostComponent, 'canSubmit').and.returnValue(true);
            boostComponent.object = boostActivity;
            boostComponent.syncAllowedTypes();
            fixture.detectChanges();
            var boostTypeOption = getBoostTypeItem(2);
            boostTypeOption.nativeElement.click();
            fixture.detectChanges();
            expect(boostComponent.boost.type).toEqual('p2p');
            boostComponent.setTarget(boostTargetUser);
            fixture.detectChanges();
            var pointsOption = getPaymentMethodItem(2);
            pointsOption.nativeElement.click();
            fixture.detectChanges();
            var amountInput = getAmountInput();
            amountInput.nativeElement.value = '10';
            amountInput.nativeElement.dispatchEvent(new Event('input'));
            fixture.detectChanges();
            client_mock_spec_1.clientMock.post.calls.reset();
            client_mock_spec_1.clientMock.response["api/v1/boost/peer/" + boostActivity.guid + "/" + boostActivity.owner_guid] = { 'status': 'success' };
            boostSubmitButton.nativeElement.click();
            fixture.detectChanges();
            testing_1.tick();
            expect(boostComponent.submit).toHaveBeenCalled();
            expect(client_mock_spec_1.clientMock.post).toHaveBeenCalled();
            var args = client_mock_spec_1.clientMock.post.calls.mostRecent().args;
            expect(args[0]).toBe("api/v1/boost/peer/" + boostActivity.guid + "/" + boostActivity.owner_guid);
            expect(args[1]).toEqual({
                type: 'points',
                bid: 10,
                destination: boostTargetUser.guid,
                scheduledTs: null,
                postToFacebook: null,
                nonce: ''
            });
        }));
    });
    describe('boost target categories', function () {
        it('should be offered if the boost type is NOT "p2p"', function () {
            boostComponent.object = boostActivity;
            boostComponent.syncAllowedTypes();
            fixture.detectChanges();
            var categoriesSection = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-categories'));
            expect(categoriesSection).not.toBeNull();
            var categories = fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--categories'));
            expect(categories).not.toBeNull();
            expect(categories.nativeElement.children.length).toBeGreaterThan(0);
        });
        it('should not allow more than the maximum allowed categories to be chosen', function () {
            var selectedCategories = [], i;
            boostComponent.object = boostActivity;
            boostComponent.syncAllowedTypes();
            fixture.detectChanges();
            var categoriesSection = fixture.debugElement.query(platform_browser_1.By.css('section.m-boost--creator-section-categories'));
            expect(categoriesSection).not.toBeNull();
            var categories = fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--categories'));
            expect(categories).not.toBeNull();
            expect(categories.nativeElement.children.length).toBeGreaterThan(0);
            for (i = 0; i < boostComponent.rates.maxCategories + 1; i++) {
                var category = fixture.debugElement.query(platform_browser_1.By.css(".m-boost--creator--categories > .m-boost--creator-clickable:nth-child(" + (i + 1) + ")"));
                category.nativeElement.click();
                selectedCategories.push(category);
            }
            fixture.detectChanges();
            expect(boostComponent.boost.categories.length).toEqual(boostComponent.rates.maxCategories);
        });
    });
    it('successfully submits "newsfeed" boosts', testing_1.fakeAsync(function () {
        spyOn(boostComponent, 'submit').and.callThrough();
        spyOn(boostComponent, 'canSubmit').and.returnValue(true);
        boostComponent.object = boostActivity;
        boostComponent.syncAllowedTypes();
        fixture.detectChanges();
        var boostTypeOption = getBoostTypeItem(1);
        boostTypeOption.nativeElement.click();
        fixture.detectChanges();
        expect(boostComponent.boost.type).toEqual('newsfeed');
        var pointsOption = getPaymentMethodItem(2);
        pointsOption.nativeElement.click();
        fixture.detectChanges();
        var amountInput = getAmountInput();
        amountInput.nativeElement.value = '10';
        amountInput.nativeElement.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        var firstCategory = fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--categories > .m-boost--creator-clickable:first-child'));
        var secondCategory = fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--categories > .m-boost--creator-clickable:nth-child(2)'));
        var thirdCategory = fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--categories > .m-boost--creator-clickable:nth-child(3)'));
        firstCategory.nativeElement.click();
        secondCategory.nativeElement.click();
        thirdCategory.nativeElement.click();
        fixture.detectChanges();
        client_mock_spec_1.clientMock.post.calls.reset();
        client_mock_spec_1.clientMock.response["api/v1/boost/" + boostActivity.type + "/" + boostActivity.guid + "/" + boostActivity.owner_guid] = { 'status': 'success' };
        boostSubmitButton.nativeElement.click();
        fixture.detectChanges();
        testing_1.tick();
        expect(boostComponent.submit).toHaveBeenCalled();
        expect(client_mock_spec_1.clientMock.post).toHaveBeenCalled();
        var args = client_mock_spec_1.clientMock.post.calls.mostRecent().args;
        expect(args[0]).toBe("api/v1/boost/" + boostActivity.type + "/" + boostActivity.guid + "/" + boostActivity.owner_guid);
        expect(args[1]).toEqual({
            bidType: 'points',
            impressions: 10,
            categories: [
                firstCategory.nativeElement.textContent.trim().toLowerCase(),
                secondCategory.nativeElement.textContent.trim().toLowerCase(),
                thirdCategory.nativeElement.textContent.trim().toLowerCase()
            ],
            priority: null,
            paymentMethod: ''
        });
    }));
    it('successfully submits "content" boosts', testing_1.fakeAsync(function () {
        spyOn(boostComponent, 'submit').and.callThrough();
        spyOn(boostComponent, 'canSubmit').and.returnValue(true);
        boostComponent.object = boostBlog;
        boostComponent.syncAllowedTypes();
        fixture.detectChanges();
        var boostTypeOption = getBoostTypeItem(1);
        boostTypeOption.nativeElement.click();
        fixture.detectChanges();
        expect(boostComponent.boost.type).toEqual('content');
        var pointsOption = getPaymentMethodItem(2);
        pointsOption.nativeElement.click();
        fixture.detectChanges();
        var amountInput = getAmountInput();
        amountInput.nativeElement.value = '10';
        amountInput.nativeElement.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        var firstCategory = fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--categories > .m-boost--creator-clickable:first-child'));
        var secondCategory = fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--categories > .m-boost--creator-clickable:nth-child(2)'));
        var thirdCategory = fixture.debugElement.query(platform_browser_1.By.css('.m-boost--creator--categories > .m-boost--creator-clickable:nth-child(3)'));
        firstCategory.nativeElement.click();
        secondCategory.nativeElement.click();
        thirdCategory.nativeElement.click();
        fixture.detectChanges();
        client_mock_spec_1.clientMock.post.calls.reset();
        client_mock_spec_1.clientMock.response["api/v1/boost/" + boostBlog.type + "/" + boostBlog.guid + "/" + boostBlog.owner_guid] = { 'status': 'success' };
        boostSubmitButton.nativeElement.click();
        fixture.detectChanges();
        testing_1.tick();
        expect(boostComponent.submit).toHaveBeenCalled();
        expect(client_mock_spec_1.clientMock.post).toHaveBeenCalled();
        var args = client_mock_spec_1.clientMock.post.calls.mostRecent().args;
        expect(args[0]).toBe("api/v1/boost/" + boostBlog.type + "/" + boostBlog.guid + "/" + boostBlog.owner_guid);
        expect(args[1]).toEqual({
            bidType: 'points',
            impressions: 10,
            categories: [
                firstCategory.nativeElement.textContent.trim().toLowerCase(),
                secondCategory.nativeElement.textContent.trim().toLowerCase(),
                thirdCategory.nativeElement.textContent.trim().toLowerCase()
            ],
            priority: null,
            paymentMethod: ''
        });
    }));
});
//# sourceMappingURL=creator.component.spec.js.map