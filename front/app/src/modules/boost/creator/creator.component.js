"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var overlay_modal_1 = require("../../../services/ux/overlay-modal");
var api_1 = require("../../../services/api");
var session_1 = require("../../../services/session");
var VisibleBoostError = (function (_super) {
    __extends(VisibleBoostError, _super);
    function VisibleBoostError() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.visible = true;
        return _this;
    }
    return VisibleBoostError;
}(Error));
exports.VisibleBoostError = VisibleBoostError;
var BoostCreatorComponent = (function () {
    function BoostCreatorComponent(_changeDetectorRef, overlayModal, client, currency) {
        this._changeDetectorRef = _changeDetectorRef;
        this.overlayModal = overlayModal;
        this.client = client;
        this.currency = currency;
        this.object = {};
        this.boost = {
            amount: 1000,
            currency: 'points',
            type: null,
            categories: [],
            priority: false,
            target: null,
            postToFacebook: false,
            scheduledTs: null,
            nonce: ''
        };
        this.allowedTypes = {};
        this.categories = [];
        this.rates = {
            balance: null,
            rate: 1,
            min: 250,
            cap: 5000,
            usd: 1000,
            btc: 0,
            minUsd: 1,
            priority: 1,
            maxCategories: 3
        };
        this.editingAmount = false;
        this.editingTarget = false;
        this.targetQuery = '';
        this.targetResults = [];
        this.inProgress = false;
        this.success = false;
        this.criticalError = false;
        this.error = '';
        this.session = session_1.SessionFactory.build();
    }
    Object.defineProperty(BoostCreatorComponent.prototype, "data", {
        set: function (object) {
            this.object = object;
        },
        enumerable: true,
        configurable: true
    });
    BoostCreatorComponent.prototype.ngOnInit = function () {
        this.loadCategories();
        this.load();
    };
    BoostCreatorComponent.prototype.ngAfterViewInit = function () {
        this.syncAllowedTypes();
        this.amountEditorFocus();
    };
    BoostCreatorComponent.prototype.loadCategories = function () {
        this.categories = [];
        for (var id in window.Minds.categories) {
            this.categories.push({
                id: id,
                label: window.Minds.categories[id]
            });
        }
        this.categories.sort(function (a, b) { return a.label > b.label ? 1 : -1; });
    };
    BoostCreatorComponent.prototype.load = function () {
        var _this = this;
        this.inProgress = true;
        return this.client.get("api/v1/boost/rates")
            .then(function (rates) {
            _this.inProgress = false;
            _this.rates = rates;
            _this.rates = __assign({ btc: 0, maxCategories: 3 }, _this.rates);
        })
            .catch(function (e) {
            _this.inProgress = false;
            _this.criticalError = true;
            _this.error = 'Internal server error';
        });
    };
    BoostCreatorComponent.prototype.syncAllowedTypes = function () {
        if (!this.object || !this.object.type) {
            this.allowedTypes = {};
            this.boost.type = null;
            return;
        }
        switch (this.object.type) {
            case 'activity':
                this.allowedTypes = {
                    newsfeed: true,
                    p2p: true
                };
                this.boost.type = 'newsfeed';
                break;
            default:
                this.allowedTypes = {
                    content: true
                };
                this.boost.type = 'content';
                break;
        }
    };
    BoostCreatorComponent.prototype.setBoostType = function (type) {
        this.boost.type = type;
        this.roundAmount();
        this.showErrors();
    };
    BoostCreatorComponent.prototype.setBoostCurrency = function (currency) {
        this.boost.currency = currency;
        this.roundAmount();
        this.showErrors();
    };
    BoostCreatorComponent.prototype.setBoostNonce = function (nonce) {
        this.boost.nonce = nonce;
        this.showErrors();
    };
    BoostCreatorComponent.prototype.amountEditorFocus = function () {
        this.editingAmount = true;
        if (!this.boost.amount) {
            this.boost.amount = '';
        }
        this._changeDetectorRef.detectChanges();
    };
    BoostCreatorComponent.prototype.setBoostAmount = function (amount) {
        if (!amount) {
            this.boost.amount = 0;
            return;
        }
        amount = amount.replace(/,/g, '');
        this.boost.amount = parseInt(amount);
    };
    BoostCreatorComponent.prototype.amountEditorBlur = function () {
        this.editingAmount = false;
        if (!this.boost.amount) {
            this.boost.amount = 0;
        }
        if (this.boost.amount < 0) {
            this.boost.amount = 0;
        }
        this.roundAmount();
        this.showErrors();
    };
    BoostCreatorComponent.prototype.roundAmount = function () {
        if ((this.boost.type === 'p2p') && (!this.boost.currency || (this.boost.currency !== 'points'))) {
            this.boost.amount = Math.round(parseFloat("" + this.boost.amount) * 100) / 100;
        }
        else {
            this.boost.amount = Math.floor(this.boost.amount);
        }
    };
    BoostCreatorComponent.prototype.calcBaseCharges = function (type) {
        if (this.boost.type === 'p2p') {
            switch (type) {
                case 'points':
                    return Math.floor(this.boost.amount);
            }
            return this.boost.amount;
        }
        switch (type) {
            case 'usd':
                var usdFixRate = this.rates.usd / 100;
                return Math.ceil(this.boost.amount / usdFixRate) / 100;
            case 'points':
                return Math.floor(this.boost.amount / this.rates.rate);
            case 'btc':
                return 0;
        }
        throw new Error('Unknown currency');
    };
    BoostCreatorComponent.prototype.calcCharges = function (type) {
        var charges = this.calcBaseCharges(type);
        return charges + (charges * this.getPriorityRate());
    };
    BoostCreatorComponent.prototype.calcPriorityChargesPreview = function (type) {
        return this.calcBaseCharges(type) * this.getPriorityRate(true);
    };
    BoostCreatorComponent.prototype.getPriorityRate = function (force) {
        if (force || (this.boost.type !== 'p2p' && this.boost.priority)) {
            return this.rates.priority;
        }
        return 0;
    };
    BoostCreatorComponent.prototype.toggleCategory = function (id) {
        if (this.hasCategory(id)) {
            this.boost.categories.splice(this.boost.categories.indexOf(id), 1);
        }
        else {
            if (this.boost.categories.length >= this.rates.maxCategories) {
                return;
            }
            this.boost.categories.push(id);
        }
    };
    BoostCreatorComponent.prototype.hasCategory = function (id) {
        return this.boost.categories.indexOf(id) > -1;
    };
    BoostCreatorComponent.prototype.togglePriority = function () {
        this.boost.priority = !this.boost.priority;
        this.showErrors();
    };
    BoostCreatorComponent.prototype.targetEditorFocus = function () {
        var _this = this;
        this.editingTarget = true;
        this._changeDetectorRef.detectChanges();
        if (this._targetEditor.nativeElement) {
            setTimeout(function () { return _this._targetEditor.nativeElement.focus(); }, 100);
        }
    };
    BoostCreatorComponent.prototype.targetEditorBlur = function () {
        this.editingTarget = false;
        this.showErrors();
    };
    BoostCreatorComponent.prototype.searchTarget = function () {
        var _this = this;
        if (this._searchThrottle) {
            clearTimeout(this._searchThrottle);
            this._searchThrottle = void 0;
        }
        if (this.targetQuery.charAt(0) !== '@') {
            this.targetQuery = '@' + this.targetQuery;
        }
        var query = this.targetQuery;
        if (query.charAt(0) === '@') {
            query = query.substr(1);
        }
        if (!query || query.length <= 2) {
            this.targetResults = [];
            return;
        }
        this._searchThrottle = setTimeout(function () {
            _this.client.get("api/v2/search/suggest/user", {
                q: query,
                limit: 8,
                hydrate: 1
            })
                .then(function (_a) {
                var entities = _a.entities;
                if (!entities) {
                    return;
                }
                _this.targetResults = entities;
            })
                .catch(function (e) { return console.error('Cannot load results', e); });
        });
    };
    BoostCreatorComponent.prototype.setTarget = function (target, $event) {
        if ($event) {
            $event.preventDefault();
        }
        this.boost.target = __assign({}, target);
        this.targetResults = [];
        this.targetQuery = '@' + target.username;
        this.showErrors();
    };
    BoostCreatorComponent.prototype.togglePostToFacebook = function () {
        this.boost.postToFacebook = !this.boost.postToFacebook;
    };
    BoostCreatorComponent.prototype.validateBoost = function () {
        if (this.boost.amount <= 0) {
            throw new Error('Amount should be greater than zero.');
        }
        if (!this.boost.currency) {
            throw new Error('You should select a currency.');
        }
        if (!this.boost.type) {
            throw new Error('You should select a type.');
        }
        if (this.boost.currency === 'points') {
            var charges = this.calcCharges(this.boost.currency);
            if ((this.rates.balance !== null) && (charges > this.rates.balance)) {
                throw new VisibleBoostError("You only have " + this.rates.balance + " points.");
            }
        }
        else {
            if (!this.boost.nonce) {
                throw new Error('Payment method not processed.');
            }
        }
        if (this.boost.type === 'p2p') {
            if (!this.boost.target) {
                throw new Error('You should select a target.');
            }
            if (this.boost.target && (this.boost.target.guid === this.session.getLoggedInUser().guid)) {
                throw new VisibleBoostError('You cannot boost to yourself.');
            }
            if (this.boost.target && !this.boost.target.merchant && (this.boost.currency !== 'points')) {
                throw new VisibleBoostError('User cannot receive USD.');
            }
        }
        else {
            if (this.boost.amount < this.rates.min || this.boost.amount > this.rates.cap) {
                throw new VisibleBoostError("You must boost between " + this.rates.min + " and " + this.rates.cap + " views.");
            }
        }
        if (this.boost.currency === 'usd') {
            if (this.calcCharges(this.boost.currency) < this.rates.minUsd) {
                throw new VisibleBoostError("You must spend at least " + this.currency.transform(this.rates.minUsd, 'USD', true) + " USD");
            }
        }
    };
    BoostCreatorComponent.prototype.canSubmit = function () {
        try {
            this.validateBoost();
            return true;
        }
        catch (e) {
        }
        return false;
    };
    BoostCreatorComponent.prototype.showErrors = function () {
        this.error = '';
        try {
            this.validateBoost();
        }
        catch (e) {
            if (e.visible) {
                this.error = e.message;
            }
        }
    };
    BoostCreatorComponent.prototype.submit = function () {
        var _this = this;
        if (this.inProgress) {
            return;
        }
        if (!this.canSubmit()) {
            this.showErrors();
            return;
        }
        this.inProgress = true;
        var request;
        if (this.boost.type !== 'p2p') {
            request = this.client.post("api/v1/boost/" + this.object.type + "/" + this.object.guid + "/" + this.object.owner_guid, {
                bidType: this.boost.currency,
                impressions: this.boost.amount,
                categories: this.boost.categories,
                priority: this.boost.priority ? 1 : null,
                paymentMethod: this.boost.nonce
            })
                .then(function (response) {
                return { done: true };
            });
        }
        else {
            request = this.client.post("api/v1/boost/peer/" + this.object.guid + "/" + this.object.owner_guid, {
                type: this.boost.currency === 'points' ? 'points' : 'pro',
                bid: this.boost.amount,
                destination: this.boost.target.guid,
                scheduledTs: this.boost.scheduledTs,
                postToFacebook: this.boost.postToFacebook ? 1 : null,
                nonce: this.boost.nonce
            })
                .then(function (response) {
                return { done: true };
            })
                .catch(function (e) {
                if (e && e.stage === 'transaction') {
                    throw new Error('Sorry, your payment failed. Please, try again or use another card');
                }
            });
        }
        request
            .then(function (_a) {
            var done = _a.done;
            _this.inProgress = false;
            if (done) {
                _this.success = true;
                setTimeout(function () {
                    _this.overlayModal.dismiss();
                }, 2500);
            }
        })
            .catch(function (e) {
            _this.inProgress = false;
            _this.error = (e && e.message) || 'Sorry, something went wrong';
        });
    };
    return BoostCreatorComponent;
}());
__decorate([
    core_1.Input('object'),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], BoostCreatorComponent.prototype, "data", null);
__decorate([
    core_1.ViewChild('amountEditor'),
    __metadata("design:type", core_1.ElementRef)
], BoostCreatorComponent.prototype, "_amountEditor", void 0);
__decorate([
    core_1.ViewChild('targetEditor'),
    __metadata("design:type", core_1.ElementRef)
], BoostCreatorComponent.prototype, "_targetEditor", void 0);
BoostCreatorComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        providers: [common_1.CurrencyPipe],
        selector: 'm-boost--creator',
        templateUrl: 'creator.component.html'
    }),
    __metadata("design:paramtypes", [core_1.ChangeDetectorRef,
        overlay_modal_1.OverlayModalService,
        api_1.Client,
        common_1.CurrencyPipe])
], BoostCreatorComponent);
exports.BoostCreatorComponent = BoostCreatorComponent;
//# sourceMappingURL=creator.component.js.map