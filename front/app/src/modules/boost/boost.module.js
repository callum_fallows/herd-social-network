"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var common_module_1 = require("../../common/common.module");
var forms_1 = require("@angular/forms");
var checkout_module_1 = require("../checkout/checkout.module");
var third_party_networks_module_1 = require("../third-party-networks/third-party-networks.module");
var ads_module_1 = require("../ads/ads.module");
var creator_component_1 = require("./creator/creator.component");
var console_component_1 = require("./console/console.component");
var network_component_1 = require("./console/list/network.component");
var p2p_component_1 = require("./console/list/p2p.component");
var card_component_1 = require("./console/card/card.component");
var booster_component_1 = require("./console/booster/booster.component");
var marketing_component_1 = require("./marketing.component");
var boostRoutes = [
    { path: 'boost', component: marketing_component_1.BoostMarketingComponent }
];
var BoostModule = (function () {
    function BoostModule() {
    }
    return BoostModule;
}());
BoostModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            forms_1.ReactiveFormsModule,
            router_1.RouterModule.forChild(boostRoutes),
            common_module_1.CommonModule,
            checkout_module_1.CheckoutModule,
            third_party_networks_module_1.ThirdPartyNetworksModule,
            ads_module_1.AdsModule,
        ],
        declarations: [
            creator_component_1.BoostCreatorComponent,
            console_component_1.BoostConsoleComponent,
            network_component_1.BoostConsoleNetworkListComponent,
            p2p_component_1.BoostConsoleP2PListComponent,
            card_component_1.BoostConsoleCard,
            booster_component_1.BoostConsoleBooster,
            marketing_component_1.BoostMarketingComponent
        ],
        exports: [
            network_component_1.BoostConsoleNetworkListComponent,
            p2p_component_1.BoostConsoleP2PListComponent,
            card_component_1.BoostConsoleCard,
            booster_component_1.BoostConsoleBooster
        ],
        entryComponents: [
            creator_component_1.BoostCreatorComponent,
            console_component_1.BoostConsoleComponent,
            marketing_component_1.BoostMarketingComponent
        ]
    })
], BoostModule);
exports.BoostModule = BoostModule;
//# sourceMappingURL=boost.module.js.map