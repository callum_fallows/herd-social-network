"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../../../services/api");
var session_1 = require("../../../../services/session");
var service_1 = require("../../../../modules/modals/signup/service");
var attachment_1 = require("../../../../services/attachment");
var sockets_1 = require("../../../../services/sockets");
var textarea_component_1 = require("../../../../common/components/editors/textarea.component");
var Comments = (function () {
    function Comments(client, attachment, modal, sockets, renderer, cd) {
        this.client = client;
        this.attachment = attachment;
        this.modal = modal;
        this.sockets = sockets;
        this.renderer = renderer;
        this.cd = cd;
        this.guid = '';
        this.comments = [];
        this.content = '';
        this.reversed = false;
        this.session = session_1.SessionFactory.build();
        this.focusOnInit = false;
        this.editing = false;
        this.showModal = false;
        this.limit = 5;
        this.offset = '';
        this.inProgress = false;
        this.canPost = true;
        this.triedToPost = false;
        this.moreData = false;
        this.loaded = false;
        this.socketSubscriptions = {
            comment: null
        };
        this.conversation = false;
        this.readonly = false;
        this.commentsScrollEmitter = new core_1.EventEmitter();
        this.autoloadBlocked = false;
        this.overscrollAmount = 0;
        this.minds = window.Minds;
    }
    Object.defineProperty(Comments.prototype, "_object", {
        set: function (value) {
            this.object = value;
            this.guid = this.object.guid;
            if (this.object.entity_guid)
                this.guid = this.object.entity_guid;
            this.parent = this.object;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Comments.prototype, "_reversed", {
        set: function (value) {
            if (value)
                this.reversed = true;
            else
                this.reversed = false;
        },
        enumerable: true,
        configurable: true
    });
    Comments.prototype.ngOnInit = function () {
        this.load(true);
        this.listen();
    };
    Comments.prototype.load = function (refresh) {
        var _this = this;
        if (refresh === void 0) { refresh = false; }
        if (refresh) {
            this.offset = '';
            this.moreData = true;
            this.comments = [];
            if (this.socketRoomName) {
                this.sockets.leave(this.socketRoomName);
            }
            this.socketRoomName = void 0;
        }
        if (this.inProgress) {
            return;
        }
        this.error = '';
        this.inProgress = true;
        this.client.get('api/v1/comments/' + this.guid, { limit: this.limit, offset: this.offset, reversed: true })
            .then(function (response) {
            if (!_this.socketRoomName && response.socketRoomName) {
                _this.socketRoomName = response.socketRoomName;
                _this.joinSocketRoom();
            }
            _this.loaded = true;
            _this.inProgress = false;
            _this.moreData = true;
            if (!response.comments) {
                _this.moreData = false;
                return false;
            }
            _this.comments = response.comments.concat(_this.comments);
            if (refresh) {
                _this.commentsScrollEmitter.emit('bottom');
            }
            if (_this.offset && _this.scrollView) {
                var el = _this.scrollView.nativeElement;
                var scrollTop = el.scrollTop;
                var scrollHeight = el.scrollHeight;
                _this.cd.detectChanges();
                el.scrollTop = scrollTop + el.scrollHeight - scrollHeight;
            }
            _this.offset = response['load-previous'];
            if (!_this.offset ||
                _this.offset === null ||
                response.comments.length < (_this.limit - 1)) {
                _this.moreData = false;
            }
        })
            .catch(function (e) {
            _this.inProgress = false;
            _this.error = (e && e.message) || 'There was an error';
        });
    };
    Comments.prototype.autoloadPrevious = function () {
        var _this = this;
        if (!this.moreData || this.autoloadBlocked) {
            return;
        }
        this.cancelOverscroll();
        this.autoloadBlocked = true;
        setTimeout(function () {
            _this.autoloadBlocked = false;
        }, 1000);
        this.load();
    };
    Comments.prototype.overscrollHandler = function (_a) {
        var _this = this;
        var deltaY = _a.deltaY;
        this.cancelOverscroll();
        if (this.autoloadBlocked) {
            this.overscrollAmount = 0;
            return;
        }
        this.overscrollAmount += deltaY;
        this.overscrollTimer = setTimeout(function () {
            if (_this.overscrollAmount < -75) {
                _this.autoloadPrevious();
            }
            _this.overscrollAmount = 0;
        }, 250);
    };
    Comments.prototype.cancelOverscroll = function () {
        if (this.overscrollTimer) {
            clearTimeout(this.overscrollTimer);
        }
    };
    Comments.prototype.joinSocketRoom = function () {
        if (this.socketRoomName) {
            this.sockets.join(this.socketRoomName);
        }
    };
    Comments.prototype.ngAfterViewInit = function () {
        if (this.focusOnInit) {
            this.textareaControl.focus();
        }
    };
    Comments.prototype.ngOnDestroy = function () {
        this.cancelOverscroll();
        if (this.socketRoomName && !this.conversation) {
            this.sockets.leave(this.socketRoomName);
        }
        for (var sub in this.socketSubscriptions) {
            if (this.socketSubscriptions[sub]) {
                this.socketSubscriptions[sub].unsubscribe();
            }
        }
    };
    Comments.prototype.listen = function () {
        var _this = this;
        this.socketSubscriptions.comment = this.sockets.subscribe('comment', function (parent_guid, owner_guid, guid) {
            if (parent_guid !== _this.guid) {
                return;
            }
            if (_this.session.isLoggedIn() && owner_guid === _this.session.getLoggedInUser().guid) {
                return;
            }
            _this.client.get('api/v1/comments/' + _this.guid, { limit: 1, offset: guid, reversed: false })
                .then(function (response) {
                if (!response.comments || response.comments.length === 0) {
                    return;
                }
                _this.comments.push(response.comments[0]);
                _this.commentsScrollEmitter.emit('bottom');
            });
        });
    };
    Comments.prototype.postEnabled = function () {
        return !this.inProgress && this.canPost && (this.content || this.attachment.has());
    };
    Comments.prototype.post = function (e) {
        return __awaiter(this, void 0, void 0, function () {
            var data, newLength, currentIndex, response, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        e.preventDefault();
                        if (!this.content && !this.attachment.has()) {
                            return [2 /*return*/];
                        }
                        if (this.inProgress || !this.canPost) {
                            this.triedToPost = true;
                            return [2 /*return*/];
                        }
                        data = this.attachment.exportMeta();
                        data['comment'] = this.content;
                        newLength = this.comments.push({
                            description: this.content,
                            guid: 0,
                            ownerObj: this.session.getLoggedInUser(),
                            owner_guid: this.session.getLoggedInUser().guid,
                            time_created: Date.now() / 1000,
                            type: 'comment'
                        }), currentIndex = newLength - 1;
                        this.attachment.reset();
                        this.content = '';
                        this.commentsScrollEmitter.emit('bottom');
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.client.post('api/v1/comments/' + this.guid, data)];
                    case 2:
                        response = _a.sent();
                        this.comments[currentIndex] = response.comment;
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        this.comments[currentIndex].error = (e_1 && e_1.message) || 'There was an error';
                        console.error('Error posting', e_1);
                        return [3 /*break*/, 4];
                    case 4:
                        this.commentsScrollEmitter.emit('bottom');
                        return [2 /*return*/];
                }
            });
        });
    };
    Comments.prototype.isLoggedIn = function () {
        if (!this.session.isLoggedIn()) {
            this.modal.setSubtitle('You need to have channel in order to comment').open();
        }
    };
    Comments.prototype.delete = function (index) {
        this.comments.splice(index, 1);
    };
    Comments.prototype.edited = function (index, $event) {
        this.comments[index] = $event.comment;
    };
    Comments.prototype.uploadAttachment = function (file, e) {
        var _this = this;
        this.canPost = false;
        this.triedToPost = false;
        this.attachment.setHidden(true);
        this.attachment.setContainer(this.object);
        this.attachment.upload(file)
            .then(function (guid) {
            _this.canPost = true;
            _this.triedToPost = false;
            file.value = null;
        })
            .catch(function (e) {
            console.error(e);
            _this.canPost = true;
            _this.triedToPost = false;
            file.value = null;
        });
    };
    Comments.prototype.removeAttachment = function (file) {
        var _this = this;
        this.canPost = false;
        this.triedToPost = false;
        this.attachment.remove(file).then(function () {
            _this.canPost = true;
            _this.triedToPost = false;
            file.value = '';
        }).catch(function (e) {
            console.error(e);
            _this.canPost = true;
            _this.triedToPost = false;
        });
    };
    Comments.prototype.getPostPreview = function (message) {
        if (!message) {
            return;
        }
        this.attachment.preview(message);
    };
    Comments.prototype.reply = function (comment) {
        var _this = this;
        if (!comment || !comment.ownerObj) {
            return;
        }
        var username = comment.ownerObj.username;
        this.content = "@" + username + " " + this.content;
        setTimeout(function () {
            _this.textareaControl.focus();
        }, 50);
    };
    return Comments;
}());
__decorate([
    core_1.ViewChild('message'),
    __metadata("design:type", textarea_component_1.Textarea)
], Comments.prototype, "textareaControl", void 0);
__decorate([
    core_1.ViewChild('scrollArea'),
    __metadata("design:type", core_1.ElementRef)
], Comments.prototype, "scrollView", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], Comments.prototype, "conversation", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], Comments.prototype, "readonly", void 0);
Comments = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'herd-comments',
        inputs: ['_object : object', '_reversed : reversed', 'limit', 'focusOnInit'],
        templateUrl: 'list.html',
        providers: [
            {
                provide: attachment_1.AttachmentService,
                useFactory: attachment_1.AttachmentService._,
                deps: [api_1.Client, api_1.Upload]
            }
        ]
    }),
    __metadata("design:paramtypes", [api_1.Client,
        attachment_1.AttachmentService,
        service_1.SignupModalService,
        sockets_1.SocketsService,
        core_1.Renderer,
        core_1.ChangeDetectorRef])
], Comments);
exports.Comments = Comments;
//# sourceMappingURL=comments.js.map