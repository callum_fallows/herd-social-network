"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../../../../services/api");
var session_1 = require("../../../../../services/session");
var attachment_1 = require("../../../../../services/attachment");
var selector_1 = require("../../../../../modules/third-party-networks/selector");
var Poster = (function () {
    function Poster(client, upload, attachment) {
        this.client = client;
        this.upload = upload;
        this.attachment = attachment;
        this.content = '';
        this.meta = {
            wire_threshold: null
        };
        this.session = session_1.SessionFactory.build();
        this.load = new core_1.EventEmitter();
        this.inProgress = false;
        this.canPost = true;
        this.validThreshold = true;
        this.errorMessage = null;
        this.minds = window.Minds;
    }
    Object.defineProperty(Poster.prototype, "_container_guid", {
        set: function (guid) {
            this.attachment.setContainer(guid);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Poster.prototype, "accessId", {
        set: function (access_id) {
            this.attachment.setAccessId(access_id);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Poster.prototype, "message", {
        set: function (value) {
            if (value) {
                value = decodeURIComponent((value).replace(/\+/g, '%20'));
                this.meta.message = value;
                this.getPostPreview({ value: value });
            }
        },
        enumerable: true,
        configurable: true
    });
    Poster.prototype.post = function () {
        var _this = this;
        if (!this.meta.message && !this.attachment.has()) {
            return;
        }
        var data = Object.assign(this.meta, this.attachment.exportMeta());
        data = this.thirdPartyNetworksSelector.inject(data);
        this.inProgress = true;
        this.client.post('api/v1/newsfeed', data)
            .then(function (data) {
            data.activity.boostToggle = true;
            _this.load.next(data.activity);
            _this.attachment.reset();
            _this.meta = { wire_threshold: null };
            _this.inProgress = false;
        })
            .catch(function (e) {
            this.inProgress = false;
        });
    };
    Poster.prototype.uploadAttachment = function (file, event) {
        var _this = this;
        if (file.value) {
            this.canPost = false;
            this.inProgress = true;
            this.errorMessage = null;
            this.attachment.upload(file)
                .then(function (guid) {
                _this.inProgress = false;
                _this.canPost = true;
                file.value = null;
            })
                .catch(function (e) {
                if (e && e.message) {
                    _this.errorMessage = e.message;
                }
                _this.inProgress = false;
                _this.canPost = true;
                file.value = null;
                _this.attachment.reset();
            });
        }
    };
    Poster.prototype.removeAttachment = function (file) {
        var _this = this;
        this.canPost = false;
        this.inProgress = true;
        this.errorMessage = '';
        this.attachment.remove(file).then(function () {
            _this.inProgress = false;
            _this.canPost = true;
            file.value = '';
        }).catch(function (e) {
            console.error(e);
            _this.inProgress = false;
            _this.canPost = true;
        });
    };
    Poster.prototype.getPostPreview = function (message) {
        if (!message.value) {
            return;
        }
        this.attachment.preview(message.value);
    };
    return Poster;
}());
__decorate([
    core_1.ViewChild('thirdPartyNetworksSelector'),
    __metadata("design:type", selector_1.ThirdPartyNetworksSelector)
], Poster.prototype, "thirdPartyNetworksSelector", void 0);
Poster = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-newsfeed-poster',
        inputs: ['_container_guid: containerGuid', 'accessId', 'message'],
        outputs: ['load'],
        providers: [
            {
                provide: attachment_1.AttachmentService,
                useFactory: attachment_1.AttachmentService._,
                deps: [api_1.Client, api_1.Upload]
            }
        ],
        templateUrl: 'poster.html'
    }),
    __metadata("design:paramtypes", [api_1.Client, api_1.Upload, attachment_1.AttachmentService])
], Poster);
exports.Poster = Poster;
//# sourceMappingURL=poster.js.map