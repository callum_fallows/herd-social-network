"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var session_1 = require("../../../../../services/session");
var TagsInput = (function () {
    function TagsInput(element) {
        this.element = element;
        this.session = session_1.SessionFactory.build();
        this.error = '';
        this.inProgress = false;
        this.input = '';
        this.placeholder = '+';
        this.tags = [];
        this.change = new core_1.EventEmitter();
    }
    Object.defineProperty(TagsInput.prototype, "_tags", {
        set: function (tags) {
            if (Array.isArray(tags))
                this.tags = tags;
        },
        enumerable: true,
        configurable: true
    });
    TagsInput.prototype.keyUp = function (e) {
        switch (e.keyCode) {
            case 32:
            case 9:
            case 13:
            case 188:
                this.push();
                break;
            case 8:
                if (!this.input) {
                    this.pop();
                }
                break;
        }
        this.change.next(this.tags);
    };
    TagsInput.prototype.blur = function (e) {
        this.push();
    };
    TagsInput.prototype.removeTag = function (index) {
        this.tags.splice(index, 1);
        this.change.next(this.tags);
    };
    TagsInput.prototype.focus = function () {
        this.element.nativeElement.getElementsByTagName('input')[0].focus();
    };
    TagsInput.prototype.push = function () {
        var input = this.input;
        input = input
            .replace(/^[,\s]+/, '')
            .replace(/[,\s]+$/, '');
        if (!input) {
            return;
        }
        this.tags.push(input);
        this.input = '';
    };
    TagsInput.prototype.pop = function () {
        this.tags.pop();
    };
    return TagsInput;
}());
TagsInput = __decorate([
    core_1.Component({
        selector: 'minds-form-tags-input',
        host: {
            '(click)': 'focus()'
        },
        inputs: ['_tags: tags'],
        outputs: ['change: tagsChange'],
        template: "\n    <div class=\"m-form-tags-input-tags-tag mdl-shadow--2dp mdl-color--indigo-600 mdl-color-text--indigo-50\"\n      *ngFor=\"let tag of tags; let i = index\"\n      (click)=\"removeTag(i)\">\n      <span>{{tag}}</span>\n      <i class=\"material-icons mdl-color-text--white\">close</i>\n    </div>\n    <input\n      type=\"text\"\n      name=\"input-tags\"\n      [(ngModel)]=\"input\"\n      (keyup)=\"keyUp($event)\"\n      (blur)=\"blur($event)\"\n      [size]=\"input.length ? input.length : 1\">\n  "
    }),
    __metadata("design:paramtypes", [core_1.ElementRef])
], TagsInput);
exports.TagsInput = TagsInput;
//# sourceMappingURL=tags.js.map