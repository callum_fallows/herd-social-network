"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var MindsFatBanner = (function () {
    function MindsFatBanner() {
        this.minds = window.Minds;
        this.editing = false;
        this.src = '';
        this.index = 0;
        this.top = 0;
        this.added = new core_1.EventEmitter();
    }
    Object.defineProperty(MindsFatBanner.prototype, "_object", {
        set: function (value) {
            if (!value)
                return;
            this.object = value;
            this.src = '/fs/v1/banners/' + this.object.guid + '/' + this.top + '/' + this.object.last_updated;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MindsFatBanner.prototype, "_src", {
        set: function (value) {
            this.src = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MindsFatBanner.prototype, "_top", {
        set: function (value) {
            if (!value)
                return;
            this.top = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MindsFatBanner.prototype, "_editMode", {
        set: function (value) {
            this.editing = value;
        },
        enumerable: true,
        configurable: true
    });
    MindsFatBanner.prototype.add = function (e) {
        var _this = this;
        if (!this.editing)
            return;
        var element = e.target ? e.target : e.srcElement;
        this.file = element ? element.files[0] : null;
        var reader = new FileReader();
        reader.onloadend = function () {
            _this.src = reader.result;
        };
        reader.readAsDataURL(this.file);
        element.value = '';
    };
    MindsFatBanner.prototype.cancel = function () {
        this.file = null;
    };
    Object.defineProperty(MindsFatBanner.prototype, "_done", {
        set: function (value) {
            if (value)
                this.done();
        },
        enumerable: true,
        configurable: true
    });
    MindsFatBanner.prototype.done = function () {
        this.added.next({
            index: this.index,
            file: this.file,
            top: this.top
        });
        this.file = null;
    };
    MindsFatBanner.prototype.onClick = function (e) {
        e.target.parentNode.parentNode.getElementsByTagName('input')[0].click();
    };
    return MindsFatBanner;
}());
MindsFatBanner = __decorate([
    core_1.Component({
        selector: 'minds-banner-fat',
        inputs: ['_object: object', '_src: src', '_editMode: editMode', '_done: done'],
        outputs: ['added'],
        template: "\n  <div class=\"minds-banner\" *ngIf=\"!editing\">\n    <img [src]=\"src\" class=\"minds-banner-img\" />\n  </div>\n  <div *ngIf=\"editing\" class=\"minds-banner minds-banner-editing\">\n    <img [src]=\"src\" class=\"minds-banner-img\"/>\n    <div class=\"overlay\" [hidden]=\"file\">\n      <i class=\"material-icons\">camera</i>\n      <span i18n=\"@@MINDS__BANNER__ADD_NEW_BANNER\">Click here to add a new banner</span>\n    </div>\n\n    <button class=\"add-button mdl-button mdl-button--raised mdl-button--colored material-icons\" (click)=\"onClick($event)\">\n      <i class=\"material-icons\">file_upload</i>\n    </button>\n    <input type=\"file\" id=\"file\" (change)=\"add($event)\" [hidden]=\"file\" />\n  </div>\n  "
    })
], MindsFatBanner);
exports.MindsFatBanner = MindsFatBanner;
//# sourceMappingURL=fat.js.map