"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var MindsBanner = (function () {
    function MindsBanner() {
        this.minds = window.Minds;
        this.editing = false;
        this.src = '';
        this.index = 0;
        this.startY = 0;
        this.offsetY = 0;
        this.top = 0;
        this.dragging = false;
        this.added = new core_1.EventEmitter();
    }
    Object.defineProperty(MindsBanner.prototype, "_object", {
        set: function (value) {
            if (!value)
                return;
            this.object = value;
            this.src = '/fs/v1/banners/' + this.object.guid + '/' + this.top + '/' + this.object.banner;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MindsBanner.prototype, "_src", {
        set: function (value) {
            this.src = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MindsBanner.prototype, "_top", {
        set: function (value) {
            if (!value)
                return;
            this.top = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MindsBanner.prototype, "_editMode", {
        set: function (value) {
            this.editing = value;
        },
        enumerable: true,
        configurable: true
    });
    MindsBanner.prototype.add = function (e) {
        var _this = this;
        if (!this.editing)
            return;
        var element = e.target ? e.target : e.srcElement;
        this.file = element ? element.files[0] : null;
        var reader = new FileReader();
        reader.onloadend = function () {
            _this.src = reader.result;
        };
        reader.readAsDataURL(this.file);
        element.value = '';
    };
    MindsBanner.prototype.cancel = function () {
        this.file = null;
    };
    Object.defineProperty(MindsBanner.prototype, "_done", {
        set: function (value) {
            if (value)
                this.done();
        },
        enumerable: true,
        configurable: true
    });
    MindsBanner.prototype.done = function () {
        this.added.next({
            index: this.index,
            file: this.file,
            top: this.top
        });
        this.file = null;
    };
    MindsBanner.prototype.onClick = function (e) {
        e.target.parentNode.parentNode.getElementsByTagName('input')[0].click();
    };
    return MindsBanner;
}());
MindsBanner = __decorate([
    core_1.Component({
        selector: 'minds-banner',
        inputs: ['_object: object', '_src: src', '_top: top', 'overlay', '_editMode: editMode', '_done: done'],
        outputs: ['added'],
        template: "\n  <div class=\"minds-banner\" *ngIf=\"!editing\">\n    <div class=\"minds-banner-img m-banner--img-cover mdl-color--indigo\"\n      [style.backgroundImage]=\"src ? 'url(' + src + ')' : null\"\n    ></div>\n\n    <div class=\"minds-banner-overlay\"></div>\n  </div>\n  <div *ngIf=\"editing\" class=\"minds-banner minds-banner-editing m-banner--img-cover\"\n    [style.backgroundImage]=\"src ? 'url(' + src + ')' : null\"\n  >\n    <div class=\"overlay\" [hidden]=\"file\">\n      <i class=\"material-icons\">camera</i>\n      <span>\n        <!-- i18n: @@MINDS__BANNER__ADD_NEW_BANNER_W_RECOMMENDATION -->Click here to add a new banner<br>\n        <em>Recommended minimum size 2000px &times; 1125px (Ratio 16:9)</em><!-- /i18n -->\n      </span>\n    </div>\n    <div class=\"minds-banner-overlay\"></div>\n\n    <button class=\"add-button mdl-button mdl-button--raised mdl-button--colored material-icons\" (click)=\"onClick($event)\">\n      <i class=\"material-icons\">file_upload</i>\n    </button>\n\n    <div class=\"save-bar\" [hidden]=\"!file\">\n      <div class=\"mdl-layout-spacer\"></div>\n      <span class=\"minds-button-edit cancel-button\" (click)=\"cancel()\">\n        <button i18n=\"@@M__ACTION__CANCEL\">Cancel</button>\n      </span>\n      <span class=\"minds-button-edit save-button\" (click)=\"done()\">\n        <button i18n=\"@@M__ACTION__SAVE\">Save</button>\n      </span>\n    </div>\n    <input type=\"file\" id=\"file\" (change)=\"add($event)\" [hidden]=\"file\" />\n  </div>\n  "
    })
], MindsBanner);
exports.MindsBanner = MindsBanner;
//# sourceMappingURL=banner.js.map