"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var session_1 = require("../../../../services/session");
var overlay_modal_1 = require("../../../../services/ux/overlay-modal");
var creator_component_1 = require("../../../boost/creator/creator.component");
var BoostButton = (function () {
    function BoostButton(overlayModal) {
        this.overlayModal = overlayModal;
        this.object = {
            'guid': null
        };
        this.session = session_1.SessionFactory.build();
        this.showModal = false;
    }
    BoostButton.prototype.boost = function () {
        var creator = this.overlayModal.create(creator_component_1.BoostCreatorComponent, this.object);
        creator.present();
    };
    return BoostButton;
}());
BoostButton = __decorate([
    core_1.Component({
        selector: 'minds-button-boost',
        inputs: ['object'],
        template: "\n    <button class=\"mdl-button mdl-color-text--white mdl-button--colored mdl-button--raised m-boost-button-fat\"\n      (click)=\"boost()\">\n    <!-- i18n: verb|@@M__ACTION__PROMOTE -->Boost<!-- /i18n -->\n    </button>\n  "
    }),
    __metadata("design:paramtypes", [overlay_modal_1.OverlayModalService])
], BoostButton);
exports.BoostButton = BoostButton;
//# sourceMappingURL=boost.js.map