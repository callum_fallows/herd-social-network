"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var session_1 = require("../../../../services/session");
var api_1 = require("../../../../services/api");
var service_1 = require("../../../../modules/modals/signup/service");
var SubscribeButton = (function () {
    function SubscribeButton(client, modal) {
        this.client = client;
        this.modal = modal;
        this._user = {
            subscribed: false
        };
        this._inprogress = false;
        this.showModal = false;
        this.session = session_1.SessionFactory.build();
    }
    Object.defineProperty(SubscribeButton.prototype, "user", {
        set: function (value) {
            this._user = value;
        },
        enumerable: true,
        configurable: true
    });
    SubscribeButton.prototype.subscribe = function () {
        var _this = this;
        var self = this;
        if (!this.session.isLoggedIn()) {
            this.modal.setSubtitle('You need to have a channel in order to subscribe').open();
            return false;
        }
        this._user.subscribed = true;
        this.client.post('api/v1/subscribe/' + this._user.guid, {})
            .then(function (response) {
            if (response && response.error) {
                throw 'error';
            }
            _this._user.subscribed = true;
        })
            .catch(function (e) {
            _this._user.subscribed = false;
            alert('You can\'t subscribe to this user.');
        });
    };
    SubscribeButton.prototype.unSubscribe = function () {
        var _this = this;
        var self = this;
        this._user.subscribed = false;
        this.client.delete('api/v1/subscribe/' + this._user.guid, {})
            .then(function (response) {
            _this._user.subscribed = false;
        })
            .catch(function (e) {
            _this._user.subscribed = true;
        });
    };
    return SubscribeButton;
}());
SubscribeButton = __decorate([
    core_1.Component({
        selector: 'herd-button-subscribe',
        inputs: ['user'],
        template: "\n    <button class=\"herd-subscribe-button\" *ngIf=\"!_user.subscribed\" (click)=\"subscribe()\">\n      <i class=\"material-icons\">person_add</i>\n      <!-- i18n: @@M__ACTION__SUBSCRIBE -->Subscribe<!-- /i18n -->\n    </button>\n    <button class=\"herd-subscribe-button *ngIf=\"_user.subscribed\" (click)=\"unSubscribe()\">\n      <i class=\"material-icons\">person_add</i>\n      <!-- i18n: @@MINDS__BUTTONS__SUBSCRIBE__SUBSCRIBED_LABEL -->Subscribed<!-- /i18n -->\n    </button>\n  "
    }),
    __metadata("design:paramtypes", [api_1.Client, service_1.SignupModalService])
], SubscribeButton);
exports.SubscribeButton = SubscribeButton;
//# sourceMappingURL=subscribe.js.map