"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../../../../services/api");
var session_1 = require("../../../../../services/session");
var scroll_1 = require("../../../../../services/ux/scroll");
var attachment_1 = require("../../../../../services/attachment");
var translation_1 = require("../../../../../services/translation");
var overlay_modal_1 = require("../../../../../services/ux/overlay-modal");
var creator_component_1 = require("../../../../boost/creator/creator.component");
var creator_component_2 = require("../../../../wire/creator/creator.component");
var Activity = (function () {
    function Activity(client, scroll, _element, attachment, translationService, overlayModal) {
        this.client = client;
        this.scroll = scroll;
        this.attachment = attachment;
        this.translationService = translationService;
        this.overlayModal = overlayModal;
        this.minds = window.Minds;
        this.commentsToggle = false;
        this.shareToggle = false;
        this.deleteToggle = false;
        this.translateToggle = false;
        this.translateEvent = new core_1.EventEmitter();
        this.session = session_1.SessionFactory.build();
        this.showBoostOptions = false;
        this.boost = false;
        this.visible = false;
        this.editing = false;
        this._delete = new core_1.EventEmitter();
        this.commentsOpened = new core_1.EventEmitter();
        this.childEventsEmitter = new core_1.EventEmitter();
        this.onViewed = new core_1.EventEmitter();
        this.canDelete = false;
        this.menuOptions = ['edit', 'translate', 'share', 'mute', 'feature', 'delete', 'report', 'set-explicit', 'block'];
        this.element = _element.nativeElement;
        this.isVisible();
    }
    Object.defineProperty(Activity.prototype, "object", {
        set: function (value) {
            if (!value)
                return;
            this.activity = value;
            this.activity.url = window.Minds.site_url + 'newsfeed/' + value.guid;
            if (!this.activity.message) {
                this.activity.message = '';
            }
            if (!this.activity.title) {
                this.activity.title = '';
            }
            this.isTranslatable = (this.translationService.isTranslatable(this.activity) ||
                (this.activity.remind_object && this.translationService.isTranslatable(this.activity.remind_object)));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Activity.prototype, "boostToggle", {
        set: function (toggle) {
            return;
        },
        enumerable: true,
        configurable: true
    });
    Activity.prototype.save = function () {
        console.log('trying to save your changes to the server', this.activity);
        this.editing = false;
        this.activity.edited = true;
        this.client.post('api/v1/newsfeed/' + this.activity.guid, this.activity);
    };
    Activity.prototype.delete = function ($event) {
        var _this = this;
        if ($event === void 0) { $event = {}; }
        if ($event.inProgress) {
            $event.inProgress.emit(true);
        }
        this.client.delete("api/v1/newsfeed/" + this.activity.guid)
            .then(function (response) {
            if ($event.inProgress) {
                $event.inProgress.emit(false);
                $event.completed.emit(0);
            }
            _this._delete.next(_this.activity);
        })
            .catch(function (e) {
            if ($event.inProgress) {
                $event.inProgress.emit(false);
                $event.completed.emit(1);
            }
        });
    };
    Activity.prototype.openComments = function () {
        this.commentsToggle = !this.commentsToggle;
        this.commentsOpened.emit(this.commentsToggle);
    };
    Activity.prototype.togglePin = function () {
        var _this = this;
        if (this.session.getLoggedInUser().guid != this.activity.owner_guid) {
            return;
        }
        var action = 'pin';
        if (this.activity.pinned) {
            action = 'unpin';
        }
        this.activity.pinned = !this.activity.pinned;
        this.client.post("api/v1/newsfeed/" + action + "/" + this.activity.guid)
            .catch(function (response) {
            _this.activity.pinned = !_this.activity.pinned;
        });
    };
    Activity.prototype.showBoost = function () {
        var _this = this;
        var boostModal = this.overlayModal.create(creator_component_1.BoostCreatorComponent, this.activity);
        boostModal.onDidDismiss(function () {
            _this.showBoostOptions = false;
        });
        boostModal.present();
    };
    Activity.prototype.showWire = function () {
        this.overlayModal.create(creator_component_2.WireCreatorComponent, this.activity.remind_object ? this.activity.remind_object : this.activity)
            .present();
    };
    Activity.prototype.menuOptionSelected = function (option) {
        switch (option) {
            case 'edit':
                this.editing = true;
                break;
            case 'delete':
                this.delete();
                break;
            case 'set-explicit':
                this.setExplicit(true);
                break;
            case 'remove-explicit':
                this.setExplicit(false);
                break;
            case 'translate':
                this.translateToggle = true;
                break;
        }
    };
    Activity.prototype.setExplicit = function (value) {
        var _this = this;
        var oldValue = this.activity.mature, oldMatureVisibility = this.activity.mature_visibility;
        this.activity.mature = value;
        this.activity.mature_visibility = void 0;
        if (this.activity.custom_data && this.activity.custom_data[0]) {
            this.activity.custom_data[0].mature = value;
        }
        else if (this.activity.custom_data) {
            this.activity.custom_data.mature = value;
        }
        this.client.post("api/v1/entities/explicit/" + this.activity.guid, { value: value ? '1' : '0' })
            .catch(function (e) {
            _this.activity.mature = oldValue;
            _this.activity.mature_visibility = oldMatureVisibility;
            if (_this.activity.custom_data && _this.activity.custom_data[0]) {
                _this.activity.custom_data[0].mature = oldValue;
            }
            else if (_this.activity.custom_data) {
                _this.activity.custom_data.mature = oldValue;
            }
        });
    };
    Activity.prototype.isVisible = function () {
        var _this = this;
        if (this.visible) {
            this.onViewed.emit(this.activity);
            return true;
        }
        this.scroll_listener = this.scroll.listenForView().subscribe(function (view) {
            if (_this.element.offsetTop - _this.scroll.view.clientHeight <= _this.scroll.view.scrollTop && !_this.visible) {
                _this.scroll.unListen(_this.scroll_listener);
                _this.visible = true;
                if (_this.boost) {
                    _this.onViewed.emit(_this.activity);
                }
                else {
                    _this.client.put('api/v1/newsfeed/' + _this.activity.guid + '/view');
                }
            }
        });
    };
    Activity.prototype.ngOnDestroy = function () {
        this.scroll.unListen(this.scroll_listener);
    };
    Activity.prototype.propagateTranslation = function ($event) {
        if (this.activity.remind_object && this.translationService.isTranslatable(this.activity.remind_object)) {
            this.childEventsEmitter.emit({
                action: 'translate',
                args: [$event]
            });
        }
    };
    return Activity;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], Activity.prototype, "boost", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], Activity.prototype, "hideTabs", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean),
    __metadata("design:paramtypes", [Boolean])
], Activity.prototype, "boostToggle", null);
Activity = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-activity',
        host: {
            'class': 'mdl-card mdl-shadow--2dp'
        },
        inputs: ['object', 'commentsToggle', 'visible', 'canDelete'],
        outputs: ['_delete: delete', 'commentsOpened', 'onViewed'],
        templateUrl: 'activity.html'
    }),
    __metadata("design:paramtypes", [api_1.Client,
        scroll_1.ScrollService,
        core_1.ElementRef,
        attachment_1.AttachmentService,
        translation_1.TranslationService,
        overlay_modal_1.OverlayModalService])
], Activity);
exports.Activity = Activity;
//# sourceMappingURL=activity.js.map