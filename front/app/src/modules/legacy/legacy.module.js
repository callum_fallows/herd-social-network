"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var common_module_1 = require("../../common/common.module");
var translate_module_1 = require("../translate/translate.module");
var video_module_1 = require("../video/video.module");
var modals_module_1 = require("../modals/modals.module");
var payments_module_1 = require("../payments/payments.module");
var boost_module_1 = require("../boost/boost.module");
var channel_module_1 = require("../channel/channel.module");
var third_party_networks_module_1 = require("../third-party-networks/third-party-networks.module");
var wire_module_1 = require("../wire/wire.module");
var activity_1 = require("./components/cards/activity/activity");
var comment_1 = require("./components/cards/comment/comment");
var album_1 = require("./components/cards/object/album/album");
var image_1 = require("./components/cards/object/image/image");
var video_1 = require("./components/cards/object/video/video");
var remind_1 = require("./components/cards/remind/remind");
var user_1 = require("./components/cards/user/user");
var boost_1 = require("./components/buttons/boost");
var comment_2 = require("./components/buttons/comment");
var feature_1 = require("./components/buttons/feature");
var monetize_1 = require("./components/buttons/monetize");
var remind_2 = require("./components/buttons/remind");
var subscribe_1 = require("./components/buttons/subscribe");
var thumbs_down_1 = require("./components/buttons/thumbs-down");
var thumbs_up_1 = require("./components/buttons/thumbs-up");
var user_dropdown_1 = require("./components/buttons/user-dropdown");
var banner_1 = require("./components/banner/banner");
var fat_1 = require("./components/banner/fat");
var tags_1 = require("./components/forms/tags-input/tags");
var preview_1 = require("./components/cards/activity/preview");
var social_icons_1 = require("./components/social-icons/social-icons");
var comments_1 = require("./controllers/comments/comments");
var scroll_1 = require("./controllers/comments/scroll");
var poster_1 = require("./controllers/newsfeed/poster/poster");
var post_menu_module_1 = require("../../common/components/post-menu/post-menu.module");
var LegacyModule = (function () {
    function LegacyModule() {
    }
    return LegacyModule;
}());
LegacyModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            common_module_1.CommonModule,
            router_1.RouterModule.forChild([]),
            forms_1.FormsModule,
            forms_1.ReactiveFormsModule,
            translate_module_1.TranslateModule,
            video_module_1.VideoModule,
            payments_module_1.PaymentsModule,
            modals_module_1.ModalsModule,
            boost_module_1.BoostModule,
            channel_module_1.ChannelModule,
            third_party_networks_module_1.ThirdPartyNetworksModule,
            wire_module_1.WireModule,
            post_menu_module_1.PostMenuModule
        ],
        declarations: [
            activity_1.Activity,
            preview_1.ActivityPreview,
            comment_1.CommentCard,
            album_1.AlbumCard,
            image_1.ImageCard,
            video_1.VideoCard,
            remind_1.Remind,
            user_1.UserCard,
            boost_1.BoostButton,
            comment_2.CommentButton,
            feature_1.FeatureButton,
            monetize_1.MonetizeButton,
            remind_2.RemindButton,
            subscribe_1.SubscribeButton,
            thumbs_down_1.ThumbsDownButton,
            thumbs_up_1.ThumbsUpButton,
            user_dropdown_1.UserDropdownButton,
            banner_1.MindsBanner,
            fat_1.MindsFatBanner,
            tags_1.TagsInput,
            social_icons_1.SocialIcons,
            comments_1.Comments,
            scroll_1.CommentsScrollDirective,
            poster_1.Poster
        ],
        exports: [
            activity_1.Activity,
            preview_1.ActivityPreview,
            comment_1.CommentCard,
            album_1.AlbumCard,
            image_1.ImageCard,
            video_1.VideoCard,
            remind_1.Remind,
            user_1.UserCard,
            boost_1.BoostButton,
            comment_2.CommentButton,
            feature_1.FeatureButton,
            monetize_1.MonetizeButton,
            remind_2.RemindButton,
            subscribe_1.SubscribeButton,
            thumbs_down_1.ThumbsDownButton,
            thumbs_up_1.ThumbsUpButton,
            user_dropdown_1.UserDropdownButton,
            banner_1.MindsBanner,
            fat_1.MindsFatBanner,
            tags_1.TagsInput,
            social_icons_1.SocialIcons,
            comments_1.Comments,
            scroll_1.CommentsScrollDirective,
            poster_1.Poster
        ],
        entryComponents: [
            activity_1.Activity,
            preview_1.ActivityPreview,
            comment_1.CommentCard,
            album_1.AlbumCard,
            image_1.ImageCard,
            video_1.VideoCard,
            remind_1.Remind,
            user_1.UserCard,
            boost_1.BoostButton
        ]
    })
], LegacyModule);
exports.LegacyModule = LegacyModule;
//# sourceMappingURL=legacy.module.js.map