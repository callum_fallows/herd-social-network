"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../services/api");
var Checkout = (function () {
    function Checkout(client) {
        this.client = client;
        this.inProgress = false;
        this.confirmation = false;
        this.error = '';
        this.inputed = new core_1.EventEmitter;
        this.done = new core_1.EventEmitter;
        this.amount = 0;
        this.gateway = 'merchants';
        this.nonce = '';
        this.useCreditCard = true;
        this.usePayPal = false;
        this.useBitcoin = false;
        this.init();
    }
    Checkout.prototype.init = function () {
        var _this = this;
        System.import('lib/braintree.js').then(function (braintree) {
            _this.client.get('api/v1/payments/braintree/token/' + _this.gateway)
                .then(function (response) { _this.setupClient(braintree, response.token); });
        });
    };
    Checkout.prototype.setupClient = function (braintree, token) {
        var _this = this;
        this.braintree_client = new braintree.api.Client({ clientToken: token });
        if (this.usePayPal && !window.BraintreeLoaded) {
            braintree.setup(token, 'custom', {
                onReady: function (integration) {
                    _this.bt_checkout = integration;
                    window.BraintreeLoaded = true;
                },
                onPaymentMethodReceived: function (payload) {
                    _this.inputed.next(payload.nonce);
                },
                paypal: {
                    singleUse: false,
                    container: 'paypal-btn'
                },
            });
        }
    };
    Checkout.prototype.setCard = function (card) {
        console.log(card);
        this.card = card;
        this.getCardNonce();
    };
    Checkout.prototype.getCardNonce = function () {
        var _this = this;
        this.inProgress = true;
        this.braintree_client.tokenizeCard({
            number: this.card.number,
            expirationDate: this.card.month + '/' + this.card.year,
            cvv: this.card.sec
        }, function (err, nonce) {
            if (err) {
                _this.error = err;
                _this.inProgress = false;
                return false;
            }
            console.log(err, nonce);
            _this.nonce = nonce;
            _this.inputed.next(nonce);
            _this.inProgress = false;
        });
    };
    Checkout.prototype.purchase = function () {
        var self = this;
        this.inProgress = true;
        this.error = '';
        this.client.post('api/v1/payments/braintree/charge', {
            nonce: this.nonce,
            amount: this.amount,
            merchant_guid: this.merchant_guid
        })
            .then(function (response) {
            if (response.status !== 'success') {
                self.inProgress = false;
                self.error = 'Please check your card details and try again.';
                return false;
            }
            self.confirmation = true;
            self.inProgress = false;
        })
            .catch(function (e) {
            self.inProgress = false;
            self.error = 'there was an error';
        });
    };
    return Checkout;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], Checkout.prototype, "amount", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], Checkout.prototype, "merchant_guid", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], Checkout.prototype, "gateway", void 0);
__decorate([
    core_1.Input('creditcard'),
    __metadata("design:type", Boolean)
], Checkout.prototype, "useCreditCard", void 0);
__decorate([
    core_1.Input('paypal'),
    __metadata("design:type", Boolean)
], Checkout.prototype, "usePayPal", void 0);
__decorate([
    core_1.Input('bitcoin'),
    __metadata("design:type", Boolean)
], Checkout.prototype, "useBitcoin", void 0);
Checkout = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-payments-checkout',
        outputs: ['inputed', 'done'],
        template: "\n\n    <div class=\"mdl-card m-payments-options\" style=\"margin-bottom:8px;\" *ngIf=\"usePayPal || useBitcoin\">\n        <div id=\"paypal-btn\" *ngIf=\"usePayPal\"></div>\n        <div id=\"coinbase-btn\" *ngIf=\"useBitcoin\"></div>\n    </div>\n\n    <minds-checkout-card-input\n      (confirm)=\"setCard($event)\"\n      [hidden]=\"inProgress || confirmation\"\n      *ngIf=\"useCreditCard\">\n    </minds-checkout-card-input>\n    <div [hidden]=\"!inProgress\" class=\"m-checkout-loading\">\n      <div class=\"mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active\" style=\"margin:auto; display:block;\" [mdl]></div>\n      <p i18n=\"@@CHECKOUT__CAPTURING_DETAILS\">Capturing card details...</p>\n    </div>\n\n  "
    }),
    __metadata("design:paramtypes", [api_1.Client])
], Checkout);
exports.Checkout = Checkout;
//# sourceMappingURL=braintree-checkout.js.map