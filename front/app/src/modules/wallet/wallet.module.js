"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var common_module_1 = require("../../common/common.module");
var monetization_overview_module_1 = require("../monetization/monetization.overview.module");
var checkout_module_1 = require("../checkout/checkout.module");
var ads_module_1 = require("../ads/ads.module");
var wallet_component_1 = require("./wallet.component");
var points_overview_component_1 = require("./points-overview.component");
var transactions_component_1 = require("./transactions/transactions.component");
var points_component_1 = require("./transactions/points.component");
var purchase_component_1 = require("./purchase/purchase.component");
var boost_component_1 = require("./boost/boost.component");
var wire_component_1 = require("./wire/wire.component");
var walletRoutes = [
    { path: 'wallet', component: wallet_component_1.WalletComponent,
        children: [
            { path: '', redirectTo: 'transactions', pathMatch: 'full' },
            { path: 'transactions', component: transactions_component_1.WalletTransactionsComponent },
            { path: 'purchase', component: transactions_component_1.WalletTransactionsComponent },
            { path: 'boost/:type/:filter', component: boost_component_1.WalletBoostComponent },
            { path: 'boost/:type', component: boost_component_1.WalletBoostComponent },
            { path: 'boost', component: boost_component_1.WalletBoostComponent },
            { path: 'wire', component: wire_component_1.WalletWireComponent }
        ]
    }
];
var WalletModule = (function () {
    function WalletModule() {
    }
    return WalletModule;
}());
WalletModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            forms_1.ReactiveFormsModule,
            common_module_1.CommonModule,
            checkout_module_1.CheckoutModule,
            monetization_overview_module_1.MonetizationOverviewModule,
            router_1.RouterModule.forChild(walletRoutes),
            ads_module_1.AdsModule
        ],
        declarations: [
            wallet_component_1.WalletComponent,
            points_overview_component_1.PointsOverviewComponent,
            transactions_component_1.WalletTransactionsComponent,
            points_component_1.WalletPointsTransactionsComponent,
            purchase_component_1.WalletPurchaseComponent,
            boost_component_1.WalletBoostComponent,
            wire_component_1.WalletWireComponent
        ],
        exports: [
            wallet_component_1.WalletComponent,
            points_overview_component_1.PointsOverviewComponent,
            transactions_component_1.WalletTransactionsComponent,
            points_component_1.WalletPointsTransactionsComponent,
            purchase_component_1.WalletPurchaseComponent,
            boost_component_1.WalletBoostComponent,
            wire_component_1.WalletWireComponent
        ],
        entryComponents: [wallet_component_1.WalletComponent]
    })
], WalletModule);
exports.WalletModule = WalletModule;
//# sourceMappingURL=wallet.module.js.map