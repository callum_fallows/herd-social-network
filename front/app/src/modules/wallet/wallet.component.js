"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var storage_1 = require("../../services/storage");
var api_1 = require("../../services/api");
var title_1 = require("../../services/ux/title");
var session_1 = require("../../services/session");
var wallet_1 = require("../../services/wallet");
var WalletComponent = (function () {
    function WalletComponent(client, wallet, router, route, title, storage) {
        this.client = client;
        this.wallet = wallet;
        this.router = router;
        this.route = route;
        this.title = title;
        this.storage = storage;
        this.session = session_1.SessionFactory.build();
        this.filter = 'transactions';
        this.points = 0;
        this.transactions = [];
        this.offset = '';
        this.inProgress = false;
        this.moreData = true;
        this.disablePointsAnimation = false;
        this.disablePointsAnimation = !!this.storage.get('disablePointsAnimation');
    }
    WalletComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.session.isLoggedIn()) {
            this.router.navigate(['/login']);
            return;
        }
        this.title.setTitle('Wallet | Minds');
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            if (params['filter']) {
                _this.filter = params['filter'];
            }
        });
    };
    WalletComponent.prototype.ngOnDestroy = function () {
        if (this.paramsSubscription)
            this.paramsSubscription.unsubscribe();
    };
    WalletComponent.prototype.setDisablePointsAnimation = function (value) {
        this.disablePointsAnimation = !!value;
        this.storage.set('disablePointsAnimation', !!value ? '1' : '');
    };
    return WalletComponent;
}());
WalletComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'm-wallet',
        templateUrl: 'wallet.component.html'
    }),
    __metadata("design:paramtypes", [api_1.Client,
        wallet_1.WalletService,
        router_1.Router,
        router_1.ActivatedRoute,
        title_1.MindsTitle,
        storage_1.Storage])
], WalletComponent);
exports.WalletComponent = WalletComponent;
//# sourceMappingURL=wallet.component.js.map