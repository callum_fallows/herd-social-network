"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../../../services/api");
var WalletTransactionsMoney = (function () {
    function WalletTransactionsMoney(client) {
        this.client = client;
        this.ledger = [];
        this.inProgress = false;
        this.moreData = true;
        this.offset = '';
    }
    WalletTransactionsMoney.prototype.ngOnInit = function () {
        this.load(true);
    };
    WalletTransactionsMoney.prototype.load = function (refresh) {
        var _this = this;
        if (refresh === void 0) { refresh = false; }
        if (this.inProgress) {
            return;
        }
        this.inProgress = true;
        if (refresh) {
            this.ledger = [];
            this.moreData = true;
            this.offset = '';
        }
        this.client.get('api/v1/monetization/ledger/list', { offset: this.offset })
            .then(function (response) {
            _this.inProgress = false;
            if (response.ledger) {
                (_a = _this.ledger).push.apply(_a, response.ledger);
            }
            else {
                _this.moreData = false;
            }
            if (response['load-next']) {
                _this.offset = response['load-next'];
            }
            else {
                _this.moreData = false;
            }
            var _a;
        })
            .catch(function (e) {
            _this.inProgress = false;
            _this.moreData = false;
            console.error(e);
        });
    };
    return WalletTransactionsMoney;
}());
WalletTransactionsMoney = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-wallet-transactions-money',
        templateUrl: 'money.component.html'
    }),
    __metadata("design:paramtypes", [api_1.Client])
], WalletTransactionsMoney);
exports.WalletTransactionsMoney = WalletTransactionsMoney;
//# sourceMappingURL=money.component.js.map