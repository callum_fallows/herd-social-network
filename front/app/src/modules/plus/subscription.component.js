"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var client_service_1 = require("../../common/api/client.service");
var PlusSubscriptionComponent = (function () {
    function PlusSubscriptionComponent(client, cd) {
        this.client = client;
        this.cd = cd;
        this.user = window.Minds.user;
        this.inProgress = true;
        this.completed = false;
        this.active = false;
    }
    PlusSubscriptionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.load()
            .then(function () {
            _this.inProgress = false;
            _this.detectChanges();
        });
    };
    PlusSubscriptionComponent.prototype.load = function () {
        var _this = this;
        return this.client.get('api/v1/plus')
            .then(function (_a) {
            var active = _a.active;
            if (active)
                _this.active = true;
            return active;
        })
            .catch(function (e) {
            throw e;
        });
    };
    PlusSubscriptionComponent.prototype.isPlus = function () {
        if (this.user.plus)
            return true;
        return false;
    };
    PlusSubscriptionComponent.prototype.setSource = function (source) {
        this.source = source;
        this.purchase();
    };
    PlusSubscriptionComponent.prototype.purchase = function () {
        var _this = this;
        this.inProgress = true;
        this.error = '';
        this.detectChanges();
        this.client.post('api/v1/plus/subscription', {
            source: this.source
        })
            .then(function (response) {
            _this.inProgress = false;
            _this.source = '';
            _this.completed = true;
            _this.user.plus = true;
            _this.detectChanges();
        })
            .catch(function (e) {
            _this.inProgress = false;
            _this.source = '';
            _this.error = e.message;
            _this.detectChanges();
        });
    };
    PlusSubscriptionComponent.prototype.cancel = function () {
        var _this = this;
        this.inProgress = true;
        this.error = '';
        this.detectChanges();
        return this.client.delete('api/v1/plus/subscription')
            .then(function (response) {
            _this.inProgress = false;
            _this.user.plus = false;
            _this.active = false;
            _this.detectChanges();
        });
    };
    PlusSubscriptionComponent.prototype.detectChanges = function () {
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    return PlusSubscriptionComponent;
}());
PlusSubscriptionComponent = __decorate([
    core_1.Component({
        selector: 'm-plus--subscription',
        templateUrl: 'subscription.component.html',
        changeDetection: core_1.ChangeDetectionStrategy.OnPush
    }),
    __metadata("design:paramtypes", [client_service_1.Client, core_1.ChangeDetectorRef])
], PlusSubscriptionComponent);
exports.PlusSubscriptionComponent = PlusSubscriptionComponent;
//# sourceMappingURL=subscription.component.js.map