"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var session_1 = require("../../../services/session");
var SignupOnActionModal = (function () {
    function SignupOnActionModal() {
        this.open = false;
        this.action = '';
        this.session = session_1.SessionFactory.build();
        this.closed = new core_1.EventEmitter();
        this.minds = window.Minds;
    }
    SignupOnActionModal.prototype.close = function () {
        this.open = false;
        this.closed.next(true);
    };
    return SignupOnActionModal;
}());
SignupOnActionModal = __decorate([
    core_1.Component({
        selector: 'm-modal-signup-on-action',
        inputs: ['open', 'action'],
        outputs: ['closed'],
        template: "\n    <m-modal-signup open=\"true\" subtitle=\"You need to have a channel in order to {{action}}\" i18n-subtitle=\"@@MODALS__SIGNUP__ON_ACTION_SUBTITLE\" *ngIf=\"open\"></m-modal-signup>\n  "
    })
], SignupOnActionModal);
exports.SignupOnActionModal = SignupOnActionModal;
//# sourceMappingURL=signup-on-action.js.map