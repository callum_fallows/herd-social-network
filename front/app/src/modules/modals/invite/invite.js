"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var session_1 = require("../../../services/session");
var InviteModal = (function () {
    function InviteModal() {
        this.open = false;
        this.closed = new core_1.EventEmitter();
        this.url = '';
        this.encodedUrl = '';
        this.embedCode = '';
        this.session = session_1.SessionFactory.build();
    }
    InviteModal.prototype.ngOnInit = function () {
        this.url = window.Minds.site_url + 'register?referrer=' + this.session.getLoggedInUser().username;
        this.encodedUrl = encodeURI(this.url);
    };
    InviteModal.prototype.close = function (e) {
        this.open = false;
        this.closed.next(true);
    };
    InviteModal.prototype.copy = function (e) {
        e.target.select();
        document.execCommand('copy');
    };
    InviteModal.prototype.openWindow = function (url) {
        window.open(url, '_blank', 'width=600, height=300, left=80, top=80');
    };
    InviteModal.prototype.openEmail = function () {
        window.location.href = 'mailto:?subject=Join%20me%20on%20minds&body=Join me on Minds ' + this.encodedUrl;
    };
    return InviteModal;
}());
InviteModal = __decorate([
    core_1.Component({
        selector: 'm-modal-invite',
        inputs: ['open'],
        outputs: ['closed'],
        template: "\n    <m-modal [open]=\"open\" (closed)=\"close($event)\">\n\n      <div class=\"mdl-card__supporting-text\">\n        <!-- i18n: @@MODALS__INVITE__DESCRIPTION -->Send the link below to your friends and get 100 points when they signup.<!-- /i18n -->\n      </div>\n\n      <div class=\"mdl-card__supporting-text\">\n        <input class=\"\" value=\"{{url}}\" (focus)=\"copy($event)\" (click)=\"copy($event)\" autofocus/>\n      </div>\n\n      <div class=\"m-social-share-buttons\">\n        <button class=\"mdl-button mdl-button--raised mdl-color-text--white m-social-share-fb\"\n          (click)=\"openWindow('https://www.facebook.com/sharer/sharer.php?u=' + encodedUrl + '&display=popup&ref=plugin&src=share_button')\">\n          <!-- i18n: @@M__NAMES__FACEBOOK -->Facebook<!-- /i18n -->\n        </button>\n        <button class=\"mdl-button mdl-button--raised mdl-color-text--white m-social-share-twitter\"\n          (click)=\"openWindow('https://twitter.com/intent/tweet?text=Join%20me%20on%20Minds&tw_p=tweetbutton&url=' + encodedUrl)\">\n          <!-- i18n: @@M__NAMES__TWITTER -->Twitter<!-- /i18n -->\n        </button>\n        <button class=\"mdl-button mdl-button--raised mdl-color-text--white m-social-share-email\" (click)=\"openEmail()\">\n          <!-- i18n: @@M__COMMON__EMAIL -->Email<!-- /i18n -->\n        </button>\n      </div>\n\n\n    </m-modal>\n  "
    })
], InviteModal);
exports.InviteModal = InviteModal;
//# sourceMappingURL=invite.js.map