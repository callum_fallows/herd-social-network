"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var title_1 = require("../../services/ux/title");
var client_1 = require("../../services/api/client");
var session_1 = require("../../services/session");
var notification_service_1 = require("./notification.service");
var NotificationsComponent = (function () {
    function NotificationsComponent(client, router, title, notificationService, route) {
        this.client = client;
        this.router = router;
        this.title = title;
        this.notificationService = notificationService;
        this.route = route;
        this.notifications = [];
        this.moreData = true;
        this.offset = '';
        this.inProgress = false;
        this.session = session_1.SessionFactory.build();
        this._filter = 'all';
    }
    NotificationsComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.session.isLoggedIn()) {
            this.router.navigate(['/login']);
            return;
        }
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            if (params['filter']) {
                _this._filter = params['filter'];
                _this.notifications = [];
                _this.load(true);
            }
            if (params['ts']) {
                _this.notifications = [];
                _this.load(true);
                _this.notificationService.clear();
            }
        });
        this.load(true);
        this.notificationService.clear();
        this.title.setTitle('Notifications');
    };
    NotificationsComponent.prototype.ngOnDestroy = function () {
        this.paramsSubscription.unsubscribe();
    };
    NotificationsComponent.prototype.load = function (refresh) {
        var _this = this;
        if (refresh === void 0) { refresh = false; }
        var self = this;
        if (this.inProgress)
            return false;
        if (refresh)
            this.offset = '';
        this.inProgress = true;
        this.client.get("api/v1/notifications/" + this._filter, { limit: 24, offset: this.offset })
            .then(function (data) {
            if (!data.notifications) {
                self.moreData = false;
                self.inProgress = false;
                return false;
            }
            if (refresh) {
                self.notifications = data.notifications;
            }
            else {
                for (var _i = 0, _a = data.notifications; _i < _a.length; _i++) {
                    var entity = _a[_i];
                    self.notifications.push(entity);
                }
            }
            if (!data['load-next'])
                _this.moreData = false;
            self.offset = data['load-next'];
            self.inProgress = false;
        });
    };
    NotificationsComponent.prototype.loadEntity = function (entity) {
        var _this = this;
        if (entity.type === 'comment') {
            this.client.get('api/v1/entities/entity/' + entity.parent_guid)
                .then(function (response) {
                _this.entity = response.entity;
            });
        }
        else {
            this.entity = entity;
        }
    };
    NotificationsComponent.prototype.changeFilter = function (filter) {
        this._filter = filter;
        this.notifications = [];
        this.load(true);
    };
    return NotificationsComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], NotificationsComponent.prototype, "params", void 0);
NotificationsComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-notifications',
        templateUrl: 'notifications.component.html'
    }),
    __metadata("design:paramtypes", [client_1.Client,
        router_1.Router,
        title_1.MindsTitle,
        notification_service_1.NotificationService,
        router_1.ActivatedRoute])
], NotificationsComponent);
exports.NotificationsComponent = NotificationsComponent;
//# sourceMappingURL=notifications.component.js.map