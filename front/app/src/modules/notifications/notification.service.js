"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var session_1 = require("../../services/session");
var NotificationService = (function () {
    function NotificationService(client, sockets, title) {
        this.client = client;
        this.sockets = sockets;
        this.title = title;
        this.session = session_1.SessionFactory.build();
        this.socketSubscriptions = {
            notification: null
        };
        this.onReceive = new core_1.EventEmitter();
        if (!window.Minds.notifications_count)
            window.Minds.notifications_count = 0;
        this.listen();
    }
    NotificationService._ = function (client, sockets, title) {
        return new NotificationService(client, sockets, title);
    };
    NotificationService.prototype.listen = function () {
        var _this = this;
        this.socketSubscriptions.notification = this.sockets.subscribe('notification', function (guid) {
            _this.increment();
            _this.client.get("api/v1/notifications/single/" + guid)
                .then(function (response) {
                if (response.notification) {
                    _this.onReceive.next(response.notification);
                }
            });
        });
    };
    NotificationService.prototype.increment = function (notifications) {
        if (notifications === void 0) { notifications = 1; }
        window.Minds.notifications_count = window.Minds.notifications_count + notifications;
        this.sync();
    };
    NotificationService.prototype.clear = function () {
        window.Minds.notifications_count = 0;
        this.sync();
    };
    NotificationService.prototype.getNotifications = function () {
        var self = this;
        setInterval(function () {
            console.log('getting notifications');
            if (!self.session.isLoggedIn())
                return;
            if (!window.Minds.notifications_count)
                window.Minds.notifications_count = 0;
            self.client.get('api/v1/notifications/count', {})
                .then(function (response) {
                window.Minds.notifications_count = response.count;
                self.sync();
            });
        }, 60000);
    };
    NotificationService.prototype.sync = function () {
        for (var i in window.Minds.navigation.topbar) {
            if (window.Minds.navigation.topbar[i].name === 'Notifications') {
                window.Minds.navigation.topbar[i].extras.counter = window.Minds.notifications_count;
            }
        }
        this.title.setCounter(window.Minds.notifications_count);
    };
    return NotificationService;
}());
exports.NotificationService = NotificationService;
//# sourceMappingURL=notification.service.js.map