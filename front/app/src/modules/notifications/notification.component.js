"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var session_1 = require("../../services/session");
var rejection_reasons_1 = require("../../controllers/admin/boosts/rejection-reasons");
var NotificationComponent = (function () {
    function NotificationComponent() {
        this.session = session_1.SessionFactory.build();
    }
    Object.defineProperty(NotificationComponent.prototype, "_notification", {
        set: function (value) {
            this.notification = value;
        },
        enumerable: true,
        configurable: true
    });
    NotificationComponent.prototype.openMessengerWindow = function (event) {
        if (event) {
            event.preventDefault();
        }
        window.openMessengerWindow();
    };
    NotificationComponent.prototype.findReason = function (code) {
        return rejection_reasons_1.rejectionReasons.find(function (item) {
            return item.code === code;
        });
    };
    return NotificationComponent;
}());
NotificationComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'herd-notification',
        inputs: ['_notification: notification'],
        templateUrl: 'notification.component.html'
    })
], NotificationComponent);
exports.NotificationComponent = NotificationComponent;
//# sourceMappingURL=notification.component.js.map