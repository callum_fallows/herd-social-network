"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var checkout_module_1 = require("../checkout/checkout.module");
var modals_module_1 = require("../modals/modals.module");
var common_module_1 = require("../../common/common.module");
var legacy_module_1 = require("../legacy/legacy.module");
var report_module_1 = require("../report/report.module");
var billing_component_1 = require("./billing/billing.component");
var saved_cards_component_1 = require("./billing/saved-cards/saved-cards.component");
var subscriptions_component_1 = require("./billing/subscriptions/subscriptions.component");
var navigation_component_1 = require("./navigation/navigation.component");
var reported_content_component_1 = require("./reported-content/reported-content.component");
var settingsRoutes = [
    { path: 'settings/billing', component: billing_component_1.SettingsBillingComponent },
    { path: 'settings/reported-content', component: reported_content_component_1.SettingsReportedContentComponent }
];
var SettingsModule = (function () {
    function SettingsModule() {
    }
    return SettingsModule;
}());
SettingsModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            forms_1.ReactiveFormsModule,
            common_module_1.CommonModule,
            checkout_module_1.CheckoutModule,
            modals_module_1.ModalsModule,
            legacy_module_1.LegacyModule,
            router_1.RouterModule.forChild(settingsRoutes),
            report_module_1.ReportModule
        ],
        declarations: [
            billing_component_1.SettingsBillingComponent,
            saved_cards_component_1.SettingsBillingSavedCardsComponent,
            subscriptions_component_1.SettingsBillingSubscriptionsComponent,
            navigation_component_1.SettingsNavigationComponent,
            reported_content_component_1.SettingsReportedContentComponent,
        ],
        exports: [
            saved_cards_component_1.SettingsBillingSavedCardsComponent,
            subscriptions_component_1.SettingsBillingSubscriptionsComponent,
            navigation_component_1.SettingsNavigationComponent
        ],
        entryComponents: [
            navigation_component_1.SettingsNavigationComponent
        ]
    })
], SettingsModule);
exports.SettingsModule = SettingsModule;
//# sourceMappingURL=settings.module.js.map