"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var common_module_1 = require("../../common/common.module");
var forms_1 = require("@angular/forms");
var card_component_1 = require("./card/card.component");
var feed_component_1 = require("./feed.component");
var onboarding_service_1 = require("./onboarding.service");
var OnboardingModule = (function () {
    function OnboardingModule() {
    }
    return OnboardingModule;
}());
OnboardingModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            forms_1.ReactiveFormsModule,
            router_1.RouterModule.forChild([]),
            common_module_1.CommonModule
        ],
        declarations: [
            card_component_1.OnboardingCardComponent,
            feed_component_1.OnboardingFeedComponent
        ],
        providers: [
            onboarding_service_1.OnboardingService
        ],
        exports: [
            card_component_1.OnboardingCardComponent,
            feed_component_1.OnboardingFeedComponent
        ]
    })
], OnboardingModule);
exports.OnboardingModule = OnboardingModule;
//# sourceMappingURL=onboarding.module.js.map