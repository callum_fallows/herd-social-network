"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var session_1 = require("../../../services/session");
var api_1 = require("../../../services/api");
var recent_1 = require("../../../services/ux/recent");
var context_service_1 = require("../../../services/context.service");
var SearchBarSuggestionsComponent = (function () {
    function SearchBarSuggestionsComponent(client, location, recentService, context, cd) {
        this.client = client;
        this.location = location;
        this.recentService = recentService;
        this.context = context;
        this.cd = cd;
        this.suggestions = [];
        this.q = '';
        this.session = session_1.SessionFactory.build();
        this.disabled = false;
    }
    Object.defineProperty(SearchBarSuggestionsComponent.prototype, "_q", {
        set: function (value) {
            var _this = this;
            if (this.searchTimeout) {
                clearTimeout(this.searchTimeout);
            }
            this.q = value || '';
            if (!value || this.location.path().indexOf('/search') === 0) {
                this.loadRecent();
                this.currentContext = null;
                this.suggestions = [];
                return;
            }
            this.currentContext = this.context.get();
            this.searchTimeout = setTimeout(function () { return __awaiter(_this, void 0, void 0, function () {
                var response, e_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            this.loadRecent();
                            _a.label = 1;
                        case 1:
                            _a.trys.push([1, 3, , 4]);
                            return [4 /*yield*/, this.client.get('api/v2/search/suggest', {
                                    q: value,
                                    limit: 4
                                })];
                        case 2:
                            response = _a.sent();
                            this.suggestions = response.entities;
                            return [3 /*break*/, 4];
                        case 3:
                            e_1 = _a.sent();
                            console.error(e_1);
                            this.suggestions = [];
                            return [3 /*break*/, 4];
                        case 4: return [2 /*return*/];
                    }
                });
            }); }, 300);
        },
        enumerable: true,
        configurable: true
    });
    SearchBarSuggestionsComponent.prototype.ngOnInit = function () {
        this.loadRecent();
    };
    SearchBarSuggestionsComponent.prototype.loadRecent = function () {
        if (this.session.getLoggedInUser()) {
            this.recent = this.recentService.fetch('recent', 6);
        }
    };
    SearchBarSuggestionsComponent.prototype.mousedown = function (e) {
        var _this = this;
        e.preventDefault();
        setTimeout(function () {
            _this.active = false;
            _this.detectChanges();
        }, 300);
    };
    SearchBarSuggestionsComponent.prototype.detectChanges = function () {
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    return SearchBarSuggestionsComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], SearchBarSuggestionsComponent.prototype, "active", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], SearchBarSuggestionsComponent.prototype, "disabled", void 0);
__decorate([
    core_1.Input('q'),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [String])
], SearchBarSuggestionsComponent.prototype, "_q", null);
SearchBarSuggestionsComponent = __decorate([
    core_1.Component({
        selector: 'm-search--bar-suggestions',
        templateUrl: 'suggestions.component.html'
    }),
    __metadata("design:paramtypes", [api_1.Client,
        common_1.Location,
        recent_1.RecentService,
        context_service_1.ContextService,
        core_1.ChangeDetectorRef])
], SearchBarSuggestionsComponent);
exports.SearchBarSuggestionsComponent = SearchBarSuggestionsComponent;
//# sourceMappingURL=suggestions.component.js.map