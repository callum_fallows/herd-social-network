"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var testing_2 = require("@angular/router/testing");
var creator_component_1 = require("./creator.component");
var client_1 = require("../../../services/api/client");
var client_mock_spec_1 = require("../../../../tests/client-mock.spec");
var abbr_1 = require("../../../common/pipes/abbr");
var material_mock_spec_1 = require("../../../../tests/material-mock.spec");
var material_switch_mock_spec_1 = require("../../../../tests/material-switch-mock.spec");
var overlay_modal_service_mock_spec_1 = require("../../../../tests/overlay-modal-service-mock.spec");
var overlay_modal_1 = require("../../../services/ux/overlay-modal");
var wire_service_1 = require("../wire.service");
var WireCreatorRewardsComponentMock = (function () {
    function WireCreatorRewardsComponentMock() {
        this.selectAmount = new core_1.EventEmitter(true);
    }
    return WireCreatorRewardsComponentMock;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], WireCreatorRewardsComponentMock.prototype, "rewards", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], WireCreatorRewardsComponentMock.prototype, "type", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], WireCreatorRewardsComponentMock.prototype, "amount", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], WireCreatorRewardsComponentMock.prototype, "channel", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], WireCreatorRewardsComponentMock.prototype, "sums", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], WireCreatorRewardsComponentMock.prototype, "selectAmount", void 0);
WireCreatorRewardsComponentMock = __decorate([
    core_1.Component({
        selector: 'm-wire--creator-rewards',
        template: ''
    })
], WireCreatorRewardsComponentMock);
exports.WireCreatorRewardsComponentMock = WireCreatorRewardsComponentMock;
var StripeCheckoutMock = (function () {
    function StripeCheckoutMock() {
        this.inputed = new core_1.EventEmitter;
        this.done = new core_1.EventEmitter;
        this.amount = 0;
        this.gateway = 'merchants';
        this.useMDLStyling = true;
        this.useCreditCard = true;
        this.useBitcoin = false;
    }
    return StripeCheckoutMock;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], StripeCheckoutMock.prototype, "amount", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], StripeCheckoutMock.prototype, "merchant_guid", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], StripeCheckoutMock.prototype, "gateway", void 0);
__decorate([
    core_1.Input('useMDLStyling'),
    __metadata("design:type", Boolean)
], StripeCheckoutMock.prototype, "useMDLStyling", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], StripeCheckoutMock.prototype, "useCreditCard", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], StripeCheckoutMock.prototype, "useBitcoin", void 0);
StripeCheckoutMock = __decorate([
    core_1.Component({
        selector: 'minds-payments-stripe-checkout',
        outputs: ['inputed', 'done'],
        template: ''
    })
], StripeCheckoutMock);
exports.StripeCheckoutMock = StripeCheckoutMock;
describe('WireCreatorComponent', function () {
    var comp;
    var fixture;
    var submitSection;
    var sendButton;
    var owner = {
        'guid': '123',
        'type': 'user',
        'subtype': false,
        'time_created': '1500037446',
        'time_updated': false,
        'container_guid': '0',
        'owner_guid': '0',
        'site_guid': false,
        'access_id': '2',
        'name': 'minds',
        'username': 'minds',
        'language': 'en',
        'icontime': false,
        'legacy_guid': false,
        'featured_id': false,
        'banned': 'no',
        'website': '',
        'briefdescription': 'test',
        'dob': '',
        'gender': '',
        'city': '',
        'merchant': {
            'service': 'stripe',
            'id': 'acct_1ApIzEA26BgQpK9C',
            'exclusive': { 'background': 1502453050, 'intro': '' }
        },
        'boostProPlus': false,
        'fb': false,
        'mature': 0,
        'monetized': '',
        'signup_method': false,
        'social_profiles': [],
        'feature_flags': false,
        'programs': ['affiliate'],
        'plus': false,
        'verified': false,
        'disabled_boost': false,
        'categories': ['news', 'film', 'spirituality'],
        'wire_rewards': null,
        'subscribed': false,
        'subscriber': false,
        'subscribers_count': 1,
        'subscriptions_count': 1,
        'impressions': 337,
        'boost_rating': '2'
    };
    function getPaymentMethodItem(i) {
        return fixture.debugElement.query(platform_browser_1.By.css(".m-wire--creator-selector > li:nth-child(" + i + ")"));
    }
    function getAmountInput() {
        return fixture.debugElement.query(platform_browser_1.By.css('input.m-wire--creator-wide-input--edit'));
    }
    function getAmountLabel() {
        return fixture.debugElement.query(platform_browser_1.By.css('span.m-wire--creator-wide-input--label'));
    }
    function getRecurringCheckbox() {
        return fixture.debugElement.query(platform_browser_1.By.css('.m-wire--creator--recurring input[type=checkbox]'));
    }
    function getErrorLabel() {
        return fixture.debugElement.query(platform_browser_1.By.css('.m-wire--creator--submit-error'));
    }
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                material_mock_spec_1.MaterialMock,
                material_switch_mock_spec_1.MaterialSwitchMock,
                abbr_1.AbbrPipe,
                WireCreatorRewardsComponentMock,
                StripeCheckoutMock,
                creator_component_1.WireCreatorComponent
            ],
            imports: [forms_1.FormsModule, testing_2.RouterTestingModule],
            providers: [
                { provide: client_1.Client, useValue: client_mock_spec_1.clientMock },
                wire_service_1.WireService,
                { provide: overlay_modal_1.OverlayModalService, useValue: overlay_modal_service_mock_spec_1.overlayModalServiceMock }
            ]
        })
            .compileComponents();
    }));
    beforeEach(function (done) {
        jasmine.MAX_PRETTY_PRINT_DEPTH = 10;
        jasmine.clock().uninstall();
        jasmine.clock().install();
        fixture = testing_1.TestBed.createComponent(creator_component_1.WireCreatorComponent);
        comp = fixture.componentInstance;
        client_mock_spec_1.clientMock.response = {};
        client_mock_spec_1.clientMock.response["api/v1/boost/rates"] = {
            'status': 'success',
            'balance': 301529,
            'hasPaymentMethod': false,
            'rate': 1,
            'cap': 5000,
            'min': 100,
            'priority': 1,
            'usd': 1000,
            'minUsd': 1
        };
        client_mock_spec_1.clientMock.response["api/v1/wire/rewards/" + owner.guid] = {
            'status': 'success',
            'username': 'minds',
            'wire_rewards': {
                'description': 'description',
                'rewards': {
                    'points': [{ 'amount': 10, 'description': 'description' }, {
                            'amount': 100,
                            'description': 'description'
                        }],
                    'money': [{ 'amount': 1, 'description': 'description' }, {
                            'amount': 10,
                            'description': ':)'
                        }, { 'amount': 1000, 'description': 'description' }]
                }
            },
            'merchant': {
                'service': 'stripe',
                'id': 'acct_123',
                'exclusive': { 'background': 1502474954, 'intro': 'Support me!' }
            },
            'sums': { 'points': '40', 'money': '3096' }
        };
        submitSection = fixture.debugElement.query(platform_browser_1.By.css('.m-wire--creator-section--last'));
        sendButton = fixture.debugElement.query(platform_browser_1.By.css('.m-wire--creator--submit > button.m-wire--creator-button'));
        comp.owner = owner;
        fixture.detectChanges();
        if (fixture.isStable()) {
            done();
        }
        else {
            fixture.whenStable().then(function () {
                done();
            });
        }
    });
    afterEach(function () {
        jasmine.clock().uninstall();
    });
    it('should have a title', function () {
        var title = fixture.debugElement.query(platform_browser_1.By.css('.m-wire--creator--header span'));
        expect(title).not.toBeNull();
        expect(title.nativeElement.textContent).toContain('Wire');
    });
    it('should have the target user\'s avatar', function () {
        var avatar = fixture.debugElement.query(platform_browser_1.By.css('.m-wire--creator--header-text .m-wire--avatar'));
        expect(avatar).not.toBeNull();
        var avatarAnchor = avatar.query(platform_browser_1.By.css('a'));
        expect(avatarAnchor).not.toBeNull();
        expect(avatarAnchor.nativeElement.href).toContain('/' + comp.owner.username);
        var avatarImage = avatarAnchor.query(platform_browser_1.By.css('img'));
        expect(avatarImage).not.toBeNull();
        expect(avatarImage.nativeElement.src).toContain('icon/' + comp.owner.guid);
    });
    it('should have subtext', function () {
        var subtitle = fixture.debugElement.query(platform_browser_1.By.css('.m-wire-creator--subtext'));
        expect(subtitle).not.toBeNull();
        expect(subtitle.nativeElement.textContent).toContain('Support @' + comp.owner.username + ' by sending them dollars, points or Bitcoin. Once you send them the amount listed in the tiers, you can receive rewards if they are offered. Otherwise, it\'s a donation.');
    });
    it('should have a payment section', function () {
        var section = fixture.debugElement.query(platform_browser_1.By.css('section.m-wire--creator-payment-section'));
        expect(section).not.toBeNull();
    });
    it('payment section should have a title that says \'Wire Type\'', function () {
        var title = fixture.debugElement.query(platform_browser_1.By.css('section.m-wire--creator-payment-section > .m-wire--creator-section-title--small'));
        expect(title).not.toBeNull();
        expect(title.nativeElement.textContent).toContain('Wire Type');
    });
    it('should have payment method list (points, usd and bitcoin)', function () {
        var list = fixture.debugElement.query(platform_browser_1.By.css('section.m-wire--creator-payment-section > ul.m-wire--creator-selector'));
        expect(list).not.toBeNull();
        expect(list.nativeElement.children.length).toBe(3);
        expect(fixture.debugElement.query(platform_browser_1.By.css('.m-wire--creator-selector > li:first-child > .m-wire--creator-selector-type > h4')).nativeElement.textContent).toContain('Points');
        expect(fixture.debugElement.query(platform_browser_1.By.css('.m-wire--creator-selector > li:nth-child(2) > .m-wire--creator-selector-type > h4')).nativeElement.textContent).toContain('USD');
        expect(fixture.debugElement.query(platform_browser_1.By.css('.m-wire--creator-selector > li:last-child > .m-wire--creator-selector-type > h4')).nativeElement.textContent).toContain('Bitcoin');
    });
    it('usd payment option should only be available if the user\'s a merchant', testing_1.fakeAsync(function () {
        comp.owner.merchant = undefined;
        fixture.detectChanges();
        var moneyOption = getPaymentMethodItem(2);
        expect(moneyOption.nativeElement.classList.contains('m-wire--creator-selector--disabled')).toBeTruthy();
    }));
    it('clicking on a payment option should highlight it', testing_1.fakeAsync(function () {
        testing_1.tick();
        var moneyOption = getPaymentMethodItem(2);
        expect(moneyOption.nativeElement.classList.contains('m-wire--creator-selector--highlight')).toBeFalsy();
        moneyOption.nativeElement.click();
        fixture.detectChanges();
        testing_1.tick();
        expect(moneyOption.nativeElement.classList.contains('m-wire--creator-selector--highlight')).toBeTruthy();
    }));
    it('if selected payment method is usd, then a card selector should appear', testing_1.fakeAsync(function () {
        testing_1.tick();
        comp.setCurrency('money');
        fixture.detectChanges();
        expect(fixture.debugElement.query(platform_browser_1.By.css('.m-wire--creator-payment'))).not.toBeNull();
    }));
    it('should have an amount section', function () {
        expect(fixture.debugElement.query(platform_browser_1.By.css('.m-wire--creator--amount'))).not.toBeNull();
    });
    it('amount section should have an input and a label', function () {
        expect(getAmountInput()).not.toBeNull();
        expect(getAmountLabel()).not.toBeNull();
    });
    it('changing amount input should change the wire\'s amount', function () {
        var amountInput = getAmountInput();
        amountInput.nativeElement.value = '10';
        amountInput.nativeElement.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        expect(comp.wire.amount).toBe(10);
    });
    it('changing currency should change amount label', function () {
        var label = getAmountLabel();
        expect(label.nativeElement.textContent).toContain('points');
        comp.setCurrency('money');
        fixture.detectChanges();
        expect(label.nativeElement.textContent).toContain('USD');
    });
    it('should have a recurring checkbox', function () {
        comp.setCurrency('money');
        fixture.detectChanges();
        expect(getRecurringCheckbox()).not.toBeNull();
    });
    it('recurring checkbox should toggle wire\'s recurring property', function () {
        comp.setCurrency('money');
        fixture.detectChanges();
        expect(comp.wire.recurring).toBe(true);
        var checkbox = getRecurringCheckbox();
        checkbox.nativeElement.click();
        checkbox.nativeElement.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        expect(comp.wire.recurring).toBe(false);
    });
    it('should show creator rewards', function () {
        expect(fixture.debugElement.query(platform_browser_1.By.css('m-wire--creator-rewards'))).not.toBeNull();
    });
    it('should have a submit section', function () {
        expect(submitSection).not.toBeNull();
    });
    it('if there are any errors, hovering over the submit section should show them', testing_1.fakeAsync(function () {
        spyOn(comp, 'showErrors').and.callThrough();
        spyOn(comp, 'validate').and.callFake(function () {
            throw new creator_component_1.VisibleWireError('I\'m an error');
        });
        submitSection.nativeElement.dispatchEvent(new Event('mouseenter'));
        fixture.detectChanges();
        testing_1.tick();
        expect(comp.showErrors).toHaveBeenCalled();
        expect(getErrorLabel().nativeElement.textContent).toContain('I\'m an error');
    }));
    it('should have a send button', function () {
        expect(sendButton).not.toBeNull();
    });
    it('send button should be disabled either if the user hasn\'t entered data, there\'s an error, the component\'s loading something or just saved the wire', function () {
        spyOn(comp, 'canSubmit').and.returnValue(true);
        fixture.detectChanges();
        expect(sendButton.nativeElement.disabled).toBeFalsy();
        comp.criticalError = true;
        fixture.detectChanges();
        expect(sendButton.nativeElement.disabled).toBeTruthy();
        comp.criticalError = false;
        comp.inProgress = true;
        fixture.detectChanges();
        expect(sendButton.nativeElement.disabled).toBeTruthy();
        comp.inProgress = false;
        comp.success = true;
        fixture.detectChanges();
        expect(sendButton.nativeElement.disabled).toBeTruthy();
        comp.success = false;
        comp.canSubmit.and.returnValue(false);
        fixture.detectChanges();
        expect(sendButton.nativeElement.disabled).toBeTruthy();
    });
    it('send button should call submit()', function () {
        spyOn(comp, 'submit').and.callThrough();
        spyOn(comp, 'canSubmit').and.returnValue(true);
        fixture.detectChanges();
        sendButton.nativeElement.click();
        fixture.detectChanges();
        expect(comp.submit).toHaveBeenCalled();
    });
    it('send a correct wire', testing_1.fakeAsync(function () {
        spyOn(comp, 'submit').and.callThrough();
        spyOn(comp, 'canSubmit').and.returnValue(true);
        var pointsOption = getPaymentMethodItem(1);
        pointsOption.nativeElement.click();
        fixture.detectChanges();
        var amountInput = getAmountInput();
        amountInput.nativeElement.value = '10';
        amountInput.nativeElement.dispatchEvent(new Event('input'));
        fixture.detectChanges();
        fixture.detectChanges();
        client_mock_spec_1.clientMock.post.calls.reset();
        client_mock_spec_1.clientMock.response["api/v1/wire/" + comp.wire.guid] = { 'status': 'success' };
        sendButton.nativeElement.click();
        fixture.detectChanges();
        testing_1.tick();
        expect(client_mock_spec_1.clientMock.post).toHaveBeenCalled();
        var args = client_mock_spec_1.clientMock.post.calls.mostRecent().args;
        expect(args[0]).toBe("api/v1/wire/" + comp.wire.guid);
        expect(args[1]).toEqual({
            payload: null,
            method: 'points',
            amount: 10,
            recurring: false
        });
    }));
});
//# sourceMappingURL=creator.component.spec.js.map