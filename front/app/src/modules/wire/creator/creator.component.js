"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var overlay_modal_1 = require("../../../services/ux/overlay-modal");
var api_1 = require("../../../services/api");
var session_1 = require("../../../services/session");
var wire_service_1 = require("../wire.service");
var VisibleWireError = (function (_super) {
    __extends(VisibleWireError, _super);
    function VisibleWireError() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.visible = true;
        return _this;
    }
    return VisibleWireError;
}(Error));
exports.VisibleWireError = VisibleWireError;
var WireCreatorComponent = (function () {
    function WireCreatorComponent(wireService, _changeDetectorRef, overlayModal, client, currency) {
        this.wireService = wireService;
        this._changeDetectorRef = _changeDetectorRef;
        this.overlayModal = overlayModal;
        this.client = client;
        this.currency = currency;
        this.minds = window.Minds;
        this.wire = {
            amount: 1000,
            currency: 'points',
            guid: null,
            recurring: false,
            payload: null
        };
        this.rates = {
            balance: null,
            rate: 1,
            min: 250,
            cap: 5000,
            usd: 1,
            btc: 0,
            minUsd: 1,
        };
        this.editingAmount = false;
        this.initialized = false;
        this.inProgress = false;
        this.success = false;
        this.criticalError = false;
        this.error = '';
        this.session = session_1.SessionFactory.build();
    }
    Object.defineProperty(WireCreatorComponent.prototype, "data", {
        set: function (object) {
            this.wire.guid = object ? object.guid : null;
            if (!this.wire.guid && object.entity_guid) {
                this.wire.guid = object.entity_guid;
            }
            this.owner = void 0;
            if (object) {
                if (object.type === 'user') {
                    this.owner = object;
                }
                else if (object.ownerObj) {
                    this.owner = object.ownerObj;
                }
            }
            if (this.initialized) {
                this.syncOwner();
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WireCreatorComponent.prototype, "opts", {
        set: function (opts) {
            this._opts = opts;
            this.setDefaults();
        },
        enumerable: true,
        configurable: true
    });
    WireCreatorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.load()
            .then(function () {
            _this.initialized = true;
            _this.syncOwner();
        });
    };
    WireCreatorComponent.prototype.ngAfterViewInit = function () {
        this.amountEditorFocus();
    };
    WireCreatorComponent.prototype.load = function () {
        var _this = this;
        this.inProgress = true;
        return this.client.get("api/v1/boost/rates")
            .then(function (rates) {
            _this.inProgress = false;
            _this.rates = rates;
            _this.rates = __assign({ btc: 0 }, _this.rates);
        })
            .catch(function (e) {
            _this.inProgress = false;
            _this.criticalError = true;
            _this.error = 'Internal server error';
        });
    };
    WireCreatorComponent.prototype.syncOwner = function () {
        var _this = this;
        if (!this.owner || !this.owner.guid) {
            return;
        }
        this.client.get("api/v1/wire/rewards/" + this.owner.guid)
            .then(function (_a) {
            var merchant = _a.merchant, wire_rewards = _a.wire_rewards, sums = _a.sums;
            _this.owner.merchant = merchant;
            _this.owner.wire_rewards = wire_rewards;
            _this.sums = sums;
            _this.setDefaults();
        });
    };
    WireCreatorComponent.prototype.setDefaults = function () {
        if (this._opts && this._opts.default && typeof this._opts.default === 'object') {
            this.wire.currency = this._opts.default.type;
            this.wire.amount = this._opts.default.min;
            if (!this._opts.disableThresholdCheck && this.sums && this.sums[this._opts.default.type]) {
                this.wire.amount = this.wire.amount - Math.ceil(this.sums[this._opts.default.type]);
            }
        }
        else if (this.owner.merchant) {
            this.wire.currency = 'money';
            this.wire.amount = 1;
            this.wire.recurring = true;
        }
        if (this.wire.amount < 0) {
            this.wire.amount = 0;
        }
    };
    WireCreatorComponent.prototype.setCurrency = function (currency) {
        if ((!this.owner || !this.owner.merchant) && (currency !== 'points')) {
            return;
        }
        var oldCurrency = this.wire.currency;
        this.wire.currency = currency;
        if (currency === 'points') {
            this.wire.recurring = false;
        }
        this.convertCurrency(oldCurrency, currency);
        this.roundAmount();
        this.showErrors();
    };
    WireCreatorComponent.prototype.setNoncePayload = function (nonce) {
        this.wire.payload = { nonce: nonce };
        this.showErrors();
    };
    WireCreatorComponent.prototype.amountEditorFocus = function () {
        this.editingAmount = true;
        if (!this.wire.amount) {
            this.wire.amount = 0;
        }
        this._changeDetectorRef.detectChanges();
    };
    WireCreatorComponent.prototype.setAmount = function (amount) {
        if (!amount) {
            this.wire.amount = 0;
            return;
        }
        if (typeof amount === 'number') {
            this.wire.amount = amount;
            return;
        }
        amount = amount.replace(/,/g, '');
        this.wire.amount = parseFloat(amount);
    };
    WireCreatorComponent.prototype.amountEditorBlur = function () {
        this.editingAmount = false;
        if (!this.wire.amount) {
            this.wire.amount = 0;
        }
        if (this.wire.amount < 0) {
            this.wire.amount = 0;
        }
        this.roundAmount();
        this.showErrors();
    };
    WireCreatorComponent.prototype.roundAmount = function () {
        if (!this.wire.currency || this.wire.currency !== 'points') {
            this.wire.amount = Math.round(parseFloat("" + this.wire.amount) * 100) / 100;
        }
        else {
            this.wire.amount = Math.floor(this.wire.amount);
        }
    };
    WireCreatorComponent.prototype.calcBaseCharges = function (type) {
        switch (type) {
            case 'points':
                return Math.floor(this.wire.amount);
        }
        return this.wire.amount;
    };
    WireCreatorComponent.prototype.calcCharges = function (type) {
        var charges = this.calcBaseCharges(type);
        return charges;
    };
    WireCreatorComponent.prototype.convertCurrency = function (from, to) {
        if (!from || !to) {
            return;
        }
        switch (from) {
            case 'points':
                if (to === 'money') {
                    this.wire.amount = this.wire.amount / this.rates.usd;
                }
                else if (to === 'btc') {
                }
                break;
            case 'money':
                if (to === 'points') {
                    this.wire.amount = this.wire.amount * this.rates.usd;
                }
                else if (to === 'btc') {
                }
                break;
            case 'btc':
                break;
        }
    };
    WireCreatorComponent.prototype.toggleRecurring = function () {
        this.wire.recurring = !this.wire.recurring;
        this.showErrors();
    };
    WireCreatorComponent.prototype.validate = function () {
        if (this.wire.amount <= 0) {
            throw new Error('Amount should be greater than zero.');
        }
        if (!this.wire.currency) {
            throw new Error('You should select a currency.');
        }
        if (this.wire.currency === 'points') {
            var charges = this.calcCharges(this.wire.currency);
            if ((this.rates.balance !== null) && (charges > this.rates.balance)) {
                throw new VisibleWireError("You only have " + this.rates.balance + " points.");
            }
        }
        else {
            if (!this.wire.payload) {
                throw new Error('Payment method not processed.');
            }
        }
        if (!this.wire.guid) {
            throw new Error('You cannot wire this.');
        }
        if (this.wire.currency === 'money') {
            if (this.calcCharges(this.wire.currency) < this.rates.minUsd) {
                throw new VisibleWireError("You must spend at least " + this.currency.transform(this.rates.minUsd, 'USD', true) + " USD");
            }
        }
    };
    WireCreatorComponent.prototype.canSubmit = function () {
        try {
            this.validate();
            return true;
        }
        catch (e) {
        }
        return false;
    };
    WireCreatorComponent.prototype.showErrors = function () {
        this.error = '';
        try {
            this.validate();
        }
        catch (e) {
            if (e.visible) {
                this.error = e.message;
            }
        }
    };
    WireCreatorComponent.prototype.submit = function () {
        var _this = this;
        if (this.inProgress) {
            return;
        }
        if (!this.canSubmit()) {
            this.showErrors();
            return;
        }
        this.inProgress = true;
        var request = this.wireService.submitWire(this.wire);
        request
            .then(function (_a) {
            var done = _a.done;
            _this.inProgress = false;
            if (done) {
                _this.success = true;
                if (_this._opts && _this._opts.onComplete) {
                    _this._opts.onComplete(_this.wire);
                }
                setTimeout(function () {
                    _this.overlayModal.dismiss();
                }, 2500);
            }
        })
            .catch(function (e) {
            _this.inProgress = false;
            if (e && e.message) {
                _this.error = e.message;
            }
            else {
                _this.error = 'Sorry, something went wrong';
            }
        });
    };
    return WireCreatorComponent;
}());
__decorate([
    core_1.Input('object'),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], WireCreatorComponent.prototype, "data", null);
__decorate([
    core_1.ViewChild('amountEditor'),
    __metadata("design:type", core_1.ElementRef)
], WireCreatorComponent.prototype, "_amountEditor", void 0);
WireCreatorComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        providers: [common_1.CurrencyPipe],
        selector: 'm-wire--creator',
        templateUrl: 'creator.component.html'
    }),
    __metadata("design:paramtypes", [wire_service_1.WireService,
        core_1.ChangeDetectorRef,
        overlay_modal_1.OverlayModalService,
        api_1.Client,
        common_1.CurrencyPipe])
], WireCreatorComponent);
exports.WireCreatorComponent = WireCreatorComponent;
//# sourceMappingURL=creator.component.js.map