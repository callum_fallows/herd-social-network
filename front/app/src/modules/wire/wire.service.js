"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var client_1 = require("../../services/api/client");
var WireService = (function () {
    function WireService(client) {
        this.client = client;
        this.wireSent = new core_1.EventEmitter();
    }
    WireService.prototype.submitWire = function (wire) {
        var _this = this;
        return this.client.post("api/v1/wire/" + wire.guid, {
            payload: wire.payload,
            method: wire.currency,
            amount: wire.amount,
            recurring: wire.recurring
        })
            .then(function (response) {
            _this.wireSent.next(wire);
            return { done: true };
        })
            .catch(function (e) {
            if (e && e.stage === 'transaction') {
                throw new Error('Sorry, your payment failed. Please, try again or use another card');
            }
            throw e;
        });
    };
    return WireService;
}());
WireService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [client_1.Client])
], WireService);
exports.WireService = WireService;
//# sourceMappingURL=wire.service.js.map