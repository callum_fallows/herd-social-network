"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WireTypeLabels = [
    { type: 'points', label: 'Points' },
    { type: 'money', label: '$ USD' },
];
//# sourceMappingURL=wire.js.map