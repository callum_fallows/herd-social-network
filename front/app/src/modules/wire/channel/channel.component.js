"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var session_1 = require("../../../services/session");
var overlay_modal_1 = require("../../../services/ux/overlay-modal");
var creator_component_1 = require("../creator/creator.component");
var api_1 = require("../../../services/api");
var wire_1 = require("../wire");
var service_1 = require("../../modals/signup/service");
var WireChannelComponent = (function () {
    function WireChannelComponent(overlayModal, client, signupModal) {
        this.overlayModal = overlayModal;
        this.client = client;
        this.signupModal = signupModal;
        this.rewardsChangeEmitter = new core_1.EventEmitter();
        this.typeLabels = wire_1.WireTypeLabels;
        this.session = session_1.SessionFactory.build();
    }
    Object.defineProperty(WireChannelComponent.prototype, "_rewards", {
        set: function (rewards) {
            if (rewards) {
                this.rewards = rewards;
            }
            else {
                this.reset();
            }
        },
        enumerable: true,
        configurable: true
    });
    WireChannelComponent.prototype.ngOnInit = function () {
        if (!this.rewards) {
            this.reset();
        }
        this.setDefaultDisplay();
    };
    WireChannelComponent.prototype.setDefaultDisplay = function () {
        this.display = 'points';
        if (this.shouldShow('money')) {
            this.display = 'money';
        }
    };
    WireChannelComponent.prototype.toggleEditing = function () {
        this.editing = !this.editing;
        if (!this.editing) {
            this.save();
        }
    };
    WireChannelComponent.prototype.reset = function () {
        this.rewards = {
            description: '',
            rewards: {
                points: [],
                money: []
            }
        };
    };
    WireChannelComponent.prototype.save = function () {
        var _this = this;
        this.rewards.rewards.points = this._cleanAndSortRewards(this.rewards.rewards.points);
        this.rewards.rewards.money = this._cleanAndSortRewards(this.rewards.rewards.money);
        this.client.post('api/v1/wire/rewards', {
            rewards: this.rewards
        })
            .then(function () {
            _this.rewardsChangeEmitter.emit(_this.rewards);
        })
            .catch(function (e) {
            _this.editing = true;
            alert((e && e.message) || 'Server error');
        });
    };
    WireChannelComponent.prototype.sendWire = function () {
        if (!this.session.isLoggedIn()) {
            this.signupModal.open();
            return;
        }
        var creator = this.overlayModal.create(creator_component_1.WireCreatorComponent, this.channel);
        creator.present();
    };
    WireChannelComponent.prototype.isOwner = function () {
        return this.session.getLoggedInUser() && (this.session.getLoggedInUser().guid === this.channel.guid);
    };
    WireChannelComponent.prototype.shouldShow = function (type) {
        var isOwner = this.isOwner();
        if (!type) {
            return isOwner || (this.rewards.description || this.rewards.rewards.points.length || this.rewards.rewards.money.length);
        }
        var canShow = (type === 'points') || this.channel.merchant;
        return canShow && (isOwner || this.rewards.rewards[type].length);
    };
    WireChannelComponent.prototype.getCurrentTypeLabel = function () {
        var _this = this;
        return this.typeLabels.find(function (typeLabel) { return typeLabel.type === _this.display; });
    };
    WireChannelComponent.prototype._cleanAndSortRewards = function (rewards) {
        if (!rewards) {
            return [];
        }
        return rewards
            .filter(function (reward) { return reward.amount || ("" + reward.description).trim(); })
            .map(function (reward) { return (__assign({}, reward, { amount: Math.abs(Math.floor(reward.amount || 0)) })); })
            .sort(function (a, b) { return a.amount > b.amount ? 1 : -1; });
    };
    return WireChannelComponent;
}());
__decorate([
    core_1.Input('rewards'),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], WireChannelComponent.prototype, "_rewards", null);
__decorate([
    core_1.Output('rewardsChange'),
    __metadata("design:type", core_1.EventEmitter)
], WireChannelComponent.prototype, "rewardsChangeEmitter", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], WireChannelComponent.prototype, "channel", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], WireChannelComponent.prototype, "editing", void 0);
WireChannelComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'm-wire-channel',
        templateUrl: 'channel.component.html'
    }),
    __metadata("design:paramtypes", [overlay_modal_1.OverlayModalService, api_1.Client, service_1.SignupModalService])
], WireChannelComponent);
exports.WireChannelComponent = WireChannelComponent;
//# sourceMappingURL=channel.component.js.map