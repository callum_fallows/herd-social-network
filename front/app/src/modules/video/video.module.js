"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var common_module_1 = require("../../common/common.module");
var ads_directive_1 = require("./ads-directive");
var video_1 = require("./video");
var VideoModule = (function () {
    function VideoModule() {
    }
    return VideoModule;
}());
VideoModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            common_module_1.CommonModule,
            router_1.RouterModule.forChild([])
        ],
        declarations: [
            ads_directive_1.VideoAdsDirective,
            video_1.VideoAds,
            video_1.MindsVideo
        ],
        exports: [
            ads_directive_1.VideoAdsDirective,
            video_1.VideoAds,
            video_1.MindsVideo
        ]
    })
], VideoModule);
exports.VideoModule = VideoModule;
//# sourceMappingURL=video.module.js.map