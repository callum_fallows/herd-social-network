"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../services/api");
var scroll_1 = require("../../services/ux/scroll");
var MindsVideo = (function () {
    function MindsVideo(_element, scroll, cd, client) {
        this._element = _element;
        this.scroll = scroll;
        this.cd = cd;
        this.client = client;
        this.src = [];
        this.time = {
            minutes: '00',
            seconds: '00'
        };
        this.elapsed = {
            minutes: '00',
            seconds: '00'
        };
        this.remaining = null;
        this.seeked = 0;
        this.muted = false;
        this.autoplay = false;
        this.visibleplay = true;
        this.loop = true;
        this.poster = '';
        this.playedOnce = false;
        this.playCount = -1;
        this.playCountDisabled = false;
    }
    MindsVideo.prototype.ngOnInit = function () {
        var _this = this;
        this.getElement();
        this.setUp();
        if (this.guid && !this.log) {
            this.log = this.guid;
        }
        if (!this.playCountDisabled && this.log && this.playCount === -1) {
            this.client.get("api/v1/analytics/@counter/play/" + this.log)
                .then(function (response) {
                if (!response.data) {
                    return;
                }
                _this.playCount = response.data;
            });
        }
    };
    Object.defineProperty(MindsVideo.prototype, "_src", {
        set: function (src) {
            this.src = src;
            this.getElement();
            this.element.load();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MindsVideo.prototype, "_muted", {
        set: function (value) {
            this.muted = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MindsVideo.prototype, "_autoplay", {
        set: function (value) {
            if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
                this.autoplay = false;
            }
            else {
                this.autoplay = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MindsVideo.prototype, "_loop", {
        set: function (value) {
            this.loop = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MindsVideo.prototype, "_visibleplay", {
        set: function (value) {
            this.visibleplay = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MindsVideo.prototype, "_playCount", {
        set: function (value) {
            if (!value && value !== 0) {
                if (value === false) {
                    this.playCountDisabled = true;
                }
                return;
            }
            this.playCount = value;
        },
        enumerable: true,
        configurable: true
    });
    MindsVideo.prototype.getElement = function () {
        this.container = this._element.nativeElement;
        this.element = this._element.nativeElement.getElementsByTagName('video')[0];
    };
    MindsVideo.prototype.setUp = function () {
        var _this = this;
        this.element.addEventListener('play', function (e) {
            _this.addViewCount();
        });
        this.element.addEventListener('loadedmetadata', function (e) {
            _this.calculateTime();
        });
    };
    MindsVideo.prototype.addViewCount = function () {
        var _this = this;
        if (!this.log || this.playedOnce) {
            return;
        }
        this.client.put('api/v1/analytics/play/' + this.log)
            .then(function () {
            if (!_this.playCountDisabled) {
                _this.playCount++;
            }
        });
        this.playedOnce = true;
    };
    MindsVideo.prototype.calculateTime = function () {
        var seconds = this.element.duration;
        this.time.minutes = Math.floor(seconds / 60);
        if (parseInt(this.time.minutes) < 10)
            this.time.minutes = '0' + this.time.minutes;
        this.time.seconds = Math.floor(seconds % 60);
        if (parseInt(this.time.seconds) < 10)
            this.time.seconds = '0' + this.time.seconds;
    };
    MindsVideo.prototype.calculateElapsed = function () {
        var seconds = this.element.currentTime;
        this.elapsed.minutes = Math.floor(seconds / 60);
        if (parseInt(this.elapsed.minutes) < 10)
            this.elapsed.minutes = '0' + this.elapsed.minutes;
        this.elapsed.seconds = Math.floor(seconds % 60);
        if (parseInt(this.elapsed.seconds) < 10)
            this.elapsed.seconds = '0' + this.elapsed.seconds;
    };
    MindsVideo.prototype.calculateRemaining = function () {
        if (!this.element.duration || this.element.paused) {
            this.remaining = null;
            return;
        }
        var seconds = this.element.duration - this.element.currentTime;
        this.remaining.minutes = Math.floor(seconds / 60);
        if (parseInt(this.remaining.minutes) < 10)
            this.remaining.minutes = '0' + this.remaining.minutes;
        this.remaining.seconds = Math.floor(seconds % 60);
        if (parseInt(this.remaining.seconds) < 10)
            this.remaining.seconds = '0' + this.remaining.seconds;
    };
    MindsVideo.prototype.onClick = function () {
        console.log(this.element.paused);
        if (this.element.paused === false) {
            this.element.pause();
        }
        else {
            this.element.play();
        }
    };
    MindsVideo.prototype.onMouseEnter = function () {
        this.getSeeker();
    };
    MindsVideo.prototype.onMouseLeave = function () {
        this.stopSeeker();
    };
    MindsVideo.prototype.seek = function (e) {
        e.preventDefault();
        var seeker = e.target;
        var seek = e.offsetX / seeker.offsetWidth;
        var seconds = this.seekerToSeconds(seek);
        this.element.currentTime = seconds;
    };
    MindsVideo.prototype.seekerToSeconds = function (seek) {
        var duration = this.element.duration;
        console.log('seeking to ', duration * seek);
        return duration * seek;
    };
    MindsVideo.prototype.getSeeker = function () {
        var _this = this;
        if (this.seek_interval)
            clearInterval(this.seek_interval);
        this.seek_interval = setInterval(function () {
            _this.seeked = (_this.element.currentTime / _this.element.duration) * 100;
            _this.calculateElapsed();
            _this.calculateRemaining();
            _this.cd.markForCheck();
        }, 100);
    };
    MindsVideo.prototype.stopSeeker = function () {
        clearInterval(this.seek_interval);
    };
    MindsVideo.prototype.openFullScreen = function () {
        if (this.element.requestFullscreen) {
            this.element.requestFullscreen();
        }
        else if (this.element.msRequestFullscreen) {
            this.element.msRequestFullscreen();
        }
        else if (this.element.mozRequestFullScreen) {
            this.element.mozRequestFullScreen();
        }
        else if (this.element.webkitRequestFullscreen) {
            this.element.webkitRequestFullscreen();
        }
    };
    MindsVideo.prototype.isVisible = function () {
        if (this.autoplay)
            return;
        if (!this.visibleplay)
            return;
        if (!this.guid)
            return;
        if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
            this.muted = false;
            return;
        }
        var bounds = this.element.getBoundingClientRect();
        if (bounds.top < this.scroll.view.clientHeight && bounds.top + (this.scroll.view.clientHeight / 2) >= 0) {
            if (this.element.paused === true) {
                this.element.play();
            }
        }
        else {
            if (this.element.paused === false) {
                this.element.muted = true;
                this.element.pause();
            }
        }
    };
    MindsVideo.prototype.ngOnDestroy = function () {
        clearInterval(this.seek_interval);
        if (this.scroll_listener)
            this.scroll.unListen(this.scroll_listener);
    };
    return MindsVideo;
}());
MindsVideo = __decorate([
    core_1.Component({
        selector: 'minds-video',
        inputs: [
            '_src: src',
            '_autoplay: autoplay',
            '_visibleplay: visibleplay',
            '_loop: loop',
            '_muted: muted',
            'poster',
            'guid',
            'log',
            '_playCount: playCount'
        ],
        host: {
            '(mouseenter)': 'onMouseEnter()',
            '(mouseleave)': 'onMouseLeave()'
        },
        template: "\n    <video (click)=\"onClick()\" preload=\"none\" [poster]=\"poster\" allowfullscreen [muted]=\"muted\" [loop]=\"loop\">\n      <source [src]=\"s.uri\" *ngFor=\"let s of src\">\n    </video>\n    <i *ngIf=\"element.paused\" class=\"material-icons minds-video-play-icon\" (click)=\"onClick()\">play_circle_outline</i>\n    <ng-content></ng-content>\n    <div class=\"minds-video-bar-min\" *ngIf=\"remaining\">\n      {{remaining.minutes}}:{{remaining.seconds}}\n    </div>\n    <div class=\"minds-video-bar-full\">\n      <i class=\"material-icons\" [hidden]=\"!element.paused\" (click)=\"onClick()\">play_arrow</i>\n      <i class=\"material-icons\" [hidden]=\"element.paused\" (click)=\"onClick()\">pause</i>\n      <span id=\"seeker\" class=\"progress-bar\" (click)=\"seek($event)\">\n        <div class=\"minds-bar progress\" [ngStyle]=\"{ 'width': seeked + '%'}\"></div>\n        <div class=\"minds-bar total\"></div>\n      </span>\n      <span class=\"progress-stamps\">{{elapsed.minutes}}:{{elapsed.seconds}}/{{time.minutes}}:{{time.seconds}}</span>\n      <i class=\"material-icons\" [hidden]=\"element.muted\" (click)=\"element.muted = true\">volume_up</i>\n      <i class=\"material-icons\" [hidden]=\"!element.muted\" (click)=\"element.muted = false\">volume_off</i>\n      <a class=\"material-icons m-video-full-page mdl-color-text--white\"\n        *ngIf=\"guid\"\n        [routerLink]=\"['/media', guid]\"\n        target=\"_blank\"\n        (click)=\"element.pause()\">\n        lightbulb_outline\n      </a>\n      <i class=\"material-icons\" (click)=\"openFullScreen()\">tv</i>\n      <!--<span class=\"m-play-count\" *ngIf=\"playCount > -1\">\n        <i class=\"material-icons\">ondemand_video</i>\n        <span>{{ playCount }}</span>\n      </span>-->\n    </div>\n  "
    }),
    __metadata("design:paramtypes", [core_1.ElementRef, scroll_1.ScrollService, core_1.ChangeDetectorRef, api_1.Client])
], MindsVideo);
exports.MindsVideo = MindsVideo;
var ads_1 = require("./ads");
exports.VideoAds = ads_1.VideoAds;
//# sourceMappingURL=video.js.map