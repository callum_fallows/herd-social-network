"use strict";
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var api_1 = require("../../services/api");
var MonetizationAnalytics = (function () {
    function MonetizationAnalytics(client, currencyPipe, cd) {
        this.client = client;
        this.currencyPipe = currencyPipe;
        this.cd = cd;
        this.transactions = [];
        this.inProgress = false;
        this.offset = '';
        this.moreData = false;
        this.chart = null;
        this.chartInProgress = false;
    }
    MonetizationAnalytics.prototype.ngOnInit = function () {
        this.load(true);
    };
    MonetizationAnalytics.prototype.load = function (refresh) {
        if (refresh === void 0) { refresh = false; }
        var tasks = [
            this.loadList(refresh)
        ];
        if (refresh) {
            tasks.push(this.loadChart());
        }
        this.detectChanges();
        return Promise.all(tasks);
    };
    MonetizationAnalytics.prototype.loadList = function (refresh) {
        var _this = this;
        if (refresh === void 0) { refresh = false; }
        if (this.inProgress) {
            return;
        }
        this.inProgress = true;
        if (refresh) {
            this.offset = '';
            this.moreData = true;
        }
        this.detectChanges();
        return this.client.get("api/v1/monetization/service/analytics/list", {
            offset: this.offset,
            limit: 12
        })
            .then(function (_a) {
            var transactions = _a.transactions, loadNext = _a["load-next"];
            _this.inProgress = false;
            if (transactions) {
                (_b = _this.transactions).push.apply(_b, transactions);
            }
            if (loadNext) {
                _this.offset = loadNext;
            }
            else {
                _this.moreData = false;
            }
            _this.detectChanges();
            var _b;
        })
            .catch(function (e) {
            _this.inProgress = false;
            _this.error = e.message || 'Server error';
            _this.detectChanges();
        });
    };
    MonetizationAnalytics.prototype.loadChart = function () {
        var _this = this;
        if (this.chartInProgress) {
            return;
        }
        this.chartInProgress = true;
        this.error = '';
        this.detectChanges();
        return this.client.get("api/v1/monetization/service/analytics/chart", {})
            .then(function (_a) {
            var chart = _a.chart;
            _this.chartInProgress = false;
            _this.chart = _this._parseChart(chart);
            _this.detectChanges();
        })
            .catch(function (e) {
            _this.chartInProgress = false;
            _this.error = e.message || 'Server error';
            _this.detectChanges();
        });
    };
    MonetizationAnalytics.prototype.detectChanges = function () {
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    MonetizationAnalytics.prototype._parseChart = function (data) {
        if (!data) {
            return null;
        }
        var chart = {
            title: data.title || void 0,
            columns: [],
            rows: []
        };
        for (var _i = 0, _a = (data.columns || []); _i < _a.length; _i++) {
            var dataColumn = _a[_i];
            var column = __assign({}, dataColumn);
            if (column.type === 'currency') {
                column.type = 'number';
            }
            chart.columns.push(column);
        }
        for (var _b = 0, _c = data.rows; _b < _c.length; _b++) {
            var dataRow = _c[_b];
            for (var colIndex = 0; colIndex < dataRow.length; colIndex++) {
                if (data.columns[colIndex] && data.columns[colIndex].type === 'currency') {
                    dataRow[colIndex] = { v: dataRow[colIndex], f: this.currencyPipe.transform(dataRow[colIndex], 'USD', true) };
                }
            }
            chart.rows.push(dataRow);
        }
        this.detectChanges();
        return chart;
    };
    return MonetizationAnalytics;
}());
MonetizationAnalytics = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'm-monetization--analytics',
        templateUrl: 'analytics.component.html',
        providers: [
            common_1.CurrencyPipe
        ]
    }),
    __metadata("design:paramtypes", [api_1.Client, common_1.CurrencyPipe, core_1.ChangeDetectorRef])
], MonetizationAnalytics);
exports.MonetizationAnalytics = MonetizationAnalytics;
//# sourceMappingURL=analytics.component.js.map