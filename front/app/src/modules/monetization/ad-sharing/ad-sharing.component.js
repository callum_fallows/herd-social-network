"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var api_1 = require("../../../services/api");
var AdSharingComponent = (function () {
    function AdSharingComponent(route, client, fb, cd) {
        this.route = route;
        this.client = client;
        this.fb = fb;
        this.cd = cd;
        this.type = 'analytics';
        this.inProgress = false;
        this.loaded = false;
        this.remote = false;
        this.isMerchant = false;
        this.canBecomeMerchant = false;
        this.enabled = false;
        this.applied = false;
        this.applyInProgress = false;
        this.applyError = '';
        this.applyForm = {
            message: ''
        };
    }
    AdSharingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            if (params['type']) {
                _this.type = params['type'];
            }
            _this.remote = (typeof params['username'] !== 'undefined');
        });
        if (!this.remote) {
            this.load();
        }
    };
    AdSharingComponent.prototype.ngOnDestroy = function () {
        this.paramsSubscription.unsubscribe();
    };
    AdSharingComponent.prototype.load = function () {
        var _this = this;
        if (this.inProgress) {
            return;
        }
        this.inProgress = true;
        this.detectChanges();
        this.client.get("api/v1/monetization/ads/status")
            .then(function (response) {
            _this.inProgress = false;
            _this.loaded = true;
            _this.isMerchant = !!response.isMerchant;
            _this.canBecomeMerchant = !!response.canBecomeMerchant;
            _this.enabled = !!response.enabled;
            _this.applied = !!response.applied;
            _this.detectChanges();
        })
            .catch(function (e) {
            _this.inProgress = false;
            _this.detectChanges();
        });
    };
    AdSharingComponent.prototype.isApplyValid = function () {
        return !this.applyInProgress && this.applyForm.message;
    };
    AdSharingComponent.prototype.apply = function () {
        var _this = this;
        if (this.applyInProgress) {
            return;
        }
        this.applyInProgress = true;
        this.detectChanges();
        this.client.post("api/v1/monetization/ads/apply", this.applyForm)
            .then(function (response) {
            _this.applyInProgress = false;
            _this.applyError = '';
            _this.applied = !!response.applied;
            _this.detectChanges();
        })
            .catch(function (e) {
            _this.applyInProgress = false;
            _this.applyError = e.message ? e.message : 'Unknown error';
            _this.detectChanges();
        });
    };
    AdSharingComponent.prototype.detectChanges = function () {
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    return AdSharingComponent;
}());
AdSharingComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'm-ad-sharing',
        templateUrl: 'ad-sharing.component.html'
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute,
        api_1.Client,
        forms_1.FormBuilder,
        core_1.ChangeDetectorRef])
], AdSharingComponent);
exports.AdSharingComponent = AdSharingComponent;
//# sourceMappingURL=ad-sharing.component.js.map