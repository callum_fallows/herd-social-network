"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../../../services/api");
var AdSharingSettingsComponent = (function () {
    function AdSharingSettingsComponent(client, cd) {
        this.client = client;
        this.cd = cd;
        this.inProgress = false;
        this.loaded = false;
        this.settings = {
            blogs: false
        };
    }
    AdSharingSettingsComponent.prototype.ngOnInit = function () {
        this.load();
    };
    AdSharingSettingsComponent.prototype.load = function () {
        var _this = this;
        this.inProgress = true;
        return this.client.get("api/v1/monetization/ads/settings")
            .then(function (response) {
            _this.inProgress = false;
            _this.loaded = true;
            _this.settings = response.settings;
            _this.detectChanges();
        })
            .catch(function (e) {
            _this.inProgress = false;
            _this.detectChanges();
        });
    };
    AdSharingSettingsComponent.prototype.save = function (key) {
        return this.client.post("api/v1/monetization/ads/settings", (_a = {}, _a[key] = this.settings[key] ? 1 : 0, _a));
        var _a;
    };
    AdSharingSettingsComponent.prototype.detectChanges = function () {
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    return AdSharingSettingsComponent;
}());
AdSharingSettingsComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'settings.component.html',
        selector: 'm-wallet-ad-sharing-settings',
    }),
    __metadata("design:paramtypes", [api_1.Client, core_1.ChangeDetectorRef])
], AdSharingSettingsComponent);
exports.AdSharingSettingsComponent = AdSharingSettingsComponent;
//# sourceMappingURL=settings.component.js.map