"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var api_1 = require("../../../../services/api");
var AdSharingAnalyticsComponent = (function () {
    function AdSharingAnalyticsComponent(client, route, cd) {
        this.client = client;
        this.route = route;
        this.cd = cd;
        this.overview = {
            today: 0,
            last7: 0,
            last28: 0,
            balanceDays: 40,
            balanceAmount: 0,
        };
        this.payouts = {
            status: '',
            available: false,
            amount: 0,
            dates: {
                start: 0,
                end: 0,
            }
        };
        this.breakdown = {
            period: 28,
            dates: {
                start: 0,
                end: 0,
            }
        };
        this.items = [];
        this.period = 28;
        this.username = '';
        this.loaded = false;
        this.isMerchant = false;
        this.canBecomeMerchant = false;
        this.overviewInProgress = false;
        this.payoutRequestInProgress = false;
        this.inProgress = false;
        this.moreData = true;
        this.offset = '';
        this.listLoaded = false;
    }
    AdSharingAnalyticsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            if (typeof params['username'] !== 'undefined') {
                var changed = _this.username !== params['username'];
                _this.username = params['username'];
                if (changed) {
                    _this.load();
                    _this.listLoaded = false;
                    _this.items = [];
                }
                _this.detectChanges();
            }
        });
        this.load()
            .then(function () {
            if (_this.hasBreakdown()) {
                _this.loadList(28, true);
            }
        });
    };
    AdSharingAnalyticsComponent.prototype.ngOnDestroy = function () {
        this.paramsSubscription.unsubscribe();
    };
    AdSharingAnalyticsComponent.prototype.load = function () {
        var _this = this;
        this.overviewInProgress = true;
        this.detectChanges();
        return this.client.get("api/v1/monetization/ads/overview/" + this.username)
            .then(function (response) {
            _this.overviewInProgress = false;
            if (response.overview) {
                _this.overview = response.overview;
            }
            if (response.payouts) {
                _this.payouts = response.payouts;
            }
            _this.isMerchant = !!response.isMerchant;
            _this.canBecomeMerchant = !!response.canBecomeMerchant;
            _this.loaded = true;
            _this.detectChanges();
        })
            .catch(function (e) {
            _this.overviewInProgress = false;
            _this.detectChanges();
        });
    };
    AdSharingAnalyticsComponent.prototype.loadList = function (period, refresh) {
        var _this = this;
        this.breakdown.period = period;
        if (refresh) {
            this.offset = '';
            this.moreData = true;
            this.items = [];
            this.detectChanges();
        }
        if (this.inProgress) {
            return Promise.reject(false);
        }
        this.inProgress = true;
        this.listLoaded = true;
        this.detectChanges();
        return this.client.get("api/v1/monetization/ads/list/" + this.username, { offset: this.offset, period: period })
            .then(function (response) {
            _this.inProgress = false;
            if (response.breakdown && response.breakdown.list) {
                (_a = _this.items).push.apply(_a, response.breakdown.list);
            }
            else {
                _this.moreData = false;
            }
            if (response.breakdown && response.breakdown.dates) {
                _this.breakdown.dates = response.breakdown.dates;
            }
            if (response['load-next']) {
                _this.offset = response['load-next'];
            }
            else {
                _this.moreData = false;
            }
            _this.detectChanges();
            var _a;
        })
            .catch(function (e) {
            _this.inProgress = false;
            _this.detectChanges();
        });
    };
    AdSharingAnalyticsComponent.prototype.payout = function () {
        var _this = this;
        if (!this.canPayout() || this.payoutRequestInProgress) {
            return;
        }
        this.payoutRequestInProgress = true;
        this.detectChanges();
        this.client.post('api/v1/monetization/ads/payout')
            .then(function (response) {
            _this.payoutRequestInProgress = false;
            _this.payouts.available = false;
            _this.payouts.status = 'inprogress';
            _this.detectChanges();
        })
            .catch(function (e) {
            _this.payoutRequestInProgress = false;
            _this.detectChanges();
        });
    };
    AdSharingAnalyticsComponent.prototype.canPayout = function () {
        return !!this.payouts.available;
    };
    AdSharingAnalyticsComponent.prototype.isPayoutInProgress = function () {
        return this.payouts.status === 'inprogress';
    };
    AdSharingAnalyticsComponent.prototype.hasBreakdown = function () {
        return this.payouts && this.payouts.dates.start && this.payouts.dates.end;
    };
    AdSharingAnalyticsComponent.prototype.detectChanges = function () {
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    return AdSharingAnalyticsComponent;
}());
AdSharingAnalyticsComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'analytics.component.html',
        selector: 'm-wallet-ad-sharing-analytics',
    }),
    __metadata("design:paramtypes", [api_1.Client, router_1.ActivatedRoute, core_1.ChangeDetectorRef])
], AdSharingAnalyticsComponent);
exports.AdSharingAnalyticsComponent = AdSharingAnalyticsComponent;
//# sourceMappingURL=analytics.component.js.map