"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var groups_service_1 = require("./groups-service");
var session_1 = require("../../services/session");
var GroupsJoinButton = (function () {
    function GroupsJoinButton(service) {
        this.service = service;
        this.showModal = false;
        this.session = session_1.SessionFactory.build();
        this.membership = new core_1.EventEmitter();
        this.minds = window.Minds;
    }
    Object.defineProperty(GroupsJoinButton.prototype, "_group", {
        set: function (value) {
            this.group = value;
        },
        enumerable: true,
        configurable: true
    });
    GroupsJoinButton.prototype.isMember = function () {
        if (this.group['is:member'])
            return true;
        return false;
    };
    GroupsJoinButton.prototype.isPublic = function () {
        if (this.group.membership !== 2)
            return false;
        return true;
    };
    GroupsJoinButton.prototype.join = function () {
        var _this = this;
        if (!this.session.isLoggedIn()) {
            this.showModal = true;
            return;
        }
        if (this.isPublic()) {
            this.group['is:member'] = true;
        }
        this.service.join(this.group)
            .then(function () {
            if (_this.isPublic()) {
                _this.group['is:member'] = true;
                _this.membership.next({
                    member: true
                });
                return;
            }
            _this.membership.next({});
            _this.group['is:awaiting'] = true;
        })
            .catch(function (e) {
            _this.group['is:member'] = false;
            _this.group['is:awaiting'] = false;
        });
    };
    GroupsJoinButton.prototype.leave = function () {
        var _this = this;
        this.service.leave(this.group)
            .then(function () {
            _this.group['is:member'] = false;
            _this.membership.next({
                member: false
            });
        })
            .catch(function (e) {
            _this.group['is:member'] = true;
        });
    };
    GroupsJoinButton.prototype.accept = function () {
        var _this = this;
        this.group['is:member'] = true;
        this.group['is:invited'] = false;
        this.service.acceptInvitation(this.group)
            .then(function (done) {
            _this.group['is:member'] = done;
            _this.group['is:invited'] = !done;
            if (done) {
                _this.membership.next({
                    member: true
                });
            }
        });
    };
    GroupsJoinButton.prototype.cancelRequest = function () {
        var _this = this;
        this.group['is:awaiting'] = false;
        this.service.cancelRequest(this.group)
            .then(function (done) {
            _this.group['is:awaiting'] = !done;
        });
    };
    GroupsJoinButton.prototype.decline = function () {
        var _this = this;
        this.group['is:member'] = false;
        this.group['is:invited'] = false;
        this.service.declineInvitation(this.group)
            .then(function (done) {
            _this.group['is:member'] = false;
            _this.group['is:invited'] = !done;
        });
    };
    return GroupsJoinButton;
}());
GroupsJoinButton = __decorate([
    core_1.Component({
        selector: 'minds-groups-join-button',
        inputs: ['_group: group'],
        outputs: ['membership'],
        template: "\n    <button class=\"minds-group-join-button\"\n      *ngIf=\"!group['is:banned'] && !group['is:awaiting']\n        && !group['is:invited'] && !group['is:member']\"\n        (click)=\"join()\" i18n=\"@@GROUPS__JOIN_BUTTON__JOIN_ACTION\"\n      >\n      Join\n    </button>\n    <span *ngIf=\"group['is:invited'] &amp;&amp; !group['is:member']\">\n      <button class=\"minds-group-join-button\" (click)=\"accept()\" i18n=\"@@M__ACTION__ACCEPT\">Accept</button>\n      <button class=\"minds-group-join-button\" (click)=\"decline()\" i18n=\"@@GROUPS__JOIN_BUTTON__DECLINE_ACTION\">Decline</button>\n    </span>\n    <button class=\"minds-group-join-button subscribed \" *ngIf=\"group['is:member']\" (click)=\"leave()\" i18n=\"@@GROUPS__JOIN_BUTTON__LEAVE_ACTION\">Leave</button>\n    <button class=\"minds-group-join-button awaiting\" *ngIf=\"group['is:awaiting']\" (click)=\"cancelRequest()\" i18n=\"@@GROUPS__JOIN_BUTTON__CANCEL_REQ_ACTION\">Cancel request</button>\n    <m-modal-signup-on-action\n      [open]=\"showModal\"\n      (closed)=\"showModal = false\"\n      action=\"join a group\"\n      i18n-action=\"@@GROUPS__JOIN_BUTTON__JOIN_A_GROUP_TITLE\"\n      *ngIf=\"!session.isLoggedIn()\">\n    </m-modal-signup-on-action>\n  "
    }),
    __metadata("design:paramtypes", [groups_service_1.GroupsService])
], GroupsJoinButton);
exports.GroupsJoinButton = GroupsJoinButton;
//# sourceMappingURL=groups-join-button.js.map