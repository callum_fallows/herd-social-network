"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var api_1 = require("../../services/api");
var title_1 = require("../../services/ux/title");
var session_1 = require("../../services/session");
var context_service_1 = require("../../services/context.service");
var Groups = (function () {
    function Groups(client, route, title, context) {
        this.client = client;
        this.route = route;
        this.title = title;
        this.context = context;
        this.moreData = true;
        this.inProgress = false;
        this.offset = '';
        this.groups = [];
        this.session = session_1.SessionFactory.build();
        this._filter = 'featured';
    }
    Groups.prototype.ngOnInit = function () {
        var _this = this;
        this.title.setTitle('Groups');
        this.context.set('group');
        this.minds = window.Minds;
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            if (params['filter']) {
                _this._filter = params['filter'];
                _this.inProgress = false;
                _this.offset = '';
                _this.moreData = true;
                _this.groups = [];
                _this.load(true);
            }
        });
    };
    Groups.prototype.ngOnDestroy = function () {
        if (this.paramsSubscription) {
            this.paramsSubscription.unsubscribe();
        }
    };
    Groups.prototype.load = function (refresh) {
        var _this = this;
        if (refresh === void 0) { refresh = false; }
        var endpoint, key;
        switch (this._filter) {
            case 'trending':
                endpoint = "api/v1/entities/" + this._filter + "/groups";
                key = 'entities';
                break;
            default:
                endpoint = "api/v1/groups/" + this._filter;
                key = 'groups';
                break;
        }
        if (this.inProgress)
            return;
        var self = this;
        this.inProgress = true;
        this.client.get(endpoint, { limit: 12, offset: this.offset })
            .then(function (response) {
            if (!response[key] || response[key].length === 0) {
                _this.moreData = false;
                _this.inProgress = false;
                return false;
            }
            if (refresh) {
                _this.groups = response[key];
            }
            else {
                if (_this.offset)
                    response[key].shift();
                (_a = _this.groups).push.apply(_a, response[key]);
            }
            _this.offset = response['load-next'];
            _this.inProgress = false;
            var _a;
        })
            .catch(function (e) {
            _this.inProgress = false;
        });
    };
    return Groups;
}());
Groups = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-groups',
        templateUrl: 'groups.html'
    }),
    __metadata("design:paramtypes", [api_1.Client, router_1.ActivatedRoute, title_1.MindsTitle, context_service_1.ContextService])
], Groups);
exports.Groups = Groups;
var profile_1 = require("./profile/profile");
exports.GroupsProfile = profile_1.GroupsProfile;
var create_1 = require("./create/create");
exports.GroupsCreator = create_1.GroupsCreator;
//# sourceMappingURL=groups.js.map