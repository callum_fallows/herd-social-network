"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var testing_2 = require("@angular/router/testing");
var platform_browser_1 = require("@angular/platform-browser");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var client_1 = require("../../services/api/client");
var client_mock_spec_1 = require("../../../tests/client-mock.spec");
var title_1 = require("../../services/ux/title");
var card_1 = require("../../mocks/modules/groups/card/card");
var infinite_scroll_1 = require("../../mocks/common/components/infinite-scroll/infinite-scroll");
var groups_1 = require("./groups");
var context_service_1 = require("../../services/context.service");
var context_service_mock_spec_1 = require("../../../tests/context-service-mock.spec");
describe('Groups', function () {
    var fixture;
    var comp;
    function getCreateAnchor() {
        return fixture.debugElement.queryAll(platform_browser_1.By.directive(router_1.RouterLinkWithHref))
            .find(function (el) { return el.properties['href'] === '/groups/create'; });
    }
    function getGroupCards() {
        return fixture.debugElement.queryAll(platform_browser_1.By.css('minds-card-group'));
    }
    function getInfiniteScroll() {
        return fixture.debugElement.query(platform_browser_1.By.css('infinite-scroll'));
    }
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                card_1.GroupsCardMock,
                infinite_scroll_1.InfiniteScrollMock,
                groups_1.Groups
            ],
            imports: [
                common_1.CommonModule,
                testing_2.RouterTestingModule
            ],
            providers: [
                { provide: client_1.Client, useValue: client_mock_spec_1.clientMock },
                { provide: title_1.MindsTitle, useClass: platform_browser_1.Title, deps: [platform_browser_1.Title] },
                { provide: context_service_1.ContextService, useValue: context_service_mock_spec_1.contextServiceMock },
            ]
        }).compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(groups_1.Groups);
        comp = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should render a button to create a group', function () {
        expect(getCreateAnchor()).toBeTruthy();
    });
    it('should render a list of groups and an infinite scroll', function () {
        comp.groups = [{}, {}];
        fixture.detectChanges();
        expect(getGroupCards().length).toBe(2);
        expect(getInfiniteScroll()).toBeTruthy();
    });
});
//# sourceMappingURL=groups.spec.js.map