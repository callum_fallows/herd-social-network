"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var signup_on_action_mock_1 = require("../../mocks/modules/modals/signup/signup-on-action.mock");
var client_mock_spec_1 = require("../../../tests/client-mock.spec");
var upload_mock_spec_1 = require("../../../tests/upload-mock.spec");
var groups_join_button_1 = require("./groups-join-button");
var groups_service_1 = require("./groups-service");
describe('GroupsJoinButton', function () {
    var fixture;
    var comp;
    function setGroup(props) {
        comp._group = Object.assign({
            guid: 1000,
            'is:banned': false,
            'is:awaiting': false,
            'is:invited': false,
            'is:member': false,
            membership: 2
        }, props);
    }
    function getJoinButtons() {
        return fixture.debugElement.queryAll(platform_browser_1.By.css('.minds-group-join-button'));
    }
    function getAcceptAndDeclineButtons() {
        return fixture.debugElement.queryAll(platform_browser_1.By.css('span > .minds-group-join-button'));
    }
    function getLeaveButton() {
        return fixture.debugElement.query(platform_browser_1.By.css('.minds-group-join-button.subscribed'));
    }
    function getCancelRequestButton() {
        return fixture.debugElement.query(platform_browser_1.By.css('.minds-group-join-button.awaiting'));
    }
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                signup_on_action_mock_1.SignupOnActionModalMock,
                groups_join_button_1.GroupsJoinButton
            ],
            imports: [],
            providers: [
                { provide: groups_service_1.GroupsService, deps: [client_mock_spec_1.clientMock, upload_mock_spec_1.uploadMock] },
            ]
        }).compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(groups_join_button_1.GroupsJoinButton);
        comp = fixture.componentInstance;
        setGroup({});
        fixture.detectChanges();
    });
    it('should render a button to join', function () {
        setGroup({
            'is:banned': false,
            'is:awaiting': false,
            'is:invited': false,
            'is:member': false
        });
        fixture.detectChanges();
        expect(getJoinButtons().length).toBe(1);
    });
    it('should not render a button to join if banned', function () {
        setGroup({
            'is:banned': true,
            'is:awaiting': false,
            'is:invited': false,
            'is:member': false
        });
        fixture.detectChanges();
        expect(getJoinButtons().length).toBe(0);
    });
    it('should render a button to accept or decline an invitation', function () {
        setGroup({
            'is:banned': false,
            'is:awaiting': false,
            'is:invited': true,
            'is:member': false
        });
        fixture.detectChanges();
        expect(getAcceptAndDeclineButtons().length).toBe(2);
    });
    it('should render a button to leave', function () {
        setGroup({
            'is:banned': false,
            'is:awaiting': false,
            'is:invited': false,
            'is:member': true
        });
        fixture.detectChanges();
        expect(getLeaveButton()).toBeTruthy();
    });
    it('should render a button to cancel join request', function () {
        setGroup({
            'is:banned': false,
            'is:awaiting': true,
            'is:invited': false,
            'is:member': false
        });
        fixture.detectChanges();
        expect(getCancelRequestButton()).toBeTruthy();
    });
});
//# sourceMappingURL=groups-join-button.spec.js.map