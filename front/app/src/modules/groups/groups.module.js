"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var common_module_1 = require("../../common/common.module");
var legacy_module_1 = require("../legacy/legacy.module");
var channel_module_1 = require("../channel/channel.module");
var modals_module_1 = require("../modals/modals.module");
var groups_1 = require("./groups");
var groups_join_button_1 = require("./groups-join-button");
var invite_1 = require("./profile/members/invite/invite");
var card_1 = require("./card/card");
var card_user_actions_button_1 = require("./profile/card-user-actions-button");
var groups_settings_button_1 = require("./profile/groups-settings-button");
var members_1 = require("./profile/members/members");
var requests_1 = require("./profile/requests/requests");
var feed_1 = require("./profile/feed/feed");
var conversation_component_1 = require("./profile/conversation/conversation.component");
var filter_selector_component_1 = require("./profile/filter-selector/filter-selector.component");
var members_2 = require("./members/members");
var routes = [
    { path: 'groups/profile/:guid/:filter', component: groups_1.GroupsProfile },
    { path: 'groups/profile/:guid', component: groups_1.GroupsProfile },
    { path: 'groups/create', component: groups_1.GroupsCreator },
    { path: 'groups/:filter', component: groups_1.Groups },
];
var GroupsModule = (function () {
    function GroupsModule() {
    }
    return GroupsModule;
}());
GroupsModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            router_1.RouterModule.forChild(routes),
            forms_1.FormsModule,
            forms_1.ReactiveFormsModule,
            common_module_1.CommonModule,
            legacy_module_1.LegacyModule,
            channel_module_1.ChannelModule,
            modals_module_1.ModalsModule
        ],
        declarations: [
            groups_1.Groups,
            groups_1.GroupsProfile,
            groups_1.GroupsCreator,
            groups_join_button_1.GroupsJoinButton,
            invite_1.GroupsProfileMembersInvite,
            card_1.GroupsCard,
            card_user_actions_button_1.GroupsCardUserActionsButton,
            members_1.GroupsProfileMembers,
            feed_1.GroupsProfileFeed,
            requests_1.GroupsProfileRequests,
            groups_settings_button_1.GroupsSettingsButton,
            conversation_component_1.GroupsProfileConversation,
            filter_selector_component_1.GroupsProfileFilterSelector,
            members_2.GroupsMembersModuleComponent
        ],
        exports: [
            groups_1.Groups,
            groups_1.GroupsProfile,
            groups_1.GroupsCreator,
            groups_join_button_1.GroupsJoinButton,
            invite_1.GroupsProfileMembersInvite,
            card_1.GroupsCard,
            card_user_actions_button_1.GroupsCardUserActionsButton,
            members_1.GroupsProfileMembers,
            feed_1.GroupsProfileFeed,
            requests_1.GroupsProfileRequests,
            groups_settings_button_1.GroupsSettingsButton,
            conversation_component_1.GroupsProfileConversation,
            filter_selector_component_1.GroupsProfileFilterSelector,
            members_2.GroupsMembersModuleComponent
        ],
        entryComponents: [
            card_1.GroupsCard
        ]
    })
], GroupsModule);
exports.GroupsModule = GroupsModule;
//# sourceMappingURL=groups.module.js.map