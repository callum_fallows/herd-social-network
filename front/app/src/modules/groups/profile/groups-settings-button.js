"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var groups_service_1 = require("../groups-service");
var GroupsSettingsButton = (function () {
    function GroupsSettingsButton(service, router) {
        this.service = service;
        this.router = router;
        this.group = {
            'is:muted': false,
            deleted: false
        };
        this.showMenu = false;
        this.isGoingToBeDeleted = false;
    }
    GroupsSettingsButton.prototype.mute = function () {
        var _this = this;
        this.group['is:muted'] = true;
        this.service.muteNotifications(this.group)
            .then(function (isMuted) {
            _this.group['is:muted'] = isMuted;
        });
        this.showMenu = false;
    };
    GroupsSettingsButton.prototype.unmute = function () {
        var _this = this;
        this.group['is:muted'] = true;
        this.service.unmuteNotifications(this.group)
            .then(function (isMuted) {
            _this.group['is:muted'] = isMuted;
        });
        this.showMenu = false;
    };
    GroupsSettingsButton.prototype.deletePrompt = function () {
        this.isGoingToBeDeleted = true;
    };
    GroupsSettingsButton.prototype.cancelDelete = function () {
        this.isGoingToBeDeleted = false;
    };
    GroupsSettingsButton.prototype.delete = function () {
        var _this = this;
        if (!this.isGoingToBeDeleted) {
            return;
        }
        this.group.deleted = true;
        this.service.deleteGroup(this.group)
            .then(function (deleted) {
            _this.group.deleted = deleted;
            if (deleted) {
                _this.router.navigate(['/groups/member']);
            }
        });
        this.showMenu = false;
        this.isGoingToBeDeleted = false;
    };
    GroupsSettingsButton.prototype.toggleMenu = function (e) {
        e.stopPropagation();
        if (this.showMenu) {
            this.showMenu = false;
            return;
        }
        this.showMenu = true;
    };
    return GroupsSettingsButton;
}());
GroupsSettingsButton = __decorate([
    core_1.Component({
        selector: 'minds-groups-settings-button',
        inputs: ['group'],
        template: "\n    <button class=\"material-icons\" (click)=\"toggleMenu($event)\">\n      settings\n      <i *ngIf=\"group['is:muted']\" class=\"minds-groups-button-badge material-icons\">notifications_off</i>\n    </button>\n\n    <ul class=\"minds-dropdown-menu\" [hidden]=\"!showMenu\" >\n      <li class=\"mdl-menu__item\" [hidden]=\"group['is:muted']\" (click)=\"mute()\" i18n=\"@@GROUPS__PROFILE__GROUP_SETTINGS_BTN__DISABLE_NOTIFICATIONS\">Disable Notifications</li>\n      <li class=\"mdl-menu__item\" [hidden]=\"!group['is:muted']\" (click)=\"unmute()\" i18n=\"@@GROUPS__PROFILE__GROUP_SETTINGS_BTN__ENABLE_NOTIFICATIONS\">Enable Notifications</li>\n      <li class=\"mdl-menu__item\" *ngIf=\"group['is:creator']\" [hidden]=\"group.deleted\" (click)=\"deletePrompt()\" i18n=\"@@GROUPS__PROFILE__GROUP_SETTINGS_BTN__DELETE_GROUP\">Delete Group</li>\n    </ul>\n    <div class=\"minds-bg-overlay\" (click)=\"toggleMenu($event)\" [hidden]=\"!showMenu\"></div>\n\n    <m-modal [open]=\"group['is:owner'] && isGoingToBeDeleted\">\n      <div class=\"mdl-card__supporting-text\">\n        <p i18n=\"@@GROUPS__PROFILE__GROUP_SETTINGS_BTN__DELETE_GROUP_CONFIRM\">Are you sure you want to delete {{ group.name }}? This action cannot be undone.</p>\n      </div>\n      <div class=\"mdl-card__actions\">\n        <button (click)=\"delete()\" class=\"mdl-button mdl-js-button mdl-button--raised mdl-button--colored\">\n          <!-- i18n: @@M__ACTION__CONFIRM -->Confirm<!-- /i18n -->\n        </button>\n        <button (click)=\"cancelDelete()\" class=\"mdl-button mdl-js-button mdl-button--colored\">\n          <!-- i18n: @@M__ACTION__CANCEL -->Cancel<!-- /i18n -->\n        </button>\n      </div>\n    </m-modal>\n  "
    }),
    __metadata("design:paramtypes", [groups_service_1.GroupsService, router_1.Router])
], GroupsSettingsButton);
exports.GroupsSettingsButton = GroupsSettingsButton;
//# sourceMappingURL=groups-settings-button.js.map