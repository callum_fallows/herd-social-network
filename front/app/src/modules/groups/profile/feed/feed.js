"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var groups_service_1 = require("../../groups-service");
var api_1 = require("../../../../services/api");
var session_1 = require("../../../../services/session");
var poster_1 = require("../../../legacy/controllers/newsfeed/poster/poster");
var GroupsProfileFeed = (function () {
    function GroupsProfileFeed(client, service) {
        this.client = client;
        this.service = service;
        this.filter = 'activity';
        this.session = session_1.SessionFactory.build();
        this.activity = [];
        this.offset = '';
        this.inProgress = false;
        this.moreData = true;
        this.pollingOffset = '';
        this.pollingNewPosts = 0;
        this.kickPrompt = false;
        this.kickBan = false;
        this.kickSuccess = false;
    }
    Object.defineProperty(GroupsProfileFeed.prototype, "_group", {
        set: function (value) {
            this.group = value;
            this.guid = value.guid;
            this.load(true);
            this.setUpPoll();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GroupsProfileFeed.prototype, "_filter", {
        set: function (value) {
            var oldFilter = this.filter;
            this.filter = value;
            if (this.group && oldFilter != this.filter) {
                this.load(true);
            }
        },
        enumerable: true,
        configurable: true
    });
    GroupsProfileFeed.prototype.setUpPoll = function () {
        var _this = this;
        this.pollingTimer = setInterval(function () {
            _this.client.get('api/v1/newsfeed/container/' + _this.guid, { offset: _this.pollingOffset, count: true }, { cache: true })
                .then(function (response) {
                if (typeof response.count === 'undefined') {
                    return;
                }
                _this.pollingNewPosts += response.count;
                _this.pollingOffset = response['load-previous'];
            })
                .catch(function (e) { console.error('Newsfeed polling', e); });
        }, 60000);
    };
    GroupsProfileFeed.prototype.pollingLoadNew = function () {
        this.load(true);
    };
    GroupsProfileFeed.prototype.ngOnDestroy = function () {
        clearInterval(this.pollingTimer);
    };
    GroupsProfileFeed.prototype.prepend = function (activity) {
        this.activity.unshift(activity);
        this.pollingOffset = activity.guid;
    };
    GroupsProfileFeed.prototype.load = function (refresh) {
        var _this = this;
        if (refresh === void 0) { refresh = false; }
        if (this.inProgress && !refresh) {
            return false;
        }
        this.inProgress = true;
        if (refresh) {
            this.offset = '';
            this.pollingOffset = '';
            this.pollingNewPosts = 0;
            this.activity = [];
        }
        var endpoint = "api/v1/newsfeed/container/" + this.guid;
        if (this.filter == 'review') {
            endpoint = "api/v1/groups/review/" + this.guid;
        }
        var currentFilter = this.filter;
        this.client.get(endpoint, { limit: 12, offset: this.offset })
            .then(function (response) {
            if (_this.filter !== currentFilter) {
                return;
            }
            _this.inProgress = false;
            if (refresh) {
                _this.activity = [];
            }
            if (typeof response['adminqueue:count'] !== 'undefined') {
                _this.group['adminqueue:count'] = response['adminqueue:count'];
            }
            if (!response.activity) {
                _this.moreData = false;
                return false;
            }
            (_a = _this.activity).push.apply(_a, (response.activity || []));
            if (typeof _this.activity[0] !== 'undefined') {
                _this.pollingOffset = response.activity[0].guid;
            }
            if (response['load-next']) {
                _this.offset = response['load-next'];
            }
            else {
                _this.moreData = false;
            }
            var _a;
        });
    };
    GroupsProfileFeed.prototype.delete = function (activity) {
        var i;
        for (i in this.activity) {
            if (this.activity[i] === activity)
                this.activity.splice(i, 1);
        }
    };
    GroupsProfileFeed.prototype.canDeactivate = function () {
        if (!this.poster || !this.poster.attachment)
            return true;
        var progress = this.poster.attachment.getUploadProgress();
        if (progress > 0 && progress < 100) {
            return confirm('Your file is still uploading. Are you sure?');
        }
        return true;
    };
    GroupsProfileFeed.prototype.approve = function (activity, index) {
        return __awaiter(this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!activity) {
                            return [2 /*return*/];
                        }
                        this.activity.splice(index, 1);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.client.put("api/v1/groups/review/" + this.group.guid + "/" + activity.guid)];
                    case 2:
                        _a.sent();
                        this.group['adminqueue:count'] = this.group['adminqueue:count'] - 1;
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        alert((e_1 && e_1.message) || 'Internal server error');
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    GroupsProfileFeed.prototype.reject = function (activity, index) {
        return __awaiter(this, void 0, void 0, function () {
            var e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!activity) {
                            return [2 /*return*/];
                        }
                        this.activity.splice(index, 1);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.client.delete("api/v1/groups/review/" + this.group.guid + "/" + activity.guid)];
                    case 2:
                        _a.sent();
                        this.group['adminqueue:count'] = this.group['adminqueue:count'] - 1;
                        return [3 /*break*/, 4];
                    case 3:
                        e_2 = _a.sent();
                        alert((e_2 && e_2.message) || 'Internal server error');
                        return [3 /*break*/, 4];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    GroupsProfileFeed.prototype.removePrompt = function (user) {
        if (!user) {
            console.error(user);
            return;
        }
        this.kickSuccess = false;
        this.kickPrompt = true;
        this.kickBan = false;
        this.kickUser = user;
    };
    GroupsProfileFeed.prototype.cancelRemove = function () {
        this.kickSuccess = false;
        this.kickPrompt = false;
        this.kickBan = false;
        this.kickUser = void 0;
    };
    GroupsProfileFeed.prototype.kick = function (ban) {
        var _this = this;
        if (ban === void 0) { ban = false; }
        if (!this.kickUser) {
            return;
        }
        var action;
        this.kickPrompt = false;
        if (ban) {
            action = this.service.ban(this.group, this.kickUser.guid);
        }
        else {
            action = this.service.kick(this.group, this.kickUser.guid);
        }
        this.kickUser = void 0;
        action.then(function () {
            _this.kickPrompt = false;
            _this.kickBan = false;
            _this.kickSuccess = true;
        });
    };
    return GroupsProfileFeed;
}());
__decorate([
    core_1.ViewChild('poster'),
    __metadata("design:type", poster_1.Poster)
], GroupsProfileFeed.prototype, "poster", void 0);
__decorate([
    core_1.Input('group'),
    __metadata("design:type", Object),
    __metadata("design:paramtypes", [Object])
], GroupsProfileFeed.prototype, "_group", null);
__decorate([
    core_1.Input('filter'),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [String])
], GroupsProfileFeed.prototype, "_filter", null);
GroupsProfileFeed = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-groups-profile-feed',
        templateUrl: 'feed.html'
    }),
    __metadata("design:paramtypes", [api_1.Client, groups_service_1.GroupsService])
], GroupsProfileFeed);
exports.GroupsProfileFeed = GroupsProfileFeed;
//# sourceMappingURL=feed.js.map