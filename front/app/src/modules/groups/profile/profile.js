"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var groups_service_1 = require("../groups-service");
var recent_1 = require("../../../services/ux/recent");
var title_1 = require("../../../services/ux/title");
var session_1 = require("../../../services/session");
var sockets_1 = require("../../../services/sockets");
var feed_1 = require("./feed/feed");
var context_service_1 = require("../../../services/context.service");
var GroupsProfile = (function () {
    function GroupsProfile(service, route, title, sockets, context, recent) {
        this.service = service;
        this.route = route;
        this.title = title;
        this.sockets = sockets;
        this.context = context;
        this.recent = recent;
        this.filter = 'activity';
        this.postMeta = {
            message: '',
            container_guid: 0
        };
        this.editing = false;
        this.editDone = false;
        this.session = session_1.SessionFactory.build();
        this.minds = window.Minds;
        this.activity = [];
        this.offset = '';
        this.inProgress = false;
        this.moreData = true;
        this.newConversationMessages = false;
    }
    GroupsProfile.prototype.ngOnInit = function () {
        var _this = this;
        this.context.set('activity');
        this.listenForNewMessages();
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            if (params['guid']) {
                var changed = params['guid'] !== _this.guid;
                _this.guid = params['guid'];
                _this.postMeta.container_guid = _this.guid;
                if (changed) {
                    _this.group = void 0;
                    _this.load()
                        .then(function () {
                        _this.filterToDefaultView();
                    });
                }
            }
            if (params['filter']) {
                _this.filter = params['filter'];
                if (_this.filter == 'conversation') {
                    _this.newConversationMessages = false;
                }
            }
            _this.filterToDefaultView();
        });
        this.reviewCountInterval = setInterval(function () {
            _this.reviewCountLoad();
        }, 120 * 1000);
    };
    GroupsProfile.prototype.ngOnDestroy = function () {
        this.paramsSubscription.unsubscribe();
        this.unlistenForNewMessages();
        this.leaveCommentsSocketRoom();
        if (this.reviewCountInterval) {
            clearInterval(this.reviewCountInterval);
        }
    };
    GroupsProfile.prototype.load = function () {
        var _this = this;
        return this.service.load(this.guid)
            .then(function (group) {
            _this.group = group;
            _this.joinCommentsSocketRoom();
            _this.title.setTitle(_this.group.name);
            _this.context.set('activity', { label: _this.group.name, nameLabel: _this.group.name, id: _this.group.guid });
            if (_this.session.getLoggedInUser()) {
                _this.addRecent();
            }
        });
    };
    GroupsProfile.prototype.reviewCountLoad = function () {
        return __awaiter(this, void 0, void 0, function () {
            var count, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.guid) {
                            return [2 /*return*/];
                        }
                        count = 0;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.service.getReviewCount(this.guid)];
                    case 2:
                        count = _a.sent();
                        return [3 /*break*/, 4];
                    case 3:
                        e_1 = _a.sent();
                        return [3 /*break*/, 4];
                    case 4:
                        this.group['adminqueue:count'] = count;
                        return [2 /*return*/];
                }
            });
        });
    };
    GroupsProfile.prototype.addRecent = function () {
        var _this = this;
        if (!this.group) {
            return;
        }
        this.recent
            .store('recent', this.group, function (entry) { return entry.guid == _this.group.guid; })
            .splice('recent', 50);
    };
    GroupsProfile.prototype.filterToDefaultView = function () {
        if (!this.group || this.route.snapshot.params.filter) {
            return;
        }
        this.filter = 'activity';
        switch (this.group.default_view) {
            case 1:
                this.filter = 'conversation';
                break;
        }
    };
    GroupsProfile.prototype.save = function () {
        this.service.save({
            guid: this.group.guid,
            name: this.group.name,
            briefdescription: this.group.briefdescription,
            tags: this.group.tags,
            membership: this.group.membership,
            moderated: this.group.moderated,
            default_view: this.group.default_view
        });
        this.editing = false;
        this.editDone = true;
    };
    GroupsProfile.prototype.toggleEdit = function () {
        this.editing = !this.editing;
        if (this.editing) {
            this.editDone = false;
        }
    };
    GroupsProfile.prototype.add_banner = function (file) {
        this.service.upload({
            guid: this.group.guid,
            banner_position: file.top
        }, { banner: file.file });
        this.group.banner = true;
    };
    GroupsProfile.prototype.upload_avatar = function (file) {
        this.service.upload({
            guid: this.group.guid
        }, { avatar: file });
    };
    GroupsProfile.prototype.change_membership = function (membership) {
        this.load();
    };
    GroupsProfile.prototype.canDeactivate = function () {
        if (!this.feed)
            return true;
        return this.feed.canDeactivate();
    };
    GroupsProfile.prototype.joinCommentsSocketRoom = function (keepAlive) {
        if (keepAlive === void 0) { keepAlive = false; }
        if (!keepAlive && this.socketRoomName) {
            this.leaveCommentsSocketRoom();
        }
        if (!this.group || !this.group.guid || !this.group['is:member']) {
            return;
        }
        this.socketRoomName = "comments:" + this.group.guid;
        this.sockets.join(this.socketRoomName);
    };
    GroupsProfile.prototype.leaveCommentsSocketRoom = function () {
        if (!this.socketRoomName) {
            return;
        }
        this.sockets.leave(this.socketRoomName);
    };
    GroupsProfile.prototype.listenForNewMessages = function () {
        var _this = this;
        this.socketSubscription = this.sockets.subscribe('comment', function (parent_guid, owner_guid, guid) {
            if (!_this.group || parent_guid !== _this.group.guid) {
                return;
            }
            _this.group['comments:count']++;
            if (_this.filter != 'conversation') {
                _this.newConversationMessages = true;
            }
        });
    };
    GroupsProfile.prototype.unlistenForNewMessages = function () {
        if (this.socketSubscription) {
            this.socketSubscription.unsubscribe();
        }
    };
    return GroupsProfile;
}());
__decorate([
    core_1.ViewChild('feed'),
    __metadata("design:type", feed_1.GroupsProfileFeed)
], GroupsProfile.prototype, "feed", void 0);
GroupsProfile = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-groups-profile',
        templateUrl: 'profile.html'
    }),
    __metadata("design:paramtypes", [groups_service_1.GroupsService,
        router_1.ActivatedRoute,
        title_1.MindsTitle,
        sockets_1.SocketsService,
        context_service_1.ContextService,
        recent_1.RecentService])
], GroupsProfile);
exports.GroupsProfile = GroupsProfile;
//# sourceMappingURL=profile.js.map