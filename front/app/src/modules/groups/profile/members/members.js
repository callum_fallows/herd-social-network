"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var groups_service_1 = require("../../groups-service");
var api_1 = require("../../../../services/api");
var session_1 = require("../../../../services/session");
var GroupsProfileMembers = (function () {
    function GroupsProfileMembers(client, service) {
        this.client = client;
        this.service = service;
        this.minds = window.Minds;
        this.session = session_1.SessionFactory.build();
        this.invitees = [];
        this.members = [];
        this.offset = '';
        this.inProgress = false;
        this.moreData = true;
        this.canInvite = false;
        this.q = '';
    }
    Object.defineProperty(GroupsProfileMembers.prototype, "_group", {
        set: function (value) {
            this.group = value;
            this.load();
        },
        enumerable: true,
        configurable: true
    });
    GroupsProfileMembers.prototype.ngOnDestroy = function () {
        if (this.searchDelayTimer) {
            clearTimeout(this.searchDelayTimer);
        }
    };
    GroupsProfileMembers.prototype.load = function (refresh, query) {
        var _this = this;
        if (refresh === void 0) { refresh = false; }
        if (query === void 0) { query = null; }
        if (this.inProgress)
            return;
        if (query !== null && query !== this.lastQuery) {
            refresh = true;
            this.lastQuery = query;
        }
        if (refresh) {
            this.offset = '';
            this.moreData = true;
        }
        this.canInvite = false;
        if (this.group.membership === 0 && this.group['is:owner']) {
            this.canInvite = true;
        }
        else if (this.group.membership === 2 && this.group['is:member']) {
            this.canInvite = true;
        }
        var endpoint = "api/v1/groups/membership/" + this.group.guid, params = { limit: 12, offset: this.offset };
        if (this.lastQuery) {
            endpoint = endpoint + "/search";
            params.q = this.lastQuery;
        }
        this.inProgress = true;
        this.client.get(endpoint, params)
            .then(function (response) {
            if (!response.members) {
                _this.moreData = false;
                _this.inProgress = false;
                return false;
            }
            if (refresh) {
                _this.members = response.members;
            }
            else {
                _this.members = _this.members.concat(response.members);
            }
            if (response['load-next']) {
                _this.offset = response['load-next'];
            }
            else {
                _this.moreData = false;
            }
            _this.inProgress = false;
        })
            .catch(function (e) {
            _this.inProgress = false;
        });
    };
    GroupsProfileMembers.prototype.invite = function (user) {
        for (var _i = 0, _a = this.invitees; _i < _a.length; _i++) {
            var i = _a[_i];
            if (i.guid === user.guid)
                return;
        }
        this.invitees.push(user);
    };
    GroupsProfileMembers.prototype.search = function (q) {
        var _this = this;
        if (this.searchDelayTimer) {
            clearTimeout(this.searchDelayTimer);
        }
        this.searchDelayTimer = setTimeout(function () {
            _this.load(true, q);
        }, 500);
    };
    return GroupsProfileMembers;
}());
GroupsProfileMembers = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-groups-profile-members',
        inputs: ['_group : group'],
        templateUrl: 'members.html'
    }),
    __metadata("design:paramtypes", [api_1.Client, groups_service_1.GroupsService])
], GroupsProfileMembers);
exports.GroupsProfileMembers = GroupsProfileMembers;
//# sourceMappingURL=members.js.map