"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ThumbnailSelector = (function () {
    function ThumbnailSelector(_element) {
        this._element = _element;
        this.src = [];
        this.thumbnailSec = 0;
        this.thumbnail = new core_1.EventEmitter();
        this.inProgress = false;
    }
    ThumbnailSelector.prototype.ngOnInit = function () {
        var _this = this;
        this.element = this._element.nativeElement.getElementsByTagName('video')[0];
        if (this.src)
            this.element.src = this.src;
        this.element.addEventListener('loadedmetadata', function () {
            if (_this.thumbnailSec)
                _this.element.currentTime = _this.thumbnailSec;
            _this.inProgress = false;
        });
    };
    Object.defineProperty(ThumbnailSelector.prototype, "_src", {
        set: function (value) {
            this.src = value[0].uri;
            if (this.element)
                this.element.src = this.src;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ThumbnailSelector.prototype, "_thumbnailSec", {
        set: function (value) {
            var _this = this;
            if (!this.canvas)
                this.inProgress = true;
            this.thumbnailSec = value;
            if (this.element) {
                this.element.addEventListener('loadedmetadata', function () {
                    _this.element.currentTime = value;
                    _this.inProgress = false;
                });
            }
        },
        enumerable: true,
        configurable: true
    });
    ThumbnailSelector.prototype.seek = function (e) {
        e.preventDefault();
        var seeker = e.target;
        var seek = e.offsetX / seeker.offsetWidth;
        var seconds = this.seekerToSeconds(seek);
        this.element.currentTime = seconds;
        this.thumbnailSec = seconds;
        this.createThumbnail();
        return false;
    };
    ThumbnailSelector.prototype.seekerToSeconds = function (seek) {
        var duration = this.element.duration;
        console.log('seeking to ', duration * seek);
        return duration * seek;
    };
    ThumbnailSelector.prototype.createThumbnail = function () {
        var _this = this;
        if (!this.canvas) {
            this.canvas = document.createElement('canvas');
            this.canvas.width = 1280;
            this.canvas.height = 720;
        }
        this.inProgress = true;
        this.element.addEventListener('seeked', function () {
            _this.canvas.getContext('2d').drawImage(_this.element, 0, 0, _this.canvas.width, _this.canvas.height);
            _this.thumbnail.next([_this.canvas.toDataURL('image/jpeg'), _this.thumbnailSec]);
            _this.inProgress = false;
        });
    };
    return ThumbnailSelector;
}());
ThumbnailSelector = __decorate([
    core_1.Component({
        selector: 'minds-media-thumbnail-selector',
        inputs: ['_src: src', '_thumbnailSec: thumbnailSec'],
        outputs: ['thumbnail'],
        template: "\n  <div class=\"m-video-loading\" [hidden]=\"!inProgress\">\n    <div class=\"mdl-spinner mdl-js-spinner is-active\" [mdl]></div>\n  </div>\n  <video preload=\"metadata\" muted crossOrigin=\"anonymous\">\n  </video>\n  <div class=\"m-scrubber mdl-color--indigo-600\" (click)=\"seek($event)\">\n      <div class=\"m-scrubber-progress mdl-color--amber-600\" [ngStyle]=\"{'left': (thumbnailSec / element.duration)*100  + '%'}\"></div>\n  </div>\n  <span class=\"m-scrubber-tip\" i18n=\"@@MINDS__MEDIA__CHANGE_THUMBNAIL_INFO\">Click on this bar to change the thumbnail</span>\n  "
    }),
    __metadata("design:paramtypes", [core_1.ElementRef])
], ThumbnailSelector);
exports.ThumbnailSelector = ThumbnailSelector;
//# sourceMappingURL=thumbnail-selector.js.map