"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var api_1 = require("../../../../services/api");
var session_1 = require("../../../../services/session");
var MediaTheatre = (function () {
    function MediaTheatre(client, router) {
        this.client = client;
        this.router = router;
        this.object = {};
        this.session = session_1.SessionFactory.build();
    }
    Object.defineProperty(MediaTheatre.prototype, "_object", {
        set: function (value) {
            if (!value.guid)
                return;
            this.object = value;
        },
        enumerable: true,
        configurable: true
    });
    MediaTheatre.prototype.prev = function () {
        var pos = this.object['album_children_guids'].indexOf(this.object.guid) - 1;
        if (pos <= 0)
            pos = this.object['album_children_guids'].length - 1;
        this.router.navigate(['/media', this.object['album_children_guids'][pos]]);
    };
    MediaTheatre.prototype.next = function () {
        var pos = this.object['album_children_guids'].indexOf(this.object.guid);
        if (pos <= 0)
            pos = 1;
        if (this.object['album_children_guids'][pos] === this.object.guid)
            pos++;
        if (pos >= this.object['album_children_guids'].length)
            pos = 0;
        this.router.navigate(['/media', this.object['album_children_guids'][pos]]);
    };
    MediaTheatre.prototype.isAlbum = function () {
        return this.object.container_guid !== this.object.owner_guid
            && this.object.album_children_guids
            && this.object.album_children_guids.length > 1;
    };
    return MediaTheatre;
}());
MediaTheatre = __decorate([
    core_1.Component({
        selector: 'm-media-theatre',
        inputs: ['_object: object'],
        template: "\n    <i class=\"material-icons left\"\n      (click)=\"prev()\"\n      [hidden]=\"!isAlbum()\">\n        keyboard_arrow_left\n    </i>\n    <div class=\"m-media-stage\" *ngIf=\"object.subtype == 'image'\"\n      [class.m-media-stage--has-nav]=\"isAlbum()\"\n    >\n      <img src=\"/fs/v1/thumbnail/{{object.guid}}/xlarge\"/>\n    </div>\n    <div class=\"m-media-stage\" *ngIf=\"object.subtype == 'video'\"\n      [class.m-media-stage--has-nav]=\"isAlbum()\"\n    >\n      <herds-video\n      [poster]=\"object.thumbnail_src\"\n\t    [autoplay]=\"!object.monetized\"\n\t    [muted]=\"false\"\n\t    [src]=\"[{ 'uri': object.src['720.mp4'] }, { 'uri': object.src['360.mp4'] }]\"\n        [log]=\"object.guid\"\n        [playCount]=\"false\"\n        #player>\n        <video-ads [player]=\"player\" *ngIf=\"object.monetized\"></video-ads>\n      </herds-video>\n\n    </div>\n    <i class=\"material-icons right\"\n      (click)=\"next()\"\n      [hidden]=\"!isAlbum()\">\n        keyboard_arrow_right\n    </i>\n    <ng-content></ng-content>\n  "
    }),
    __metadata("design:paramtypes", [api_1.Client, router_1.Router])
], MediaTheatre);
exports.MediaTheatre = MediaTheatre;
//# sourceMappingURL=theatre.js.map