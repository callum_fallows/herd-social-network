"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var api_1 = require("../../../services/api");
var session_1 = require("../../../services/session");
var list_options_1 = require("../../../services/list-options");
var MediaEdit = (function () {
    function MediaEdit(client, upload, router, route) {
        this.client = client;
        this.upload = upload;
        this.router = router;
        this.route = route;
        this.session = session_1.SessionFactory.build();
        this.entity = {
            title: '',
            description: '',
            subtype: '',
            license: 'all-rights-reserved',
            mature: false
        };
        this.licenses = list_options_1.LICENSES;
        this.access = list_options_1.ACCESS;
    }
    MediaEdit.prototype.ngOnInit = function () {
        var _this = this;
        this.minds = window.Minds;
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            if (params['guid']) {
                _this.guid = params['guid'];
                _this.load();
            }
        });
    };
    MediaEdit.prototype.ngOnDestroy = function () {
        this.paramsSubscription.unsubscribe();
    };
    MediaEdit.prototype.load = function () {
        var _this = this;
        this.inProgress = true;
        this.client.get('api/v1/entities/entity/' + this.guid, { children: false })
            .then(function (response) {
            _this.inProgress = false;
            console.log(response);
            if (response.entity) {
                if (!response.entity.description)
                    response.entity.description = '';
                if (!response.entity.license)
                    response.entity.license = 'all-rights-reserved';
                response.entity.mature = response.entity.flags && response.entity.flags.mature ? 1 : 0;
                _this.entity = response.entity;
            }
        });
    };
    MediaEdit.prototype.save = function () {
        var _this = this;
        this.client.post('api/v1/media/' + this.guid, this.entity)
            .then(function (response) {
            console.log(response);
            _this.router.navigate(['/media', _this.guid]);
        })
            .catch(function (e) {
            _this.error = 'There was an error while trying to update';
        });
    };
    MediaEdit.prototype.setThumbnail = function (file) {
        console.log(file);
        this.entity.file = file[0];
        this.entity.thumbnail = file[1];
    };
    return MediaEdit;
}());
MediaEdit = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-media-edit',
        templateUrl: 'edit.html'
    }),
    __metadata("design:paramtypes", [api_1.Client,
        api_1.Upload,
        router_1.Router,
        router_1.ActivatedRoute])
], MediaEdit);
exports.MediaEdit = MediaEdit;
//# sourceMappingURL=edit.js.map