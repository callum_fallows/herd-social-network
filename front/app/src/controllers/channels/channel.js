"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var api_1 = require("../../services/api");
var title_1 = require("../../services/ux/title");
var session_1 = require("../../services/session");
var scroll_1 = require("../../services/ux/scroll");
var recent_1 = require("../../services/ux/recent");
var poster_1 = require("../../modules/legacy/controllers/newsfeed/poster/poster");
var channel_component_1 = require("../../modules/wire/channel/channel.component");
var context_service_1 = require("../../services/context.service");
var Channel = (function () {
    function Channel(client, upload, route, title, scroll, recent, context) {
        this.client = client;
        this.upload = upload;
        this.route = route;
        this.title = title;
        this.scroll = scroll;
        this.recent = recent;
        this.context = context;
        this._filter = 'feed';
        this.session = session_1.SessionFactory.build();
        this.isLocked = false;
        this.feed = [];
        this.pinned = [];
        this.offset = '';
        this.moreData = true;
        this.inProgress = false;
        this.editing = false;
        this.error = '';
        this.openWireModal = false;
        this.cities = [];
        this.showOnboarding = false;
    }
    Channel.prototype.ngOnInit = function () {
        var _this = this;
        this.title.setTitle('Channel');
        this.context.set('activity');
        this.onScroll();
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            var changed = false;
            _this.editing = false;
            if (params['username']) {
                changed = _this.username !== params['username'];
                _this.username = params['username'];
            }
            if (params['filter']) {
                if (params['filter'] === 'wire') {
                    _this.openWireModal = true;
                }
                else {
                    _this._filter = params['filter'];
                }
            }
            if (params['editToggle']) {
                _this.editing = true;
            }
            if (changed) {
                _this.load();
            }
        });
    };
    Channel.prototype.ngOnDestroy = function () {
        this.paramsSubscription.unsubscribe();
    };
    Channel.prototype.load = function () {
        var _this = this;
        this.error = '';
        this.user = null;
        this.title.setTitle(this.username);
        this.client.get('api/v1/channel/' + this.username, {})
            .then(function (data) {
            if (data.status !== 'success') {
                _this.error = data.message;
                return false;
            }
            _this.user = data.channel;
            if (!(_this.session.getLoggedInUser() && _this.session.getLoggedInUser().guid === _this.user.guid)) {
                _this.editing = false;
            }
            _this.title.setTitle(_this.user.username);
            if (_this.openWireModal) {
                setTimeout(function () {
                    _this.wire.sendWire();
                });
            }
            if (_this._filter === 'feed')
                _this.loadFeed(true);
            _this.context.set('activity', { label: "@" + _this.user.username + " posts", nameLabel: "@" + _this.user.username, id: _this.user.guid });
            if (_this.session.getLoggedInUser()) {
                _this.addRecent();
            }
        })
            .catch(function (e) {
            if (e.status === 0) {
                _this.error = 'Sorry, there was a timeout error.';
            }
            else {
                _this.error = 'Sorry, the channel couldn\'t be found';
                console.log('couldnt load channel', e);
            }
        });
    };
    Channel.prototype.loadFeed = function (refresh) {
        var _this = this;
        if (refresh === void 0) { refresh = false; }
        if (this.inProgress) {
            return false;
        }
        if (refresh) {
            this.feed = [];
            this.offset = '';
        }
        var params = {
            limit: 12,
            offset: ''
        };
        if (!this.offset && this.user.pinned_posts.length > 0) {
            params.pinned = this.user.pinned_posts;
        }
        this.inProgress = true;
        params.offset = this.offset;
        this.client.get('api/v1/newsfeed/personal/' + this.user.guid, params, { cache: true })
            .then(function (data) {
            if (!data.activity) {
                _this.moreData = false;
                _this.inProgress = false;
                return false;
            }
            if (_this.feed && !refresh) {
                for (var _i = 0, _a = data.activity; _i < _a.length; _i++) {
                    var activity = _a[_i];
                    _this.feed.push(activity);
                }
            }
            else {
                _this.feed = _this.filterPinned(data.activity);
                _this.pinned = data.pinned;
            }
            _this.offset = data['load-next'];
            _this.inProgress = false;
        })
            .catch(function (e) {
            this.inProgress = false;
        });
    };
    Channel.prototype.isOwner = function () {
        return this.session.getLoggedInUser().guid === this.user.guid;
    };
    Channel.prototype.filterPinned = function (activities) {
        var _this = this;
        return activities.filter(function (activity) {
            if (_this.user.pinned_posts.indexOf(activity.guid) >= 0) {
                activity.pinned = true;
            }
            else {
                return activity;
            }
        }).filter(function (x) { return !!x; });
    };
    Channel.prototype.toggleEditing = function () {
        if (this.editing) {
            this.update();
        }
        this.editing = !this.editing;
    };
    Channel.prototype.onScroll = function () {
        var _this = this;
        var listen = this.scroll.listen(function (view) {
            if (view.top > 250)
                _this.isLocked = true;
            if (view.top < 250)
                _this.isLocked = false;
        });
    };
    Channel.prototype.updateCarousels = function (value) {
        var _this = this;
        if (!value.length)
            return;
        for (var _i = 0, value_1 = value; _i < value_1.length; _i++) {
            var banner = value_1[_i];
            var options = { top: banner.top };
            if (banner.guid)
                options.guid = banner.guid;
            this.upload.post('api/v1/channel/carousel', [banner.file], options)
                .then(function (response) {
                response.index = banner.index;
                if (!_this.user.carousels) {
                    _this.user.carousels = [];
                }
                _this.user.carousels[banner.index] = response.carousel;
            });
        }
    };
    Channel.prototype.removeCarousel = function (value) {
        if (value.guid)
            this.client.delete('api/v1/channel/carousel/' + value.guid);
    };
    Channel.prototype.update = function () {
        var _this = this;
        this.client.post('api/v1/channel/info', this.user)
            .then(function (data) {
            _this.editing = false;
        });
    };
    Channel.prototype.delete = function (activity) {
        var i;
        for (i in this.feed) {
            if (this.feed[i] === activity)
                this.feed.splice(i, 1);
        }
    };
    Channel.prototype.prepend = function (activity) {
        activity.boostToggle = true;
        this.feed.unshift(activity);
    };
    Channel.prototype.upload_avatar = function (file) {
        var self = this;
        this.upload.post('api/v1/channel/avatar', [file], { filekey: 'file' })
            .then(function (response) {
            self.user.icontime = Date.now();
            window.Minds.user.icontime = Date.now();
        });
    };
    Channel.prototype.findCity = function (q) {
        var _this = this;
        if (this.searching) {
            clearTimeout(this.searching);
        }
        this.searching = setTimeout(function () {
            _this.client.get('api/v1/geolocation/list', { q: q })
                .then(function (response) {
                _this.cities = response.results;
            });
        }, 100);
    };
    Channel.prototype.setCity = function (row) {
        this.cities = [];
        if (row.address.city)
            window.Minds.user.city = row.address.city;
        if (row.address.town)
            window.Minds.user.city = row.address.town;
        this.user.city = window.Minds.user.city;
        this.client.post('api/v1/channel/info', {
            coordinates: row.lat + ',' + row.lon,
            city: window.Minds.user.city
        });
    };
    Channel.prototype.setSocialProfile = function (value) {
        this.user.social_profiles = value;
    };
    Channel.prototype.unBlock = function () {
        var _this = this;
        this.user.blocked = false;
        this.client.delete('api/v1/block/' + this.user.guid, {})
            .then(function (response) {
            _this.user.blocked = false;
        })
            .catch(function (e) {
            _this.user.blocked = true;
        });
    };
    Channel.prototype.canDeactivate = function () {
        if (!this.poster || !this.poster.attachment)
            return true;
        var progress = this.poster.attachment.getUploadProgress();
        if (progress > 0 && progress < 100) {
            return confirm('Your file is still uploading. Are you sure?');
        }
        return true;
    };
    Channel.prototype.addRecent = function () {
        var _this = this;
        if (!this.user) {
            return;
        }
        this.recent
            .store('recent', this.user, function (entry) { return entry.guid == _this.user.guid; })
            .splice('recent', 50);
    };
    return Channel;
}());
__decorate([
    core_1.ViewChild('poster'),
    __metadata("design:type", poster_1.Poster)
], Channel.prototype, "poster", void 0);
__decorate([
    core_1.ViewChild('wire'),
    __metadata("design:type", channel_component_1.WireChannelComponent)
], Channel.prototype, "wire", void 0);
Channel = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-channel',
        templateUrl: 'channel.html'
    }),
    __metadata("design:paramtypes", [api_1.Client,
        api_1.Upload,
        router_1.ActivatedRoute,
        title_1.MindsTitle,
        scroll_1.ScrollService,
        recent_1.RecentService,
        context_service_1.ContextService])
], Channel);
exports.Channel = Channel;
var subscribers_1 = require("./subscribers/subscribers");
exports.ChannelSubscribers = subscribers_1.ChannelSubscribers;
var subscriptions_1 = require("./subscriptions/subscriptions");
exports.ChannelSubscriptions = subscriptions_1.ChannelSubscriptions;
//# sourceMappingURL=channel.js.map