"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var api_1 = require("../../services/api");
var title_1 = require("../../services/ux/title");
var navigation_1 = require("../../services/navigation");
var session_1 = require("../../services/session");
var storage_1 = require("../../services/storage");
var poster_1 = require("../../modules/legacy/controllers/newsfeed/poster/poster");
var context_service_1 = require("../../services/context.service");
var Newsfeed = (function () {
    function Newsfeed(client, upload, navigation, router, route, title, storage, context) {
        var _this = this;
        this.client = client;
        this.upload = upload;
        this.navigation = navigation;
        this.router = router;
        this.route = route;
        this.title = title;
        this.storage = storage;
        this.context = context;
        this.prepended = [];
        this.offset = '';
        this.showBoostRotator = true;
        this.inProgress = false;
        this.moreData = true;
        this.session = session_1.SessionFactory.build();
        this.showRightSidebar = true;
        this.message = '';
        this.newUserPromo = false;
        this.postMeta = {
            title: '',
            description: '',
            thumbnail: '',
            url: '',
            active: false,
            attachment_guid: null
        };
        this.pollingOffset = '';
        this.pollingNewPosts = 0;
        this.boostFeed = false;
        this.route.url.subscribe(function (segments) {
            if (segments[segments.length - 1].path === 'boost') {
                _this.title.setTitle('Boost Newsfeed');
                _this.boostFeed = true;
            }
            else {
                _this.title.setTitle('Newsfeed');
            }
        });
    }
    Newsfeed.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.session.isLoggedIn()) {
            this.router.navigate(['/login']);
        }
        else {
            this.load();
            this.minds = window.Minds;
        }
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            if (params['message']) {
                _this.message = params['message'];
            }
            _this.newUserPromo = !!params['newUser'];
            if (params['ts']) {
                _this.showBoostRotator = false;
                _this.load(true);
                setTimeout(function () {
                    _this.showBoostRotator = true;
                }, 300);
            }
        });
        this.context.set('activity');
        this.detectWidth();
    };
    Newsfeed.prototype.setUpPoll = function () {
        var _this = this;
        this.pollingTimer = setInterval(function () {
            _this.client.get('api/v1/newsfeed', { offset: _this.pollingOffset, count: true }, { cache: true })
                .then(function (response) {
                if (typeof response.count === 'undefined') {
                    return;
                }
                _this.pollingNewPosts += response.count;
                _this.pollingOffset = response['load-previous'];
            })
                .catch(function (e) { console.error('Newsfeed polling', e); });
        }, 60000);
    };
    Newsfeed.prototype.pollingLoadNew = function () {
        var _this = this;
        if (!this.pollingOffset || !this.pollingNewPosts) {
            return;
        }
        if (this.pollingNewPosts > 120) {
            return this.load(true);
        }
        this.inProgress = true;
        this.client.get('api/v1/newsfeed', { limit: this.pollingNewPosts, offset: this.pollingOffset, prepend: true }, { cache: true })
            .then(function (data) {
            _this.inProgress = false;
            _this.pollingNewPosts = 0;
            if (!data.activity) {
                return;
            }
            _this.prepended = data.activity.concat(_this.prepended);
            _this.pollingOffset = data['load-previous'] ? data['load-previous'] : '';
        })
            .catch(function (e) {
            _this.inProgress = false;
        });
    };
    Newsfeed.prototype.ngOnDestroy = function () {
        clearInterval(this.pollingTimer);
        this.paramsSubscription.unsubscribe();
    };
    Newsfeed.prototype.load = function (refresh) {
        if (refresh === void 0) { refresh = false; }
        if (this.boostFeed) {
            this.loadBoosts(refresh);
        }
        else {
            this.loadNewsfeed(refresh);
        }
    };
    Newsfeed.prototype.loadBoosts = function (refresh) {
        var _this = this;
        if (refresh === void 0) { refresh = false; }
        if (this.inProgress) {
            return false;
        }
        if (refresh) {
            this.offset = '';
        }
        if (this.storage.get('boost:offset:boostfeed')) {
            this.offset = this.storage.get('boost:offset:boostfeed');
        }
        this.inProgress = true;
        this.client.get('api/v1/boost/fetch/newsfeed', { limit: 12, offset: this.offset }, { cache: true })
            .then(function (data) {
            if (!data.boosts) {
                _this.moreData = false;
                _this.inProgress = false;
                return false;
            }
            if (_this.newsfeed && !refresh) {
                _this.newsfeed = _this.newsfeed.concat(data.boosts);
            }
            else {
                _this.newsfeed = data.boosts;
            }
            _this.offset = data['load-next'];
            _this.storage.set('boost:offset:boostfeed', _this.offset);
            _this.inProgress = false;
        })
            .catch(function (e) {
            this.inProgress = false;
        });
    };
    Newsfeed.prototype.loadNewsfeed = function (refresh) {
        if (refresh === void 0) { refresh = false; }
        var self = this;
        if (this.inProgress) {
            return false;
        }
        if (refresh) {
            this.offset = '';
            this.pollingOffset = '';
            this.pollingNewPosts = 0;
        }
        this.inProgress = true;
        this.client.get('api/v1/newsfeed', { limit: 12, offset: this.offset }, { cache: true })
            .then(function (data) {
            if (!data.activity) {
                self.moreData = false;
                self.inProgress = false;
                return false;
            }
            if (self.newsfeed && !refresh) {
                self.newsfeed = self.newsfeed.concat(data.activity);
            }
            else {
                self.newsfeed = data.activity;
                if (data['load-previous']) {
                    self.pollingOffset = data['load-previous'];
                }
            }
            self.offset = data['load-next'];
            self.inProgress = false;
        })
            .catch(function (e) {
            self.inProgress = false;
        });
    };
    Newsfeed.prototype.prepend = function (activity) {
        if (this.newUserPromo) {
            this.autoBoost(activity);
            activity.boostToggle = false;
            activity.boosted = true;
        }
        this.prepended.unshift(activity);
        this.pollingOffset = activity.guid;
        this.newUserPromo = false;
    };
    Newsfeed.prototype.autoBoost = function (activity) {
        this.client.post('api/v1/boost/activity/' + activity.guid + '/' + activity.owner_guid, {
            newUserPromo: true,
            impressions: 200,
            destination: 'Newsfeed'
        });
    };
    Newsfeed.prototype.delete = function (activity) {
        var i;
        for (i in this.newsfeed) {
            if (this.newsfeed[i] === activity)
                this.newsfeed.splice(i, 1);
        }
    };
    Newsfeed.prototype.onViewed = function (activity) {
        if (this.boostFeed) {
            this.client.put('api/v1/boost/fetch/newsfeed/' + activity.boosted_guid);
        }
    };
    Newsfeed.prototype.detectWidth = function () {
        if (window.innerWidth < 1200)
            this.showRightSidebar = false;
        else
            this.showRightSidebar = true;
    };
    Newsfeed.prototype.canDeactivate = function () {
        if (!this.poster || !this.poster.attachment)
            return true;
        var progress = this.poster.attachment.getUploadProgress();
        if (progress > 0 && progress < 100) {
            return confirm('Your file is still uploading. Are you sure?');
        }
        return true;
    };
    return Newsfeed;
}());
__decorate([
    core_1.ViewChild('poster'),
    __metadata("design:type", poster_1.Poster)
], Newsfeed.prototype, "poster", void 0);
__decorate([
    core_1.HostListener('window:resize'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], Newsfeed.prototype, "detectWidth", null);
Newsfeed = __decorate([
    core_1.Component({
        selector: 'minds-newsfeed',
        templateUrl: 'list.html'
    }),
    __metadata("design:paramtypes", [api_1.Client,
        api_1.Upload,
        navigation_1.Navigation,
        router_1.Router,
        router_1.ActivatedRoute,
        title_1.MindsTitle,
        storage_1.Storage,
        context_service_1.ContextService])
], Newsfeed);
exports.Newsfeed = Newsfeed;
var single_1 = require("./single/single");
exports.NewsfeedSingle = single_1.NewsfeedSingle;
//# sourceMappingURL=newsfeed.js.map