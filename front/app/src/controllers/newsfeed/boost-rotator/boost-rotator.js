"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var scroll_1 = require("../../../services/ux/scroll");
var api_1 = require("../../../services/api");
var storage_1 = require("../../../services/storage");
var session_1 = require("../../../services/session");
var router_1 = require("@angular/router");
var NewsfeedBoostRotator = (function () {
    function NewsfeedBoostRotator(router, client, scroll, storage, element, cd) {
        this.router = router;
        this.client = client;
        this.scroll = scroll;
        this.storage = storage;
        this.element = element;
        this.cd = cd;
        this.session = session_1.SessionFactory.build();
        this.boosts = [];
        this.offset = '';
        this.inProgress = false;
        this.moreData = true;
        this.running = false;
        this.paused = false;
        this.interval = 5;
        this.currentPosition = 0;
        this.lastTs = Date.now();
        this.rating = 2;
        this.ratingMenuToggle = false;
        this.plus = false;
        this.disabled = false;
    }
    NewsfeedBoostRotator.prototype.ngOnInit = function () {
        var _this = this;
        this.rating = this.session.getLoggedInUser().boost_rating;
        this.plus = this.session.getLoggedInUser().plus;
        this.disabled = this.session.getLoggedInUser().plus && this.session.getLoggedInUser().disabled_boost;
        this.load();
        this.scroll_listener = this.scroll.listenForView().subscribe(function () { return _this.isVisible(); });
    };
    NewsfeedBoostRotator.prototype.load = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (_this.inProgress) {
                return reject(false);
            }
            _this.inProgress = true;
            if (_this.storage.get('boost:offset:rotator')) {
                _this.offset = _this.storage.get('boost:offset:rotator');
            }
            _this.client.get('api/v1/boost/fetch/newsfeed', { limit: 10, rating: _this.rating, offset: _this.offset })
                .then(function (response) {
                if (!response.boosts) {
                    _this.inProgress = false;
                    return reject(false);
                }
                _this.boosts = _this.boosts.concat(response.boosts);
                if (_this.boosts.length >= 40) {
                    _this.boosts.splice(0, 20);
                    _this.currentPosition = 0;
                }
                if (!_this.running) {
                    _this.recordImpression(_this.currentPosition, true);
                    _this.start();
                    _this.isVisible();
                }
                _this.offset = response['load-next'];
                _this.storage.set('boost:offset:rotator', _this.offset);
                _this.inProgress = false;
                return resolve(true);
            })
                .catch(function (e) {
                _this.inProgress = false;
                return reject();
            });
        });
    };
    NewsfeedBoostRotator.prototype.setExplicit = function (active) {
        this.session.getLoggedInUser().mature = active;
        this.load();
        this.client.post('api/v1/settings/' + this.session.getLoggedInUser().guid, {
            mature: active,
            boost_rating: this.rating
        }).catch(function (e) {
            window.Minds.user.mature = !active;
        });
    };
    NewsfeedBoostRotator.prototype.setRating = function (rating) {
        this.rating = rating;
        this.session.getLoggedInUser().boost_rating = rating;
        this.boosts = [];
        this.load();
        this.client.post('api/v1/settings/' + this.session.getLoggedInUser().guid, {
            mature: this.session.getLoggedInUser().mature,
            boost_rating: rating,
        });
    };
    NewsfeedBoostRotator.prototype.toggleRating = function () {
        if (this.rating != 1) {
            this.setRating(1);
        }
        else {
            this.setRating(2);
        }
        this.detectChanges();
    };
    NewsfeedBoostRotator.prototype.ratingMenuHandler = function () {
        this.ratingMenuToggle = !this.ratingMenuToggle;
    };
    NewsfeedBoostRotator.prototype.start = function () {
        var _this = this;
        if (this.rotator)
            window.clearInterval(this.rotator);
        this.running = true;
        this.rotator = setInterval(function (e) {
            if (_this.paused) {
                return;
            }
            _this.next();
        }, this.interval * 1000);
    };
    NewsfeedBoostRotator.prototype.isVisible = function () {
        var bounds = this.element.nativeElement.getBoundingClientRect();
        if (bounds.top > 0) {
            if (!this.running)
                this.start();
        }
        else {
            console.log('[rotator]: out of view', this.rotator);
            if (this.running) {
                this.running = false;
                window.clearInterval(this.rotator);
            }
        }
    };
    NewsfeedBoostRotator.prototype.recordImpression = function (position, force) {
        if ((Date.now() > this.lastTs + 1000 || force) && this.boosts[position].boosted_guid) {
            this.client.put('api/v1/boost/fetch/newsfeed/' + this.boosts[position].boosted_guid);
        }
        this.lastTs = Date.now();
        window.localStorage.setItem('boost-rotator-offset', this.boosts[position].boosted_guid);
    };
    NewsfeedBoostRotator.prototype.active = function () {
        this.isVisible();
    };
    NewsfeedBoostRotator.prototype.inActive = function () {
        this.running = false;
        window.clearInterval(this.rotator);
    };
    NewsfeedBoostRotator.prototype.mouseOver = function () {
        this.running = false;
        window.clearInterval(this.rotator);
    };
    NewsfeedBoostRotator.prototype.mouseOut = function () {
        this.isVisible();
    };
    NewsfeedBoostRotator.prototype.pause = function () {
        this.paused = true;
    };
    NewsfeedBoostRotator.prototype.prev = function () {
        if (this.currentPosition <= 0) {
            this.currentPosition = this.boosts.length - 1;
        }
        else {
            this.currentPosition--;
        }
        this.recordImpression(this.currentPosition, false);
    };
    NewsfeedBoostRotator.prototype.next = function () {
        var _this = this;
        if (this.currentPosition + 1 > this.boosts.length - 1) {
            this.load()
                .then(function () {
                _this.currentPosition++;
            })
                .catch(function () {
                _this.currentPosition = 0;
            });
        }
        else {
            this.currentPosition++;
        }
        this.recordImpression(this.currentPosition, false);
    };
    NewsfeedBoostRotator.prototype.disable = function () {
        var _this = this;
        this.session.getLoggedInUser().disabled_boost = true;
        this.disabled = true;
        this.client.put('api/v1/plus/boost')
            .catch(function () {
            _this.session.getLoggedInUser().disabled_boost = false;
            _this.disabled = false;
        });
    };
    NewsfeedBoostRotator.prototype.enable = function () {
        var _this = this;
        this.session.getLoggedInUser().disabled_boost = false;
        this.disabled = false;
        this.client.delete('api/v1/plus/boost')
            .catch(function () {
            _this.session.getLoggedInUser().disabled_boost = true;
            _this.disabled = true;
        });
    };
    NewsfeedBoostRotator.prototype.selectCategories = function () {
        this.router.navigate(['/settings/general/categories']);
    };
    NewsfeedBoostRotator.prototype.detectChanges = function () {
        this.cd.markForCheck();
        this.cd.detectChanges();
    };
    NewsfeedBoostRotator.prototype.ngOnDestroy = function () {
        if (this.rotator)
            window.clearInterval(this.rotator);
        this.scroll.unListen(this.scroll_listener);
    };
    return NewsfeedBoostRotator;
}());
NewsfeedBoostRotator = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-newsfeed-boost-rotator',
        host: {
            '(window:blur)': 'inActive()',
            '(window:focus)': 'active()',
            '(mouseover)': 'mouseOver()',
            '(mouseout)': 'mouseOut()'
        },
        inputs: ['interval'],
        templateUrl: 'boost-rotator.html'
    }),
    __metadata("design:paramtypes", [router_1.Router,
        api_1.Client,
        scroll_1.ScrollService,
        storage_1.Storage,
        core_1.ElementRef,
        core_1.ChangeDetectorRef])
], NewsfeedBoostRotator);
exports.NewsfeedBoostRotator = NewsfeedBoostRotator;
//# sourceMappingURL=boost-rotator.js.map