"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var title_1 = require("../../services/ux/title");
var session_1 = require("../../services/session");
var Admin = (function () {
    function Admin(route, title, router) {
        this.route = route;
        this.title = title;
        this.router = router;
        this.filter = '';
        this.session = session_1.SessionFactory.build();
    }
    Admin.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.session.isAdmin()) {
            this.router.navigate(['/']);
        }
        this.title.setTitle('Admin');
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            if (params['filter']) {
                _this.filter = params['filter'];
            }
        });
    };
    Admin.prototype.ngOnDestroy = function () {
        this.paramsSubscription.unsubscribe();
    };
    return Admin;
}());
Admin = __decorate([
    core_1.Component({
        selector: 'minds-admin',
        template: "\n    <minds-admin-analytics *ngIf=\"filter == 'analytics'\"></minds-admin-analytics>\n    <minds-admin-boosts *ngIf=\"filter == 'boosts'\"></minds-admin-boosts>\n    <minds-admin-pages *ngIf=\"filter == 'pages'\"></minds-admin-pages>\n    <minds-admin-reports *ngIf=\"filter == 'reports' || filter == 'appeals'\"></minds-admin-reports>\n    <minds-admin-monetization *ngIf=\"filter == 'monetization'\"></minds-admin-monetization>\n    <minds-admin-programs *ngIf=\"filter == 'programs'\"></minds-admin-programs>\n    <minds-admin-payouts *ngIf=\"filter == 'payouts'\"></minds-admin-payouts>\n    <minds-admin-featured *ngIf=\"filter == 'featured'\"></minds-admin-featured>\n    <minds-admin-tagcloud *ngIf=\"filter == 'tagcloud'\"></minds-admin-tagcloud>\n    <m-admin--verify *ngIf=\"filter == 'verify'\"></m-admin--verify>\n  "
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute, title_1.MindsTitle, router_1.Router])
], Admin);
exports.Admin = Admin;
//# sourceMappingURL=admin.js.map