"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../../services/api");
var AdminAnalytics = (function () {
    function AdminAnalytics(client) {
        this.client = client;
        this.boost_newsfeed = {
            review: 0,
            approved: 0,
            percent: 50,
            total: 0,
            review_backlog: 0,
            approved_backlog: 0,
            impressions: 0,
            impressions_met: 0
        };
        this.getActives();
        this.getSignups();
        this.getRetention();
        this.getBoosts();
    }
    AdminAnalytics.prototype.getActives = function () {
        var self = this;
        this.client.get('api/v1/admin/analytics/active')
            .then(function (response) {
            self.dam = response['daily'];
            self.dam_list = response['daily'].slice(0).reverse();
            self.mam = response['monthly'];
            self.mam_list = response['monthly'].slice(0).reverse();
        });
    };
    AdminAnalytics.prototype.getSignups = function () {
        var _this = this;
        this.client.get('api/v1/admin/analytics/signups')
            .then(function (response) {
            _this.signups = response['daily'];
            _this.signups_list = response['daily'].slice(0).reverse();
        });
    };
    AdminAnalytics.prototype.getRetention = function () {
        var _this = this;
        this.client.get('api/v1/admin/analytics/retention')
            .then(function (response) {
            _this.retention = response.retention[0];
            console.log(_this.retention);
        });
    };
    AdminAnalytics.prototype.getBoosts = function () {
        var self = this;
        this.client.get('api/v1/admin/analytics/boost')
            .then(function (response) {
            self.boost_newsfeed = response.newsfeed;
            self.boost_newsfeed.total = self.boost_newsfeed.review + self.boost_newsfeed.approved;
            self.boost_newsfeed.percent = (self.boost_newsfeed.approved / self.boost_newsfeed.total) * 100;
        });
    };
    return AdminAnalytics;
}());
AdminAnalytics = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-admin-analytics',
        templateUrl: 'analytics.html'
    }),
    __metadata("design:paramtypes", [api_1.Client])
], AdminAnalytics);
exports.AdminAnalytics = AdminAnalytics;
//# sourceMappingURL=analytics.js.map