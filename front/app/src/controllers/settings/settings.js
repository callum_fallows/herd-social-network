"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var api_1 = require("../../services/api");
var title_1 = require("../../services/ux/title");
var session_1 = require("../../services/session");
var Settings = (function () {
    function Settings(client, router, route, title) {
        this.client = client;
        this.router = router;
        this.route = route;
        this.title = title;
        this.session = session_1.SessionFactory.build();
    }
    Settings.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.session.isLoggedIn()) {
            this.router.navigate(['/login']);
        }
        this.minds = window.Minds;
        this.title.setTitle('Settings');
        this.filter = 'general';
        this.account_time_created = window.Minds.user.time_created;
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            if (params['filter']) {
                _this.filter = params['filter'];
            }
            else {
                _this.filter = 'general';
            }
            if (params['card']) {
                _this.card = params['card'];
            }
        });
    };
    Settings.prototype.ngOnDestroy = function () {
        this.paramsSubscription.unsubscribe();
    };
    return Settings;
}());
Settings = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-settings',
        templateUrl: 'settings.html'
    }),
    __metadata("design:paramtypes", [api_1.Client, router_1.Router, router_1.ActivatedRoute, title_1.MindsTitle])
], Settings);
exports.Settings = Settings;
//# sourceMappingURL=settings.js.map