"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../../services/api");
var SettingsTwoFactor = (function () {
    function SettingsTwoFactor(client) {
        this.client = client;
        this.waitingForCheck = false;
        this.sendingSms = false;
        this.inProgress = false;
        this.error = '';
        this.minds = window.Minds;
        this.load();
    }
    SettingsTwoFactor.prototype.load = function () {
        var _this = this;
        this.inProgress = true;
        this.client.get('api/v1/twofactor')
            .then(function (response) {
            if (response.telno)
                _this.telno = response.telno;
            _this.inProgress = false;
        });
    };
    SettingsTwoFactor.prototype.setup = function (smsNumber) {
        var _this = this;
        this.telno = smsNumber;
        this.waitingForCheck = true;
        this.sendingSms = true;
        this.error = '';
        this.client.post('api/v1/twofactor/setup', { tel: smsNumber })
            .then(function (response) {
            _this.secret = response.secret;
            _this.sendingSms = false;
        })
            .catch(function () {
            _this.waitingForCheck = false;
            _this.sendingSms = false;
            _this.telno = null;
            _this.error = 'The phone number you entered was incorrect. Please, try again.';
        });
    };
    SettingsTwoFactor.prototype.check = function (code) {
        var _this = this;
        this.client.post('api/v1/twofactor/check/' + this.secret, {
            code: code,
            telno: this.telno
        })
            .then(function (response) {
            _this.waitingForCheck = false;
        })
            .catch(function (response) {
            _this.waitingForCheck = false;
            _this.telno = null;
            _this.error = 'The code was incorrect. Please, try again.';
        });
    };
    SettingsTwoFactor.prototype.retry = function () {
        this.telno = null;
        this.waitingForCheck = false;
    };
    SettingsTwoFactor.prototype.cancel = function () {
        this.client.delete('api/v1/twofactor');
        this.telno = null;
        this.error = '';
    };
    return SettingsTwoFactor;
}());
SettingsTwoFactor = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-settings-two-factor',
        inputs: ['object'],
        templateUrl: 'two-factor.html'
    }),
    __metadata("design:paramtypes", [api_1.Client])
], SettingsTwoFactor);
exports.SettingsTwoFactor = SettingsTwoFactor;
//# sourceMappingURL=two-factor.js.map