"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var api_1 = require("../../../services/api");
var SettingsDisableChannel = (function () {
    function SettingsDisableChannel(client, router) {
        this.client = client;
        this.router = router;
        this.minds = window.Minds;
    }
    SettingsDisableChannel.prototype.disable = function () {
        var _this = this;
        this.client.delete('api/v1/channel')
            .then(function (response) {
            _this.router.navigate(['/logout']);
        })
            .catch(function (e) {
            alert('Sorry, we could not disable your account');
        });
    };
    return SettingsDisableChannel;
}());
SettingsDisableChannel = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-settings-disable-channel',
        inputs: ['object'],
        templateUrl: 'disable.html'
    }),
    __metadata("design:paramtypes", [api_1.Client, router_1.Router])
], SettingsDisableChannel);
exports.SettingsDisableChannel = SettingsDisableChannel;
//# sourceMappingURL=disable.js.map