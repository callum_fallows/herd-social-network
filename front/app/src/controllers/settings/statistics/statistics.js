"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../../services/api");
var SettingsStatistics = (function () {
    function SettingsStatistics(client) {
        this.client = client;
        this.data = {
            fullname: 'minds',
            email: 'minds@minds.com',
            memberSince: new Date(),
            lastLogin: new Date(),
            storage: '0 GB\'s',
            bandwidth: '0 GB\'s',
            referrals: 500,
            earnings: 123123
        };
        this.minds = window.Minds;
        this.minds.cdn_url = 'https://d3ae0shxev0cb7.cloudfront.net';
    }
    return SettingsStatistics;
}());
SettingsStatistics = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-settings-statistics',
        inputs: ['object'],
        templateUrl: 'statistics.html',
    }),
    __metadata("design:paramtypes", [api_1.Client])
], SettingsStatistics);
exports.SettingsStatistics = SettingsStatistics;
//# sourceMappingURL=statistics.js.map