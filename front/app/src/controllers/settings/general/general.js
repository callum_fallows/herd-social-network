"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var session_1 = require("../../../services/session");
var api_1 = require("../../../services/api");
var third_party_networks_1 = require("../../../services/third-party-networks");
var SettingsGeneral = (function () {
    function SettingsGeneral(element, client, route, thirdpartynetworks) {
        this.element = element;
        this.client = client;
        this.route = route;
        this.thirdpartynetworks = thirdpartynetworks;
        this.session = session_1.SessionFactory.build();
        this.error = '';
        this.changed = false;
        this.saved = false;
        this.inProgress = false;
        this.guid = '';
        this.mature = false;
        this.enabled_mails = true;
        this.language = 'en';
        this.selectedCategories = [];
        this.minds = window.Minds;
        this.getCategories();
    }
    SettingsGeneral.prototype.ngOnInit = function () {
        var _this = this;
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            if (params['guid'] && params['guid'] === _this.session.getLoggedInUser().guid) {
                _this.load(true);
            }
            else {
                _this.load(false);
            }
        });
    };
    SettingsGeneral.prototype.ngAfterViewInit = function () {
        if (this.card && this.card !== '') {
            var el = this.element.nativeElement.querySelector('.m-settings--' + this.card);
            window.scrollTo(0, el.offsetTop - 64);
        }
    };
    SettingsGeneral.prototype.ngOnDestroy = function () {
        this.paramsSubscription.unsubscribe();
    };
    SettingsGeneral.prototype.load = function (remote) {
        var _this = this;
        if (remote === void 0) { remote = false; }
        if (!remote) {
            var user = this.session.getLoggedInUser();
            this.name = user.name;
        }
        this.client.get('api/v1/settings/' + this.guid)
            .then(function (response) {
            console.log('LOAD', response.channel);
            _this.email = response.channel.email;
            _this.mature = !!parseInt(response.channel.mature, 10);
            _this.enabled_mails = !parseInt(response.channel.disabled_emails, 10);
            _this.language = response.channel.language || 'en';
            _this.selectedCategories = response.channel.categories || [];
            _this.thirdpartynetworks.overrideStatus(response.thirdpartynetworks);
            if (window.Minds.user) {
                window.Minds.user.mature = _this.mature;
            }
            if (_this.selectedCategories.length > 0) {
                _this.selectedCategories.forEach(function (item, index, array) {
                    var i = _this.categories.findIndex(function (i) { return i.id === item; });
                    if (i !== -1)
                        _this.categories[i].selected = true;
                });
            }
        });
    };
    SettingsGeneral.prototype.canSubmit = function () {
        if (!this.changed)
            return false;
        if (this.password && !this.password1 || this.password && !this.password2)
            return false;
        if (this.password1 && !this.password) {
            this.error = 'You must enter your current password';
            return false;
        }
        if (this.password1 !== this.password2) {
            this.error = 'Your new passwords do not match';
            return false;
        }
        this.error = '';
        return true;
    };
    SettingsGeneral.prototype.change = function () {
        this.changed = true;
        this.saved = false;
    };
    SettingsGeneral.prototype.save = function () {
        var _this = this;
        if (!this.canSubmit())
            return;
        this.inProgress = true;
        this.client.post('api/v1/settings/' + this.guid, {
            name: this.name,
            email: this.email,
            password: this.password,
            new_password: this.password2,
            mature: this.mature ? 1 : 0,
            disabled_emails: this.enabled_mails ? 0 : 1,
            language: this.language,
            categories: this.selectedCategories
        })
            .then(function (response) {
            _this.changed = false;
            _this.saved = true;
            _this.error = '';
            _this.password = '';
            _this.password1 = '';
            _this.password2 = '';
            if (window.Minds.user) {
                window.Minds.user.mature = _this.mature ? 1 : 0;
                if (window.Minds.user.name !== _this.name) {
                    window.Minds.user.name = _this.name;
                }
            }
            if (_this.language !== window.Minds['language']) {
                window.location.reload(true);
            }
            _this.inProgress = false;
        });
    };
    SettingsGeneral.prototype.connectFb = function () {
        var _this = this;
        this.thirdpartynetworks.connect('facebook')
            .then(function () {
            _this.load();
        });
    };
    SettingsGeneral.prototype.connectTw = function () {
        var _this = this;
        this.thirdpartynetworks.connect('twitter')
            .then(function () {
            _this.load();
        });
    };
    SettingsGeneral.prototype.removeFbLogin = function () {
        this.thirdpartynetworks.removeFbLogin();
    };
    SettingsGeneral.prototype.removeFb = function () {
        this.thirdpartynetworks.disconnect('facebook');
    };
    SettingsGeneral.prototype.removeTw = function () {
        this.thirdpartynetworks.disconnect('twitter');
    };
    SettingsGeneral.prototype.getCategories = function () {
        this.categories = [];
        for (var id in window.Minds.categories) {
            this.categories.push({
                id: id,
                label: window.Minds.categories[id],
                selected: false
            });
        }
        this.categories.sort(function (a, b) { return a.label > b.label ? 1 : -1; });
    };
    SettingsGeneral.prototype.onCategoryClick = function (category) {
        category.selected = !category.selected;
        if (category.selected) {
            this.selectedCategories.push(category.id);
        }
        else {
            this.selectedCategories.splice(this.selectedCategories.indexOf(category.id), 1);
        }
        this.changed = true;
        this.saved = false;
    };
    return SettingsGeneral;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], SettingsGeneral.prototype, "object", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], SettingsGeneral.prototype, "card", void 0);
SettingsGeneral = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-settings-general',
        templateUrl: 'general.html'
    }),
    __metadata("design:paramtypes", [core_1.ElementRef,
        api_1.Client,
        router_1.ActivatedRoute,
        third_party_networks_1.ThirdPartyNetworksService])
], SettingsGeneral);
exports.SettingsGeneral = SettingsGeneral;
//# sourceMappingURL=general.js.map