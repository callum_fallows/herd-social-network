"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var title_1 = require("../../../services/ux/title");
var api_1 = require("../../../services/api");
var session_1 = require("../../../services/session");
var ForgotPassword = (function () {
    function ForgotPassword(client, router, route, title) {
        this.client = client;
        this.router = router;
        this.route = route;
        this.title = title;
        this.session = session_1.SessionFactory.build();
        this.error = '';
        this.inProgress = false;
        this.step = 1;
        this.username = '';
        this.code = '';
    }
    ForgotPassword.prototype.ngOnInit = function () {
        var _this = this;
        this.title.setTitle('Forgot Password');
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            if (params['code']) {
                _this.setCode(params['code']);
            }
            if (params['username']) {
                _this.username = params['username'];
            }
        });
    };
    ForgotPassword.prototype.ngOnDestroy = function () {
        this.paramsSubscription.unsubscribe();
    };
    ForgotPassword.prototype.request = function (username) {
        var _this = this;
        this.error = '';
        this.inProgress = true;
        var self = this;
        this.client.post('api/v1/forgotpassword/request', {
            username: username.value
        })
            .then(function (data) {
            username.value = '';
            _this.inProgress = false;
            self.step = 2;
        })
            .catch(function (e) {
            _this.inProgress = false;
            if (e.status === 'failed') {
                self.error = 'There was a problem trying to reset your password. Please try again.';
            }
            if (e.status === 'error') {
                self.error = e.message;
            }
        });
    };
    ForgotPassword.prototype.setCode = function (code) {
        this.step = 3;
        this.code = code;
    };
    ForgotPassword.prototype.reset = function (password) {
        var self = this;
        this.client.post('api/v1/forgotpassword/reset', {
            password: password.value,
            code: this.code,
            username: this.username
        })
            .then(function (response) {
            self.session.login(response.user);
            self.router.navigate(['/newsfeed']);
        })
            .catch(function (e) {
            self.error = e.message;
            setTimeout(function () {
                self.router.navigate(['/login']);
            }, 2000);
        });
    };
    return ForgotPassword;
}());
ForgotPassword = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-register',
        templateUrl: 'forgot-password.html'
    }),
    __metadata("design:paramtypes", [api_1.Client, router_1.Router, router_1.ActivatedRoute, title_1.MindsTitle])
], ForgotPassword);
exports.ForgotPassword = ForgotPassword;
//# sourceMappingURL=forgot-password.js.map