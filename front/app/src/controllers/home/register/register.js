"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var api_1 = require("../../../services/api");
var session_1 = require("../../../services/session");
var service_1 = require("../../../modules/modals/signup/service");
var login_referrer_service_1 = require("../../../services/login-referrer.service");
var Register = (function () {
    function Register(client, router, route, modal, loginReferrer) {
        this.client = client;
        this.router = router;
        this.route = route;
        this.modal = modal;
        this.loginReferrer = loginReferrer;
        this.minds = window.Minds;
        this.session = session_1.SessionFactory.build();
        this.errorMessage = '';
        this.twofactorToken = '';
        this.hideLogin = false;
        this.inProgress = false;
        this.flags = {
            canPlayInlineVideos: true
        };
    }
    Register.prototype.ngOnInit = function () {
        var _this = this;
        if (this.session.isLoggedIn()) {
            this.loginReferrer.navigate();
        }
        this.paramsSubscription = this.route.params.subscribe(function (params) {
            if (params['referrer']) {
                _this.referrer = params['referrer'];
            }
        });
        if (/iP(hone|od)/.test(window.navigator.userAgent)) {
            this.flags.canPlayInlineVideos = false;
        }
    };
    Register.prototype.ngOnDestroy = function () {
        this.paramsSubscription.unsubscribe();
    };
    Register.prototype.registered = function () {
        this.modal.setDisplay('onboarding').open();
        this.loginReferrer.navigate({
            defaultUrl: '/' + this.session.getLoggedInUser().username + ';onboarding=1'
        });
    };
    return Register;
}());
Register = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-register',
        templateUrl: 'register.html'
    }),
    __metadata("design:paramtypes", [api_1.Client,
        router_1.Router,
        router_1.ActivatedRoute,
        service_1.SignupModalService,
        login_referrer_service_1.LoginReferrerService])
], Register);
exports.Register = Register;
//# sourceMappingURL=register.js.map