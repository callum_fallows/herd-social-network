"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var navigation_1 = require("../../../services/navigation");
var session_1 = require("../../../services/session");
var title_1 = require("../../../services/ux/title");
var api_1 = require("../../../services/api");
var service_1 = require("../../../modules/modals/signup/service");
var login_referrer_service_1 = require("../../../services/login-referrer.service");
var Homepage = (function () {
    function Homepage(client, title, router, navigation, modal, loginReferrer) {
        this.client = client;
        this.title = title;
        this.router = router;
        this.navigation = navigation;
        this.modal = modal;
        this.loginReferrer = loginReferrer;
        this.videos = [];
        this.blogs = [];
        this.channels = [];
        this.stream = {
            1: [],
            2: [],
            3: []
        };
        this.offset = '';
        this.inProgress = false;
        this.session = session_1.SessionFactory.build();
        this.minds = window.Minds;
        this.flags = {
            canPlayInlineVideos: true
        };
        this.title.setTitle('Home');
        this.loadStream();
        if (/iP(hone|od)/.test(window.navigator.userAgent)) {
            this.flags.canPlayInlineVideos = false;
        }
    }
    Homepage.prototype.loadStream = function (refresh) {
        var _this = this;
        if (refresh === void 0) { refresh = false; }
        this.inProgress = true;
        this.client.get('api/v1/newsfeed/featured', { limit: 24, offset: this.offset })
            .then(function (response) {
            var col = 0;
            for (var _i = 0, _a = response.activity; _i < _a.length; _i++) {
                var activity = _a[_i];
                if (col++ >= 3)
                    col = 1;
                _this.stream[col].push(activity);
            }
            _this.offset = response['load-next'];
            _this.inProgress = false;
        })
            .catch(function () {
            _this.inProgress = false;
        });
    };
    Homepage.prototype.loadVideos = function () {
        var _this = this;
        this.client.get('api/v1/entities/featured/videos', { limit: 4 })
            .then(function (response) {
            _this.videos = response.entities;
        });
    };
    Homepage.prototype.loadBlogs = function () {
        var _this = this;
        this.client.get('api/v1/blog/featured', { limit: 4 })
            .then(function (response) {
            _this.blogs = response.blogs;
        });
    };
    Homepage.prototype.registered = function () {
        this.modal.setDisplay('categories').open();
        this.loginReferrer.navigate({
            defaultUrl: '/' + this.session.getLoggedInUser().username + ';onboarding=1'
        });
    };
    return Homepage;
}());
Homepage = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-homepage',
        templateUrl: 'homepage.html'
    }),
    __metadata("design:paramtypes", [api_1.Client,
        title_1.MindsTitle,
        router_1.Router,
        navigation_1.Navigation,
        service_1.SignupModalService,
        login_referrer_service_1.LoginReferrerService])
], Homepage);
exports.Homepage = Homepage;
//# sourceMappingURL=homepage.js.map