"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var client_1 = require("./api/client");
var AnalyticsService = (function () {
    function AnalyticsService(router, client) {
        var _this = this;
        this.router = router;
        this.client = client;
        this.id = 'UA-35146796-1';
        this.defaultPrevented = false;
        this.onRouterInit();
        this.router.events.subscribe(function (navigationState) {
            if (navigationState instanceof router_1.NavigationEnd) {
                try {
                    _this.onRouteChanged(navigationState.urlAfterRedirects);
                }
                catch (e) {
                    console.error('Minds: router hook(AnalyticsService)', e);
                }
            }
        });
    }
    AnalyticsService.send = function (type, fields) {
        if (fields === void 0) { fields = {}; }
        if (window.ga) {
            window.ga('send', type, fields);
        }
    };
    AnalyticsService._ = function (router, client) {
        return new AnalyticsService(router, client);
    };
    AnalyticsService.prototype.send = function (type, fields, entityGuid) {
        if (fields === void 0) { fields = {}; }
        if (window.ga) {
            window.ga('send', type, fields);
        }
        this.client.post('api/v1/analytics', { type: type, fields: fields, entityGuid: entityGuid });
    };
    AnalyticsService.prototype.onRouterInit = function () {
        window.ga('create', this.id, 'auto');
    };
    AnalyticsService.prototype.onRouteChanged = function (path) {
        if (!this.defaultPrevented) {
            AnalyticsService.send('pageview', { 'page': path });
        }
        this.defaultPrevented = false;
    };
    AnalyticsService.prototype.preventDefault = function () {
        this.defaultPrevented = true;
    };
    AnalyticsService.prototype.wasDefaultPrevented = function () {
        return this.defaultPrevented;
    };
    return AnalyticsService;
}());
AnalyticsService = __decorate([
    __param(0, core_1.Inject(router_1.Router)), __param(1, core_1.Inject(client_1.Client)),
    __metadata("design:paramtypes", [router_1.Router, client_1.Client])
], AnalyticsService);
exports.AnalyticsService = AnalyticsService;
//# sourceMappingURL=analytics.js.map