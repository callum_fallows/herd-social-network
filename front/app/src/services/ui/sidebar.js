"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Sidebar = (function () {
    function Sidebar() {
    }
    Sidebar._ = function () {
        return new Sidebar();
    };
    Sidebar.prototype.open = function () {
        var self = this;
        var drawer = document.getElementsByTagName('minds-sidebar')[0];
        if (drawer.classList.contains('is-visible')) {
            return this.close();
        }
        drawer.classList.add('is-visible');
        setTimeout(function () {
            var listener = function (e) {
                drawer.classList.remove('is-visible');
                document.removeEventListener('click', listener);
            };
            document.addEventListener('click', listener);
        }, 300);
    };
    Sidebar.prototype.close = function () {
        var drawer = document.getElementsByTagName('minds-sidebar')[0];
        drawer.classList.remove('is-visible');
    };
    return Sidebar;
}());
exports.Sidebar = Sidebar;
//# sourceMappingURL=sidebar.js.map