"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Experimental = (function () {
    function Experimental() {
    }
    Experimental.prototype.feature = function (feature) {
        return window.Minds.user &&
            window.Minds.user.feature_flags &&
            window.Minds.user.feature_flags.length &&
            window.Minds.user.feature_flags.indexOf(feature) > -1;
    };
    return Experimental;
}());
exports.Experimental = Experimental;
//# sourceMappingURL=experimental.js.map