"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/http");
var cookie_1 = require("../cookie");
var Client = (function () {
    function Client(http) {
        this.http = http;
        this.base = '/';
        this.cookie = new cookie_1.Cookie();
    }
    Client._ = function (http) {
        return new Client(http);
    };
    Client.prototype.get = function (endpoint, data, options) {
        var _this = this;
        if (data === void 0) { data = {}; }
        if (options === void 0) { options = {}; }
        var self = this;
        endpoint += '?' + this.buildParams(data);
        return new Promise(function (resolve, reject) {
            self.http.get(self.base + endpoint, _this.buildOptions(options))
                .subscribe(function (res) {
                var data = res.json();
                if (!data || data.status !== 'success')
                    return reject(data);
                return resolve(data);
            }, function (err) {
                if (err.status === 401 && err.json().loggedin === false) {
                    window.location.href = '/login';
                    return reject(err);
                }
                return reject(err);
            });
        });
    };
    Client.prototype.post = function (endpoint, data, options) {
        var _this = this;
        if (data === void 0) { data = {}; }
        if (options === void 0) { options = {}; }
        var self = this;
        return new Promise(function (resolve, reject) {
            self.http.post(self.base + endpoint, JSON.stringify(data), _this.buildOptions(options))
                .subscribe(function (res) {
                var data = res.json();
                if (!data || data.status !== 'success')
                    return reject(data);
                return resolve(data);
            }, function (err) {
                if (err.status === 401 && err.json().loggedin === false) {
                    window.location.href = '/login';
                    return reject(err);
                }
                if (err.status !== 200) {
                    return reject(err.json());
                }
            });
        });
    };
    Client.prototype.put = function (endpoint, data, options) {
        var _this = this;
        if (data === void 0) { data = {}; }
        if (options === void 0) { options = {}; }
        var self = this;
        return new Promise(function (resolve, reject) {
            self.http.put(self.base + endpoint, JSON.stringify(data), _this.buildOptions(options))
                .subscribe(function (res) {
                var data = res.json();
                if (!data || data.status !== 'success')
                    return reject(data);
                return resolve(data);
            }, function (err) {
                if (err.status === 401 && err.json().loggedin === false) {
                    window.location.href = '/login';
                    return reject(err);
                }
                if (err.status !== 200) {
                    return reject(err.json());
                }
            });
        });
    };
    Client.prototype.delete = function (endpoint, data, options) {
        var _this = this;
        if (data === void 0) { data = {}; }
        if (options === void 0) { options = {}; }
        var self = this;
        return new Promise(function (resolve, reject) {
            self.http.delete(self.base + endpoint, _this.buildOptions(options))
                .subscribe(function (res) {
                var data = res.json();
                if (!data || data.status !== 'success')
                    return reject(data);
                return resolve(data);
            }, function (err) {
                if (err.status === 401 && err.json().loggedin === false) {
                    window.location.href = '/login';
                    return reject(err);
                }
                if (err.status !== 200) {
                    return reject(err.json());
                }
            });
        });
    };
    Client.prototype.buildParams = function (object) {
        return Object.keys(object).map(function (k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(object[k]);
        }).join('&');
    };
    Client.prototype.buildOptions = function (options) {
        var XSRF_TOKEN = this.cookie.get('XSRF-TOKEN');
        var headers = new http_1.Headers();
        headers.append('X-XSRF-TOKEN', XSRF_TOKEN);
        var Objecti = Object;
        return Objecti.assign(options, {
            headers: headers,
            cache: true
        });
    };
    return Client;
}());
exports.Client = Client;
//# sourceMappingURL=client.js.map