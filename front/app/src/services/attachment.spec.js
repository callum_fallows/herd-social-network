"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var attachment_1 = require("./attachment");
var api_1 = require("./api");
var client_mock_spec_1 = require("../../tests/client-mock.spec");
var upload_mock_spec_1 = require("../../tests/upload-mock.spec");
var testing_1 = require("@angular/core/testing");
describe('Service: Attachment Service', function () {
    var service;
    var mockObject;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [attachment_1.AttachmentService],
            providers: [
                { provide: api_1.Upload, useValue: upload_mock_spec_1.uploadMock },
                { provide: api_1.Client, useValue: client_mock_spec_1.clientMock }
            ]
        });
        service = new attachment_1.AttachmentService(client_mock_spec_1.clientMock, upload_mock_spec_1.uploadMock);
        mockObject = {
            'guid': '758019279000969217',
            'type': 'object',
            'subtype': 'video',
            'time_created': '1506101878',
            'time_updated': '1506101878',
            'container_guid': '758019184876593168',
            'owner_guid': '758019184876593168',
            'access_id': '2',
            'featured': false,
            'featured_id': false,
            'ownerObj': {
                'guid': '758019184876593168',
                'type': 'user',
                'name': 'nicoronchiprod',
                'username': 'nicoronchiprod'
            },
            'category': false,
            'flags': { 'mature': true },
            'wire_threshold': '0',
            'thumbnail': false,
            'cinemr_guid': '758019279000969217',
            'license': false,
            'monetized': false,
            'mature': false,
            'boost_rejection_reason': -1,
            'thumbnail_src': 'https:\/\/d3ae0shxev0cb7.cloudfront.net\/api\/v1\/media\/thumbnails\/758019279000969217',
            'src': { '360.mp4': 'https:\/\/d2isvgrdif6ua5.cloudfront.net\/cinemr_com\/758019279000969217\/360.mp4', '720.mp4': 'https:\/\/d2isvgrdif6ua5.cloudfront.net\/cinemr_com\/758019279000969217\/720.mp4' },
            'play:count': 6,
            'description': ''
        };
    }));
    it('parseMature should return false when undefined', function () {
        expect(service.parseMaturity(undefined)).toEqual(false);
    });
    it('service should should prioritice flag as mature when repeated value', function () {
        expect(service.parseMaturity(mockObject)).toEqual(true);
    });
    it('service should should prioritice flag as mature when repeated value', function () {
        mockObject.mature = undefined;
        expect(service.parseMaturity(mockObject)).toEqual(true);
    });
    it('service should should return false if not present in flags, and mature:false', function () {
        mockObject.flags = {};
        expect(service.parseMaturity(mockObject)).toEqual(false);
    });
    it('service should should return false if not present in flags, and mature:true', function () {
        mockObject.mature = true;
        mockObject.flags = {};
        expect(service.parseMaturity(mockObject)).toEqual(false);
    });
});
//# sourceMappingURL=attachment.spec.js.map