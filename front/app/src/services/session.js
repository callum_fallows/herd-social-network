"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Session = (function () {
    function Session() {
        this.loggedinEmitter = new core_1.EventEmitter();
        this.userEmitter = new core_1.EventEmitter();
    }
    Session._ = function () {
        return new Session();
    };
    Session.prototype.isLoggedIn = function (observe) {
        if (observe === void 0) { observe = null; }
        if (observe) {
            this.loggedinEmitter.subscribe({
                next: function (is) {
                    if (is)
                        observe(true);
                    else
                        observe(false);
                }
            });
        }
        if (window.Minds.LoggedIn)
            return true;
        return false;
    };
    Session.prototype.isAdmin = function () {
        if (!this.isLoggedIn)
            return false;
        if (window.Minds.Admin)
            return true;
        return false;
    };
    Session.prototype.getLoggedInUser = function (observe) {
        if (observe === void 0) { observe = null; }
        if (observe) {
            this.userEmitter.subscribe({
                next: function (user) {
                    observe(user);
                }
            });
        }
        if (window.Minds.user)
            return window.Minds.user;
        return false;
    };
    Session.prototype.login = function (user) {
        if (user === void 0) { user = null; }
        window.localStorage.clear();
        this.userEmitter.next(user);
        window.Minds.user = user;
        if (user.admin === true)
            window.Minds.Admin = true;
        window.Minds.LoggedIn = true;
        this.loggedinEmitter.next(true);
    };
    Session.prototype.logout = function () {
        this.userEmitter.next(null);
        delete window.Minds.user;
        window.Minds.LoggedIn = false;
        window.localStorage.clear();
        this.loggedinEmitter.next(false);
    };
    return Session;
}());
exports.Session = Session;
var SessionFactory = (function () {
    function SessionFactory() {
    }
    SessionFactory.build = function () {
        if (!SessionFactory.instance) {
            var providers = core_1.ReflectiveInjector.resolve([Session]), injector = core_1.ReflectiveInjector.fromResolvedProviders(providers);
            SessionFactory.instance = injector.get(Session);
        }
        return SessionFactory.instance;
    };
    return SessionFactory;
}());
exports.SessionFactory = SessionFactory;
//# sourceMappingURL=session.js.map