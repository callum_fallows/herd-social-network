"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MindsTitle = (function () {
    function MindsTitle(title) {
        this.title = title;
        this.sep = ' | ';
        this.default_title = 'Minds';
        this.text = '';
    }
    MindsTitle._ = function (title) {
        return new MindsTitle(title);
    };
    MindsTitle.prototype.setTitle = function (value) {
        var title;
        if (value) {
            title = [value, this.default_title].join(this.sep);
        }
        else {
            title = this.default_title;
        }
        this.text = title;
        this.applyTitle();
    };
    MindsTitle.prototype.setCounter = function (value) {
        this.counter = value;
        this.applyTitle();
    };
    MindsTitle.prototype.applyTitle = function () {
        if (this.counter) {
            this.title.setTitle("(" + this.counter + ") " + this.text);
        }
        else {
            this.title.setTitle(this.text);
        }
    };
    return MindsTitle;
}());
exports.MindsTitle = MindsTitle;
//# sourceMappingURL=title.js.map