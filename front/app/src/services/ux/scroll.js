"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Rx_1 = require("rxjs/Rx");
var ScrollService = (function () {
    function ScrollService() {
        this.viewEmitter = new core_1.EventEmitter();
        this.view = document.getElementsByTagName('body')[0];
        this.view.scrollTop = 0;
        this.scroll = Rx_1.Observable.fromEvent(window, 'scroll');
    }
    ScrollService._ = function () {
        return new ScrollService();
    };
    ScrollService.prototype.fire = function () {
        this.scroll.next({ top: this.view.scrollTop, height: this.view.clientHeight });
    };
    ScrollService.prototype.listen = function (callback, debounce, throttle) {
        if (debounce === void 0) { debounce = 0; }
        if (throttle === void 0) { throttle = 0; }
        if (debounce) {
            return this.scroll
                .debounceTime(debounce)
                .subscribe(callback);
        }
        if (throttle) {
            return this.scroll
                .throttleTime(throttle)
                .subscribe(callback);
        }
        return this.scroll
            .subscribe(callback);
    };
    ScrollService.prototype.unListen = function (subscription) {
        subscription.unsubscribe();
    };
    ScrollService.prototype.listenForView = function () {
        var _this = this;
        if (!this.viewListener) {
            this.viewListener = this.scroll.debounceTime(500).subscribe(function (e) { _this.viewEmitter.next(e); });
        }
        return this.viewEmitter;
    };
    return ScrollService;
}());
exports.ScrollService = ScrollService;
//# sourceMappingURL=scroll.js.map