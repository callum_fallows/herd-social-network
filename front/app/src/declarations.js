"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var announcement_component_1 = require("./components/announcements/announcement.component");
var topbar_1 = require("./components/topbar/topbar");
var sidebar_navigation_1 = require("./components/sidebar-navigation/sidebar-navigation");
var hovercard_popup_1 = require("./components/hovercard-popup/hovercard-popup");
var graphs_1 = require("./components/graphs");
var svg_1 = require("./components/graphs/svg");
var points_1 = require("./components/graphs/points");
var topbar_navigation_1 = require("./components/topbar/topbar-navigation");
var forms_1 = require("./components/forms/forms");
var carousel_1 = require("./components/carousel");
var impressions_1 = require("./components/analytics/impressions");
var tooltip_1 = require("./components/tooltip/tooltip");
var tagcloud_component_1 = require("./components/tagcloud/tagcloud.component");
var analytics_1 = require("./controllers/admin/analytics/analytics");
var boosts_1 = require("./controllers/admin/boosts/boosts");
var pages_1 = require("./controllers/admin/pages/pages");
var reports_1 = require("./controllers/admin/reports/reports");
var monetization_1 = require("./controllers/admin/monetization/monetization");
var programs_component_1 = require("./controllers/admin/programs/programs.component");
var payouts_component_1 = require("./controllers/admin/payouts/payouts.component");
var featured_1 = require("./controllers/admin/featured/featured");
var tagcloud_component_2 = require("./controllers/admin/tagcloud/tagcloud.component");
var verify_component_1 = require("./controllers/admin/verify/verify.component");
var supporters_1 = require("./controllers/channels/supporters/supporters");
var subscribers_1 = require("./controllers/channels/subscribers/subscribers");
var subscriptions_1 = require("./controllers/channels/subscriptions/subscriptions");
var social_profiles_1 = require("./controllers/channels/social-profiles/social-profiles");
var boost_rotator_1 = require("./controllers/newsfeed/boost-rotator/boost-rotator");
var general_1 = require("./controllers/settings/general/general");
var statistics_1 = require("./controllers/settings/statistics/statistics");
var disable_1 = require("./controllers/settings/disable/disable");
var two_factor_1 = require("./controllers/settings/two-factor/two-factor");
var subscriptions_component_1 = require("./controllers/settings/subscriptions/subscriptions.component");
var comingsoon_1 = require("./controllers/home/comingsoon/comingsoon");
var theatre_1 = require("./controllers/media/view/views/theatre");
var grid_1 = require("./controllers/media/view/views/grid");
var thumbnail_selector_1 = require("./controllers/media/components/thumbnail-selector");
var recommended_component_1 = require("./controllers/media/view/recommended/recommended.component");
var rejection_reason_modal_component_1 = require("./controllers/admin/boosts/modal/rejection-reason-modal.component");
exports.MINDS_DECLARATIONS = [
    announcement_component_1.AnnouncementComponent,
    topbar_1.Topbar,
    sidebar_navigation_1.SidebarNavigation,
    hovercard_popup_1.HovercardPopup,
    graphs_1.MINDS_GRAPHS,
    svg_1.GraphSVG,
    points_1.GraphPoints,
    topbar_navigation_1.TopbarNavigation,
    forms_1.FORM_COMPONENTS,
    carousel_1.HerdCarousel,
    impressions_1.AnalyticsImpressions,
    tooltip_1.MindsTooltip,
    tagcloud_component_1.TagcloudComponent,
    analytics_1.AdminAnalytics,
    rejection_reason_modal_component_1.RejectionReasonModalComponent,
    boosts_1.AdminBoosts,
    pages_1.AdminPages,
    reports_1.AdminReports,
    monetization_1.AdminMonetization,
    programs_component_1.AdminPrograms,
    payouts_component_1.AdminPayouts,
    featured_1.AdminFeatured,
    tagcloud_component_2.AdminTagcloud,
    verify_component_1.AdminVerify,
    supporters_1.ChannelSupporters,
    subscribers_1.ChannelSubscribers,
    subscriptions_1.ChannelSubscriptions,
    social_profiles_1.ChannelSocialProfiles,
    boost_rotator_1.NewsfeedBoostRotator,
    general_1.SettingsGeneral,
    statistics_1.SettingsStatistics,
    disable_1.SettingsDisableChannel,
    two_factor_1.SettingsTwoFactor,
    subscriptions_component_1.SettingsSubscriptions,
    comingsoon_1.ComingSoon,
    theatre_1.MediaTheatre,
    grid_1.MediaGrid,
    thumbnail_selector_1.ThumbnailSelector,
    recommended_component_1.MediaViewRecommended
];
//# sourceMappingURL=declarations.js.map