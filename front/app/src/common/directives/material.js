"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ui_1 = require("../../services/ui");
var text_field_1 = require("./material/text-field");
var upload_1 = require("./material/upload");
var switch_1 = require("./material/switch");
var datetimepicker_directive_1 = require("./material/datetimepicker.directive");
var slider_1 = require("./material/slider");
var radio_1 = require("./material/radio/radio");
var Material = (function () {
    function Material(_element) {
        this.element = _element.nativeElement;
    }
    Material.prototype.ngAfterViewInit = function () {
        ui_1.Material.updateElement(this.element);
    };
    return Material;
}());
Material = __decorate([
    core_1.Directive({
        selector: '[mdl]',
        inputs: ['mdl']
    }),
    __metadata("design:paramtypes", [core_1.ElementRef])
], Material);
exports.Material = Material;
exports.MDL_DIRECTIVES = [
    Material,
    text_field_1.MaterialTextfield,
    upload_1.MaterialUpload,
    switch_1.MaterialSwitch,
    datetimepicker_directive_1.MaterialDateTimePickerDirective,
    slider_1.MaterialSlider,
    radio_1.MaterialRadio
];
//# sourceMappingURL=material.js.map