"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var MindsTinymce = (function () {
    function MindsTinymce() {
        this.ready = false;
        this.content = '';
        this.update = new core_1.EventEmitter();
        this.urlPatterns = [
            {
                regex: /youtu\.be\/([\w\-.]+)/,
                type: 'iframe', w: 560, h: 314,
                url: 'https://www.youtube.com/embed/$1',
                allowFullscreen: true
            },
            {
                regex: /youtube\.com(.+)v=([^&]+)/,
                type: 'iframe', w: 560, h: 314,
                url: 'https://www.youtube.com/embed/$2',
                allowFullscreen: true
            },
            {
                regex: /youtube.com\/embed\/([a-z0-9\-_]+(?:\?.+)?)/i,
                type: 'iframe', w: 560, h: 314,
                url: 'https://www.youtube.com/embed/$1',
                allowFullscreen: true
            },
            {
                regex: /vimeo\.com\/([0-9]+)/,
                type: 'iframe', w: 425, h: 350,
                url: 'https://player.vimeo.com/video/$1?title=0&byline=0&portrait=0&color=8dc7dc',
                allowFullscreen: true
            },
            {
                regex: /vimeo\.com\/(.*)\/([0-9]+)/,
                type: 'iframe', w: 425, h: 350,
                url: 'https://player.vimeo.com/video/$2?title=0&amp;byline=0',
                allowFullscreen: true
            },
            {
                regex: /maps\.google\.([a-z]{2,3})\/maps\/(.+)msid=(.+)/,
                type: 'iframe', w: 425, h: 350,
                url: 'https://maps.google.com/maps/ms?msid=$2&output=embed"',
                allowFullscreen: false
            },
            {
                regex: /dailymotion\.com\/video\/([^_]+)/,
                type: 'iframe', w: 480, h: 270,
                url: 'https://www.dailymotion.com/embed/video/$1',
                allowFullscreen: true
            }
        ];
    }
    MindsTinymce.prototype.ngOnInit = function () {
        var _this = this;
        tinymce.init({
            selector: 'minds-tinymce > textarea',
            autoresize_max_height: '400',
            content_css: '/stylesheets/main.css',
            format: 'raw',
            menubar: false,
            toolbar: 'styleselect'
                + ' | bold italic underline textcolor'
                + ' | alignleft aligncenter alignright alignjustify'
                + ' | bullist numlist'
                + ' | link image media'
                + ' | removeformat'
                + ' | code',
            statusbar: false,
            relative_urls: false,
            remove_script_host: false,
            media_url_resolver: function (data, resolve) {
                var _loop_1 = function (i) {
                    var pattern = _this.urlPatterns[i];
                    var match = pattern.regex.exec(data.url);
                    var url = void 0;
                    if (match) {
                        url = pattern.url;
                        for (i = 0; match[i]; i++) {
                            url = url.replace('$' + i, function () {
                                return match[i];
                            });
                        }
                        data.source1 = url;
                        data.type = pattern.type;
                        data.allowFullscreen = pattern.allowFullscreen;
                        data.width = data.width || pattern.w;
                        data.height = data.height || pattern.h;
                    }
                    out_i_1 = i;
                };
                var out_i_1;
                for (var i = 0; i < _this.urlPatterns.length; ++i) {
                    _loop_1(i);
                    i = out_i_1;
                }
                var html = '';
                if (data.type === 'iframe') {
                    html +=
                        "<iframe src=\"" + data.source1 + "\" width=\"" + data.width + "\" height=\"" + data.height + "\"\n            fullscreen=\"" + data.allowFullscreen + "\"></iframe>";
                }
                resolve({ 'html': html });
            },
            plugins: [
                'advlist autolink link image lists preview hr anchor pagebreak',
                'media nonbreaking code',
                'table directionality autoresize'
            ],
            setup: function (ed) {
                _this.editor = ed;
                ed.on('change', function (e) {
                    _this.ready = true;
                    _this.content = ed.getContent();
                    _this.update.next(ed.getContent());
                });
                ed.on('keyup', function (e) {
                    _this.ready = true;
                    _this.content = ed.getContent();
                    _this.update.next(ed.getContent());
                });
            }
        });
    };
    MindsTinymce.prototype.ngOnDestroy = function () {
        this.editor.setContent('');
        if (tinymce)
            tinymce.remove('minds-tinymce > textarea');
        this.content = '';
        this.ready = false;
    };
    Object.defineProperty(MindsTinymce.prototype, "_content", {
        set: function (value) {
            var _this = this;
            this.content = value;
            new Promise(function (resolve, reject) {
                if (_this.editor)
                    resolve(value);
            })
                .then(function (value) {
                if (!_this.ready && value && value !== _this.editor.getContent()) {
                    _this.ready = true;
                    _this.editor.setContent(value);
                }
            });
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MindsTinymce.prototype, "reset", {
        set: function (value) {
            if (value && this.editor.getContent()) {
                this.editor.setContent(this.content);
                this.ready = false;
            }
        },
        enumerable: true,
        configurable: true
    });
    return MindsTinymce;
}());
MindsTinymce = __decorate([
    core_1.Component({
        selector: 'minds-tinymce',
        inputs: ['_content: content', 'reset'],
        outputs: ['update: contentChange'],
        template: "\n    <textarea>{{content}}</textarea>\n  "
    })
], MindsTinymce);
exports.MindsTinymce = MindsTinymce;
//# sourceMappingURL=tinymce.js.map