"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t;
    return { next: verb(0), "throw": verb(1), "return": verb(2) };
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var session_1 = require("../../../../services/session");
var api_1 = require("../../../../services/api");
var AdminActionsButtonComponent = (function () {
    function AdminActionsButtonComponent(client) {
        this.client = client;
        this.session = session_1.SessionFactory.build();
        this.open = false;
    }
    AdminActionsButtonComponent.prototype.isSpam = function () {
        if (typeof this.object['spam'] !== 'undefined') {
            return this.object['spam'];
        }
        if (typeof this.object.flags !== 'undefined') {
            return this.object.flags['spam'];
        }
        return false;
    };
    AdminActionsButtonComponent.prototype.setSpam = function (value) {
        return __awaiter(this, void 0, void 0, function () {
            var e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.object) {
                            return [2 /*return*/];
                        }
                        if (typeof this.object['spam'] !== 'undefined') {
                            this.object['spam'] = value;
                        }
                        if (typeof this.object.flags !== 'undefined') {
                            this.object.flags['spam'] = value;
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 6, , 7]);
                        if (!value) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.client.put("api/v1/admin/spam/" + this.object.guid)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, this.client.delete("api/v1/admin/spam/" + this.object.guid)];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        e_1 = _a.sent();
                        if (typeof this.object['spam'] !== 'undefined') {
                            this.object['spam'] = !value;
                        }
                        if (typeof this.object.flags !== 'undefined') {
                            this.object.flags['spam'] = value;
                        }
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    AdminActionsButtonComponent.prototype.isDeleted = function () {
        if (typeof this.object['deleted'] !== 'undefined') {
            return this.object['deleted'];
        }
        if (typeof this.object.flags !== 'undefined') {
            return this.object.flags['deleted'];
        }
        return false;
    };
    AdminActionsButtonComponent.prototype.setDeleted = function (value) {
        return __awaiter(this, void 0, void 0, function () {
            var e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!this.object) {
                            return [2 /*return*/];
                        }
                        if (typeof this.object['deleted'] !== 'undefined') {
                            this.object['deleted'] = value;
                        }
                        if (typeof this.object.flags !== 'undefined') {
                            this.object.flags['deleted'] = value;
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 6, , 7]);
                        if (!value) return [3 /*break*/, 3];
                        return [4 /*yield*/, this.client.put("api/v1/admin/delete/" + this.object.guid)];
                    case 2:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 3: return [4 /*yield*/, this.client.delete("api/v1/admin/delete/" + this.object.guid)];
                    case 4:
                        _a.sent();
                        _a.label = 5;
                    case 5: return [3 /*break*/, 7];
                    case 6:
                        e_2 = _a.sent();
                        if (typeof this.object['deleted'] !== 'undefined') {
                            this.object['deleted'] = !value;
                        }
                        if (typeof this.object.flags !== 'undefined') {
                            this.object.flags['deleted'] = value;
                        }
                        return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    return AdminActionsButtonComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], AdminActionsButtonComponent.prototype, "object", void 0);
AdminActionsButtonComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'm-button--admin-actions',
        templateUrl: 'admin-actions.component.html'
    }),
    __metadata("design:paramtypes", [api_1.Client])
], AdminActionsButtonComponent);
exports.AdminActionsButtonComponent = AdminActionsButtonComponent;
//# sourceMappingURL=admin-actions.component.js.map