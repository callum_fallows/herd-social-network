"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var pipes_1 = require("./pipes/pipes");
var tooltip_component_1 = require("./components/tooltip/tooltip.component");
var footer_component_1 = require("./components/footer/footer.component");
var infinite_scroll_1 = require("./components/infinite-scroll/infinite-scroll");
var country_input_component_1 = require("./components/forms/country-input/country-input.component");
var date_input_component_1 = require("./components/forms/date-input/date-input.component");
var state_input_component_1 = require("./components/forms/state-input/state-input.component");
var read_more_directive_1 = require("./read-more/read-more.directive");
var button_component_1 = require("./read-more/button.component");
var badges_component_1 = require("./components/badges/badges.component");
var scheduler_1 = require("./components/scheduler/scheduler");
var modal_component_1 = require("./components/modal/modal.component");
var rich_embed_1 = require("./components/rich-embed/rich-embed");
var material_1 = require("./directives/material");
var autogrow_1 = require("./directives/autogrow");
var inline_autogrow_1 = require("./directives/inline-autogrow");
var emoji_1 = require("./directives/emoji");
var hovercard_1 = require("./directives/hovercard");
var scroll_lock_1 = require("./directives/scroll-lock");
var tags_1 = require("./directives/tags");
var tooltip_1 = require("./directives/tooltip");
var avatar_1 = require("./components/avatar/avatar");
var captcha_component_1 = require("./components/captcha/captcha.component");
var textarea_component_1 = require("./components/editors/textarea.component");
var tinymce_1 = require("./components/editors/tinymce");
var dynamic_host_directive_1 = require("./directives/dynamic-host.directive");
var card_component_1 = require("./components/card/card.component");
var button_component_2 = require("./components/button/button.component");
var overlay_modal_component_1 = require("./components/overlay-modal/overlay-modal.component");
var faq_component_1 = require("./components/faq/faq.component");
var chart_component_1 = require("./components/chart/chart.component");
var date_selector_component_1 = require("./components/date-selector/date-selector.component");
var admin_actions_component_1 = require("./components/button/admin-actions/admin-actions.component");
var inline_editor_component_1 = require("./components/editors/inline-editor.component");
var attachment_1 = require("../services/attachment");
var bound_switch_component_1 = require("./components/material/bound-switch.component");
var if_feature_directive_1 = require("./directives/if-feature.directive");
var emoji_2 = require("./components/emoji/emoji");
var CommonModule = (function () {
    function CommonModule() {
    }
    return CommonModule;
}());
CommonModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            router_1.RouterModule,
            forms_1.FormsModule
        ],
        declarations: [
            pipes_1.MINDS_PIPES,
            tooltip_component_1.TooltipComponent,
            footer_component_1.FooterComponent,
            infinite_scroll_1.InfiniteScroll,
            country_input_component_1.CountryInputComponent,
            date_input_component_1.DateInputComponent,
            state_input_component_1.StateInputComponent,
            scheduler_1.Scheduler,
            modal_component_1.Modal,
            read_more_directive_1.ReadMoreDirective,
            button_component_1.ReadMoreButtonComponent,
            badges_component_1.ChannelBadgesComponent,
            rich_embed_1.MindsRichEmbed,
            autogrow_1.AutoGrow,
            inline_autogrow_1.InlineAutoGrow,
            emoji_1.Emoji,
            emoji_2.MindsEmoji,
            hovercard_1.Hovercard,
            scroll_lock_1.ScrollLock,
            tags_1.TagsLinks,
            tooltip_1.Tooltip,
            material_1.MDL_DIRECTIVES,
            date_selector_component_1.DateSelectorComponent,
            avatar_1.MindsAvatar,
            captcha_component_1.CaptchaComponent,
            textarea_component_1.Textarea,
            tinymce_1.MindsTinymce,
            inline_editor_component_1.InlineEditorComponent,
            dynamic_host_directive_1.DynamicHostDirective,
            card_component_1.MindsCard,
            button_component_2.MindsButton,
            faq_component_1.FaqComponent,
            chart_component_1.ChartComponent,
            overlay_modal_component_1.OverlayModalComponent,
            admin_actions_component_1.AdminActionsButtonComponent,
            bound_switch_component_1.MaterialBoundSwitchComponent,
            if_feature_directive_1.IfFeatureDirective
        ],
        exports: [
            pipes_1.MINDS_PIPES,
            tooltip_component_1.TooltipComponent,
            footer_component_1.FooterComponent,
            infinite_scroll_1.InfiniteScroll,
            country_input_component_1.CountryInputComponent,
            date_input_component_1.DateInputComponent,
            state_input_component_1.StateInputComponent,
            scheduler_1.Scheduler,
            modal_component_1.Modal,
            read_more_directive_1.ReadMoreDirective,
            button_component_1.ReadMoreButtonComponent,
            badges_component_1.ChannelBadgesComponent,
            rich_embed_1.MindsRichEmbed,
            autogrow_1.AutoGrow,
            inline_autogrow_1.InlineAutoGrow,
            emoji_2.MindsEmoji,
            emoji_1.Emoji,
            hovercard_1.Hovercard,
            scroll_lock_1.ScrollLock,
            tags_1.TagsLinks,
            tooltip_1.Tooltip,
            material_1.MDL_DIRECTIVES,
            date_selector_component_1.DateSelectorComponent,
            avatar_1.MindsAvatar,
            captcha_component_1.CaptchaComponent,
            textarea_component_1.Textarea,
            tinymce_1.MindsTinymce,
            inline_editor_component_1.InlineEditorComponent,
            dynamic_host_directive_1.DynamicHostDirective,
            card_component_1.MindsCard,
            button_component_2.MindsButton,
            faq_component_1.FaqComponent,
            chart_component_1.ChartComponent,
            overlay_modal_component_1.OverlayModalComponent,
            admin_actions_component_1.AdminActionsButtonComponent,
            bound_switch_component_1.MaterialBoundSwitchComponent,
            if_feature_directive_1.IfFeatureDirective
        ],
        providers: [attachment_1.AttachmentService],
        entryComponents: []
    })
], CommonModule);
exports.CommonModule = CommonModule;
//# sourceMappingURL=common.module.js.map