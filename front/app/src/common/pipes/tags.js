"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var TagsPipe = (function () {
    function TagsPipe() {
    }
    TagsPipe.prototype.transform = function (value) {
        if (!value || typeof value !== 'string')
            return value;
        var url = /(\b(https?|ftp|file):\/\/[^\s\]\)]+)/gim;
        value = value.replace(url, '<a href="$1" target="_blank">$1</a>');
        var hash = /(^|\s)#(\w*[a-zA-Z_]+\w*)/gim;
        value = value.replace(hash, '$1<a href="/search;q=%23$2;ref=hashtag" target="_blank">#$2</a>');
        var at = /(^|\s)\@(\w*[a-zA-Z_]+\w*)/gim;
        value = value.replace(at, '$1<a class="tag" href="/$2" target="_blank">@$2</a>');
        return value;
    };
    return TagsPipe;
}());
TagsPipe = __decorate([
    core_1.Pipe({
        name: 'tags'
    })
], TagsPipe);
exports.TagsPipe = TagsPipe;
//# sourceMappingURL=tags.js.map