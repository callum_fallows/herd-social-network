"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var abbr_1 = require("./abbr");
var domain_1 = require("./domain");
var tags_1 = require("./tags");
var sanitize_1 = require("./sanitize");
var safe_1 = require("./safe");
var listable_1 = require("./listable");
var excerpt_1 = require("./excerpt");
exports.MINDS_PIPES = [abbr_1.AbbrPipe, domain_1.DomainPipe, tags_1.TagsPipe, sanitize_1.SanitizePipe, safe_1.SafePipe, listable_1.ListablePipe, excerpt_1.ExcerptPipe];
//# sourceMappingURL=pipes.js.map