"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AbbrPipe = (function () {
    function AbbrPipe() {
        this.abbrev = ['k', 'm', 'b', 't'];
    }
    AbbrPipe.prototype.transform = function (number, decimals) {
        if (decimals === void 0) { decimals = 2; }
        decimals = Math.pow(10, decimals);
        for (var i = this.abbrev.length - 1; i >= 0; i--) {
            var size = Math.pow(10, (i + 1) * 3);
            if (size <= number) {
                number = Math.round(number * decimals / size) / decimals;
                if ((number === 1000) && (i < this.abbrev.length - 1)) {
                    number = 1;
                    i++;
                }
                number += this.abbrev[i];
                break;
            }
        }
        return number;
    };
    return AbbrPipe;
}());
AbbrPipe = __decorate([
    core_1.Pipe({
        name: 'abbr'
    })
], AbbrPipe);
exports.AbbrPipe = AbbrPipe;
//# sourceMappingURL=abbr.js.map