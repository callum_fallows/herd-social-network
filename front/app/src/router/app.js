"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var homepage_1 = require("../controllers/home/homepage/homepage");
var login_1 = require("../controllers/home/login/login");
var logout_1 = require("../controllers/home/logout/logout");
var register_1 = require("../controllers/home/register/register");
var forgot_password_1 = require("../controllers/home/forgot-password/forgot-password");
var newsfeed_1 = require("../controllers/newsfeed/newsfeed");
var capture_1 = require("../controllers/capture/capture");
var discovery_1 = require("../controllers/discovery/discovery");
var channel_1 = require("../controllers/channels/channel");
var settings_1 = require("../controllers/settings/settings");
var admin_1 = require("../controllers/admin/admin");
var pages_1 = require("../controllers/pages/pages");
var media_1 = require("../controllers/media/media");
var can_deactivate_guard_1 = require("../services/can-deactivate-guard");
var rewards_1 = require("../controllers/rewards/rewards");
exports.MindsAppRoutes = [
    { path: '', component: homepage_1.Homepage },
    { path: 'login', component: login_1.Login },
    { path: 'logout', component: logout_1.Logout },
    { path: 'register', component: register_1.Register },
    { path: 'forgot-password', component: forgot_password_1.ForgotPassword },
    { path: 'newsfeed/subscribed', component: newsfeed_1.Newsfeed, canDeactivate: [can_deactivate_guard_1.CanDeactivateGuardService] },
    { path: 'newsfeed/boost', component: newsfeed_1.Newsfeed, canDeactivate: [can_deactivate_guard_1.CanDeactivateGuardService] },
    { path: 'newsfeed/:guid', component: newsfeed_1.NewsfeedSingle },
    { path: 'newsfeed', component: newsfeed_1.Newsfeed, canDeactivate: [can_deactivate_guard_1.CanDeactivateGuardService] },
    { path: 'capture', component: capture_1.Capture },
    { path: 'discovery/:filter/:type', component: discovery_1.Discovery },
    { path: 'discovery/:filter', component: discovery_1.Discovery },
    { path: 'media/edit/:guid', component: media_1.MediaEdit },
    { path: 'media/:container/:guid', component: media_1.MediaView },
    { path: 'media/:guid', component: media_1.MediaView },
    { path: 'archive/view/:container/:guid', component: media_1.MediaView },
    { path: 'archive/view/:guid', component: media_1.MediaView },
    { path: 'archive/edit/:guid', component: media_1.MediaEdit },
    { path: 'settings/:filter/:card', component: settings_1.Settings },
    { path: 'settings/:filter', component: settings_1.Settings },
    { path: 'admin/:filter/:type', component: admin_1.Admin },
    { path: 'admin/:filter', component: admin_1.Admin },
    { path: 'p/:page', component: pages_1.Pages },
    { path: 'claim-rewards/:uuid', component: rewards_1.RewardsComponent },
    { path: ':username/:filter', component: channel_1.Channel },
    { path: ':username', component: channel_1.Channel, canDeactivate: [can_deactivate_guard_1.CanDeactivateGuardService] },
];
exports.MindsAppRoutingProviders = [{ provide: common_1.APP_BASE_HREF, useValue: '/' }];
exports.MINDS_APP_ROUTING_DECLARATIONS = [
    homepage_1.Homepage,
    login_1.Login,
    logout_1.Logout,
    register_1.Register,
    forgot_password_1.ForgotPassword,
    newsfeed_1.NewsfeedSingle,
    newsfeed_1.Newsfeed,
    capture_1.Capture,
    discovery_1.Discovery,
    media_1.MediaView,
    media_1.MediaEdit,
    settings_1.Settings,
    admin_1.Admin,
    pages_1.Pages,
    channel_1.Channel,
    rewards_1.RewardsComponent,
];
//# sourceMappingURL=app.js.map