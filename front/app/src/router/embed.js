"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var common_1 = require("@angular/common");
var media_1 = require("../controllers/media/media");
exports.MindsEmbedRoutes = [
    { path: 'api/v1/embed/:guid', component: media_1.MediaView }
];
exports.MindsEmbedRoutingProviders = [{ provide: common_1.APP_BASE_HREF, useValue: '/' }];
exports.MINDS_EMBED_ROUTING_DECLARATIONS = [
    media_1.MediaView,
];
//# sourceMappingURL=embed.js.map