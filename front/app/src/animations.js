"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
exports.animations = [
    core_1.trigger('foolishIn', [
        core_1.transition('* => *', [
            core_1.style({ opacity: 0 }),
            core_1.animate(2000, core_1.keyframes([
                core_1.style({ opacity: 0, transformOrigin: '50% 50%', transform: 'scale(0, 0)     rotate(360deg)', offset: 0.000 }),
                core_1.style({ opacity: 1, transformOrigin: '0% 100%', transform: 'scale(0.5, 0.5) rotate(0deg)', offset: 0.066 }),
                core_1.style({ opacity: 1, transformOrigin: '100% 100%', transform: 'scale(0.5, 0.5) rotate(0deg)', offset: 0.132 }),
                core_1.style({ opacity: 1, transformOrigin: '0%', transform: 'scale(0.5, 0.5) rotate(0deg)', offset: 0.198 }),
                core_1.style({ opacity: 1, transformOrigin: '0% 0%', transform: 'scale(0.5, 0.5) rotate(0deg)', offset: 0.264 }),
                core_1.style({ opacity: 1, transformOrigin: '50% 50%', transform: 'scale(1, 1)     rotate(0deg)', offset: 0.330 }),
                core_1.style({ opacity: 1, offset: 0.660 }),
                core_1.style({ opacity: 0, offset: 1.000 }),
            ]))
        ])
    ])
];
//# sourceMappingURL=animations.js.map