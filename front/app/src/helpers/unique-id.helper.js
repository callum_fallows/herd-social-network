"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UniqueId = (function () {
    function UniqueId() {
    }
    UniqueId.generate = function (prefix) {
        if (prefix === void 0) { prefix = 'id'; }
        UniqueId.counter++;
        return "minds-" + prefix + "--" + UniqueId.counter;
    };
    return UniqueId;
}());
UniqueId.counter = 1000;
exports.UniqueId = UniqueId;
//# sourceMappingURL=unique-id.helper.js.map