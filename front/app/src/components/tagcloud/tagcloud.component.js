"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_1 = require("../../services/api");
var TagcloudComponent = (function () {
    function TagcloudComponent(client) {
        this.client = client;
        this.tags = [];
    }
    TagcloudComponent.prototype.ngOnInit = function () {
        this.load();
    };
    TagcloudComponent.prototype.load = function () {
        var _this = this;
        this.client.get('api/v1/search/tagcloud')
            .then(function (response) {
            _this.tags = response.tags;
        });
    };
    return TagcloudComponent;
}());
TagcloudComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'm-tagcloud',
        templateUrl: 'tagcloud.component.html'
    }),
    __metadata("design:paramtypes", [api_1.Client])
], TagcloudComponent);
exports.TagcloudComponent = TagcloudComponent;
//# sourceMappingURL=tagcloud.component.js.map