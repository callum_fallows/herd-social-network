"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var line_graph_1 = require("./graphs/line-graph");
var pie_graph_1 = require("./graphs/pie-graph");
exports.MINDS_GRAPHS = [line_graph_1.LineGraph, pie_graph_1.PieGraph];
//# sourceMappingURL=graphs.js.map