"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var navigation_1 = require("../../services/navigation");
var wallet_1 = require("../../services/wallet");
var session_1 = require("../../services/session");
var storage_1 = require("../../services/storage");
var animations_1 = require("../../animations");
var TopbarNavigation = (function () {
    function TopbarNavigation(navigation, wallet, storage) {
        this.navigation = navigation;
        this.wallet = wallet;
        this.storage = storage;
        this.session = session_1.SessionFactory.build();
        this.walletPopContent = '';
        this.queueWalletAnimationPoints = 0;
    }
    TopbarNavigation.prototype.ngAfterViewInit = function () {
        this.walletListen();
    };
    TopbarNavigation.prototype.ngOnDestroy = function () {
        this.walletUnListen();
    };
    TopbarNavigation.prototype.setCounter = function (name, value) {
        this.navigation.getItems('topbar').forEach(function (item) {
            if (item.name !== name) {
                return;
            }
            item.extras.counter = value;
        });
    };
    TopbarNavigation.prototype.walletListen = function () {
        var _this = this;
        this.walletSubscription = this.wallet.onPoints().subscribe(function (_a) {
            var batch = _a.batch, total = _a.total;
            if (total === null) {
                total = '…';
            }
            _this.setCounter('Wallet', total);
            if (batch && !_this.storage.get('disablePointsAnimation')) {
                _this.queueWalletAnimation(batch);
            }
        });
    };
    TopbarNavigation.prototype.walletUnListen = function () {
        if (this.walletSubscription) {
            this.walletSubscription.unsubscribe();
        }
    };
    TopbarNavigation.prototype.queueWalletAnimation = function (points) {
        var _this = this;
        if (this.queueWalletAnimationTimer) {
            clearTimeout(this.queueWalletAnimationTimer);
        }
        this.queueWalletAnimationPoints += points;
        this.queueWalletAnimationTimer = setTimeout(function () {
            if (_this.queueWalletAnimationPoints > 0) {
                _this.playWalletAnimation(_this.queueWalletAnimationPoints);
            }
            _this.queueWalletAnimationPoints = 0;
        }, 1000);
    };
    TopbarNavigation.prototype.playWalletAnimation = function (points) {
        this.walletPopContent = "+" + points;
        this.walletPopState = Date.now();
    };
    return TopbarNavigation;
}());
TopbarNavigation = __decorate([
    core_1.Component({
        selector: 'herd-topbar-navigation',
        templateUrl: 'topbar-navigation.component.html',
        animations: animations_1.animations
    }),
    __metadata("design:paramtypes", [navigation_1.Navigation, wallet_1.WalletService, storage_1.Storage])
], TopbarNavigation);
exports.TopbarNavigation = TopbarNavigation;
//# sourceMappingURL=topbar-navigation.js.map