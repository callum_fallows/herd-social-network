"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var storage_1 = require("../../services/storage");
var sidebar_1 = require("../../services/ui/sidebar");
var notification_service_1 = require("../../modules/notifications/notification.service");
var session_1 = require("../../services/session");
var Topbar = (function () {
    function Topbar(storage, sidebar, notification) {
        this.storage = storage;
        this.sidebar = sidebar;
        this.notification = notification;
        this.session = session_1.SessionFactory.build();
        this.notifications = [];
        this.minds = window.Minds;
    }
    Topbar.prototype.ngOnInit = function () {
        this.listenForNotifications();
    };
    Topbar.prototype.openNav = function () {
        this.sidebar.open();
    };
    Topbar.prototype.listenForNotifications = function () {
        var _this = this;
        this.notification.onReceive.subscribe(function (notification) {
            _this.notifications.unshift(notification);
            setTimeout(function () {
                _this.closeNotification(notification);
            }, 6000);
        });
    };
    Topbar.prototype.closeNotification = function (notification) {
        var i;
        for (i in this.notifications) {
            if (this.notifications[i] === notification) {
                this.notifications.splice(i, 1);
            }
        }
    };
    return Topbar;
}());
Topbar = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'herd-topbar',
        templateUrl: 'topbar.html'
    }),
    __metadata("design:paramtypes", [storage_1.Storage, sidebar_1.Sidebar, notification_service_1.NotificationService])
], Topbar);
exports.Topbar = Topbar;
//# sourceMappingURL=topbar.js.map