"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var navigation_1 = require("../../services/navigation");
var session_1 = require("../../services/session");
var sockets_1 = require("../../services/sockets");
var SidebarNavigation = (function () {
    function SidebarNavigation(navigation, sockets) {
        this.navigation = navigation;
        this.sockets = sockets;
        this.session = session_1.SessionFactory.build();
        var self = this;
        this.items = navigation.getItems('sidebar');
        this.getUser();
    }
    SidebarNavigation.prototype.getUser = function () {
        var self = this;
        this.user = this.session.getLoggedInUser(function (user) {
            self.user = user;
        });
    };
    SidebarNavigation.prototype.messengerListener = function () {
        var _this = this;
        this.sockets.subscribe('messageReceived', function (from_guid, message) {
            if (message.type !== 'message') {
                return;
            }
            _this.navigation.setCounter('Messenger', 1);
        });
    };
    return SidebarNavigation;
}());
SidebarNavigation = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'minds-sidebar-navigation',
        templateUrl: 'sidebar-navigation.html'
    }),
    __metadata("design:paramtypes", [navigation_1.Navigation, sockets_1.SocketsService])
], SidebarNavigation);
exports.SidebarNavigation = SidebarNavigation;
//# sourceMappingURL=sidebar-navigation.js.map