"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var GraphImpressions = (function () {
    function GraphImpressions() {
        this.points = '0 200, 500 0';
        this.y = 200;
        this.x = 500;
        this.y_padding = 20;
    }
    Object.defineProperty(GraphImpressions.prototype, "_impressions", {
        set: function (value) {
            this.impressions = value;
            this.calculate();
        },
        enumerable: true,
        configurable: true
    });
    GraphImpressions.prototype.getBounds = function () {
        var max = 0;
        var min = this.impressions[0];
        for (var _i = 0, _a = this.impressions; _i < _a.length; _i++) {
            var stat = _a[_i];
            if (stat > max)
                max = stat;
            if (stat < min)
                min = stat;
        }
        return max;
    };
    GraphImpressions.prototype.calculate = function () {
        var y_bounds = this.getBounds();
        var y_divi = (y_bounds + this.y_padding) / this.y;
        var x_count = this.impressions.length;
        var x_diff = this.x / x_count;
        var x_ticker = 0;
        this.points = x_ticker + ' ' + this.y;
        for (var _i = 0, _a = this.impressions; _i < _a.length; _i++) {
            var stat = _a[_i];
            x_ticker = x_ticker + x_diff;
            var y_stat = this.y - (stat / y_divi);
            this.points += ', ' + x_ticker + ' ' + y_stat;
        }
    };
    return GraphImpressions;
}());
GraphImpressions = __decorate([
    core_1.Component({
        selector: 'minds-graph-impressions',
        inputs: ['_impressions: impressions', 'y: height', 'x: width'],
        template: "\n    <svg fill=\"currentColor\" [viewBox]=\"'0 0 ' + x + ' ' + y\" style=\"stroke:#757575; opacity:0.8\" xmlns=\"http://www.w3.org/2000/svg\" >\n      <!-- X Y, X Y (from top to bottom) -->\n      <polyline [points]=\"points\"\n        style=\"fill:none;stroke-width:4\"\n      />\n\n    </svg>\n  "
    }),
    __metadata("design:paramtypes", [])
], GraphImpressions);
exports.GraphImpressions = GraphImpressions;
//# sourceMappingURL=impressions.js.map