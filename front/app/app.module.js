"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var animations_1 = require("@angular/platform-browser/animations");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var captcha_module_1 = require("./src/modules/captcha/captcha.module");
var app_component_1 = require("./app.component");
var app_1 = require("./src/router/app");
var declarations_1 = require("./src/declarations");
var plugin_declarations_1 = require("./src/plugin-declarations");
var providers_1 = require("./src/services/providers");
var plugin_providers_1 = require("./src/plugin-providers");
var common_module_1 = require("./src/common/common.module");
var monetization_module_1 = require("./src/modules/monetization/monetization.module");
var wallet_module_1 = require("./src/modules/wallet/wallet.module");
var checkout_module_1 = require("./src/modules/checkout/checkout.module");
var plus_module_1 = require("./src/modules/plus/plus.module");
var i18n_module_1 = require("./src/modules/i18n/i18n.module");
var ads_module_1 = require("./src/modules/ads/ads.module");
var boost_module_1 = require("./src/modules/boost/boost.module");
var wire_module_1 = require("./src/modules/wire/wire.module");
var report_module_1 = require("./src/modules/report/report.module");
var channel_module_1 = require("./src/modules/channel/channel.module");
var forms_module_1 = require("./src/modules/forms/forms.module");
var legacy_module_1 = require("./src/modules/legacy/legacy.module");
var modals_module_1 = require("./src/modules/modals/modals.module");
var payments_module_1 = require("./src/modules/payments/payments.module");
var third_party_networks_module_1 = require("./src/modules/third-party-networks/third-party-networks.module");
var translate_module_1 = require("./src/modules/translate/translate.module");
var video_module_1 = require("./src/modules/video/video.module");
var settings_module_1 = require("./src/modules/settings/settings.module");
var onboarding_module_1 = require("./src/modules/onboarding/onboarding.module");
var notification_module_1 = require("./src/modules/notifications/notification.module");
var groups_module_1 = require("./src/modules/groups/groups.module");
var post_menu_module_1 = require("./src/common/components/post-menu/post-menu.module");
var ban_module_1 = require("./src/modules/ban/ban.module");
var blog_module_1 = require("./src/modules/blogs/blog.module");
var search_module_1 = require("./src/modules/search/search.module");
var messenger_module_1 = require("./src/modules/messenger/messenger.module");
var MindsModule = (function () {
    function MindsModule() {
    }
    return MindsModule;
}());
MindsModule = __decorate([
    core_1.NgModule({
        bootstrap: [
            app_component_1.Minds
        ],
        declarations: [
            app_component_1.Minds,
            app_1.MINDS_APP_ROUTING_DECLARATIONS,
            declarations_1.MINDS_DECLARATIONS,
            plugin_declarations_1.MINDS_PLUGIN_DECLARATIONS,
        ],
        imports: [
            platform_browser_1.BrowserModule,
            animations_1.BrowserAnimationsModule,
            forms_1.ReactiveFormsModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            router_1.RouterModule.forRoot(app_1.MindsAppRoutes),
            captcha_module_1.CaptchaModule,
            common_module_1.CommonModule,
            wallet_module_1.WalletModule,
            checkout_module_1.CheckoutModule,
            monetization_module_1.MonetizationModule,
            plus_module_1.PlusModule,
            ads_module_1.AdsModule,
            boost_module_1.BoostModule,
            wire_module_1.WireModule,
            report_module_1.ReportModule,
            i18n_module_1.I18nModule,
            ban_module_1.BanModule,
            third_party_networks_module_1.ThirdPartyNetworksModule,
            legacy_module_1.LegacyModule,
            translate_module_1.TranslateModule,
            video_module_1.VideoModule,
            settings_module_1.SettingsModule,
            modals_module_1.ModalsModule,
            payments_module_1.PaymentsModule,
            forms_module_1.MindsFormsModule,
            channel_module_1.ChannelModule,
            onboarding_module_1.OnboardingModule,
            notification_module_1.NotificationModule,
            groups_module_1.GroupsModule,
            blog_module_1.BlogModule,
            post_menu_module_1.PostMenuModule,
            search_module_1.SearchModule,
            messenger_module_1.MessengerModule
        ],
        providers: [
            app_1.MindsAppRoutingProviders,
            providers_1.MINDS_PROVIDERS,
            plugin_providers_1.MINDS_PLUGIN_PROVIDERS,
        ],
        schemas: [
            core_1.CUSTOM_ELEMENTS_SCHEMA
        ]
    })
], MindsModule);
exports.MindsModule = MindsModule;
//# sourceMappingURL=app.module.js.map