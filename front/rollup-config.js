"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rollup_plugin_node_resolve_1 = require("rollup-plugin-node-resolve");
var rollup_plugin_commonjs_1 = require("rollup-plugin-commonjs");
var rollup_plugin_uglify_1 = require("rollup-plugin-uglify");
exports.default = {
    entry: '.tmp/app/bootstrap-aot.js',
    dest: 'public/js/build-aot.js',
    sourceMap: false,
    format: 'iife',
    context: 'window',
    plugins: [
        rollup_plugin_node_resolve_1.default({ jsnext: true, module: true }),
        rollup_plugin_commonjs_1.default({
            include: 'node_modules/rxjs/**',
        }),
        rollup_plugin_uglify_1.default()
    ]
};
//# sourceMappingURL=rollup-config.js.map